# README #

### Was machen wir hier eigentlich?

Dokumentieren, welche neuen Möglichkeiten und Fehlerbehebungen es im 3x7job gibt.

#### 11. Juli 2015
Das Löschen von Bereichen wurde umgesetzt. Dabei ist das kein richtiges Löschen, sondern nur ein Ausblenden von Bereichen. 
Außerdem haben die Admins die Möglichkeit die gelöschten Bereiche auch wieder herzustellen.