<?php
add_action('wp_ajax_3x7Rooms_loadRegistrationState', 'x7_ajax_loadRegistrationState');
function x7_ajax_loadRegistrationState(){
	$registrationState = get_option('x7RoomsIsRegistrationOpen');
	$return = array();
	$return["log"] = "loadState";
	$return["state"] = $registrationState;
	x7sendOutPut($return);
}

add_action('wp_ajax_3x7Rooms_loadWaitinglistMode', 'x7_ajax_loadWaitinglistMode');
function x7_ajax_loadWaitinglistMode(){
	$registrationState = get_option('x7RoomsIsWaitingList');
	$return = array();
	$return["log"] = "loadState";
	$return["state"] = $registrationState;
	x7sendOutPut($return);
}

add_action('wp_ajax_3x7Rooms_toggleRegistrationState', 'x7Rooms_ajax_toggleRegistrationState');
function x7Rooms_ajax_toggleRegistrationState(){
	$registrationState = get_option('x7RoomsIsRegistrationOpen');
	$return = array();
	$return["log"] = "loadState";	
	
	if ($registrationState == false)
	{
		update_option('x7RoomsIsRegistrationOpen', true);
	}
	else {
		update_option('x7RoomsIsRegistrationOpen', false);
	}
	
	$return["state"] = get_option('x7RoomsIsRegistrationOpen');
	x7sendOutPut($return);
}

add_action('wp_ajax_3x7Rooms_toggleWaitinglistMode', 'x7Rooms_ajax_toggleWaitinglistMode');
function x7Rooms_ajax_toggleWaitinglistMode(){
	$isWaitingList = get_option('x7RoomsIsWaitingList');
	$return = array();
	$return["oldValue"] = $isWaitingList;
	if ($isWaitingList == false)
	{
		update_option('x7RoomsIsWaitingList', true);
	}
	else {
		update_option('x7RoomsIsWaitingList', false);
	}
	
	$return["state"] = get_option('x7RoomsIsWaitingList');
	x7sendOutPut($return);
}

add_action('wp_ajax_3x7Rooms_setOptionsText', 'x7Rooms_ajax_setOptionsText');
function x7Rooms_ajax_setOptionsText(){
	$textActive = $_POST["textActive"];
	$textNotActive = $_POST["textNotActive"];
	$textWaitingList = $_POST["textWaitinglist"];
	
	update_option('x7RoomsTextActive', $textActive);
	update_option('x7RoomsTextNotActive', $textNotActive);
	update_option('x7RoomsTextWaitinglist', $textWaitingList);
		
	x7Rooms_ajax_getOptionsText();
}

add_action('wp_ajax_3x7Rooms_getOptionsText', 'x7Rooms_ajax_getOptionsText');
function x7Rooms_ajax_getOptionsText(){
	//$registrationState = get_option('x7RoomsTextActive');
	$return = array();	
	
	$return["textActive"] = get_option('x7RoomsTextActive');
	$return["textNotActive"] =  get_option('x7RoomsTextNotActive');
	$return["textWaitinglist"] =  get_option('x7RoomsTextWaitinglist');		
		
	x7sendOutPut($return);
}

add_action('wp_ajax_3x7Rooms_getRoomReservations', 'x7Rooms_ajax_getRoomReservations');
function x7Rooms_ajax_getRoomReservations(){
	global $table_prefix;		
	$return = array();
	$loadSize = $_POST["loadSize"];
	$startItem = $_POST["startItem"];
	$endItem = $startItem + $loadSize;
	
	
    $roomsListQuery = new x7Template(X7ROOMSSQL."LoadRoomsListLight.sql", X7ROOMSPATH);
	$SqlParams = array();
	$SqlParams["__prefix__"]  	= $table_prefix;
	$SqlParams["__year__"]  	= get_option('FsmaJahr');
	
	$SqlParams["__limit__"]   	= "limit $startItem,$endItem";
	
	$roomsListQuery->showErrors = false;
	$queryResult = $roomsListQuery->DoMultipleQuery(true, $SqlParams, ARRAY_A);
		
	if ($roomsListQuery->Succeeded() == true)
	{
		$return["status"] = 1;
		$return["roomsList"] = $queryResult[0];
		$return["loadSize"] = $loadSize;
		$return["limit"] = $SqlParams["__limit__"];
		$return["startItem"] = $startItem;
		$return["nextItem"] = $endItem+1;
		$return["furtherPossible"] = (count($queryResult[0]) > 0 );
	}
	else	 
	{
		$return["status"] = 0; 
		$return["errorText"] = $roomsListQuery->GetErrorText();		
	} 
	
	
	x7sendOutPut($return);	
}
?>