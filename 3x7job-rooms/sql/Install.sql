CREATE TABLE IF NOT EXISTS `__prefix__roomrequest` (
  `request_id` int  NOT NULL AUTO_INCREMENT,
  `surname` varchar(50)  NOT NULL,
  `firstname` varchar(50)  NOT NULL,
  `email` varchar(100)  NOT NULL,
  `citycode` varchar(20) ,
  `phone` varchar(20) ,
  `bedcount` int  NOT NULL,
  `year` int  NOT NULL,
  `comment` varchar(200) ,
  PRIMARY KEY (`request_id`)
)
