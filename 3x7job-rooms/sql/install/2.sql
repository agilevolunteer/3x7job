ALTER TABLE `__prefix__roomrequest` ADD COLUMN `kidstext` varchar(200)  NOT NULL AFTER `comment`,
 ADD COLUMN `additionalSpace` integer  NOT NULL AFTER `kidstext`,
 ADD COLUMN `noRoomSharing` BOOL  NOT NULL AFTER `additionalSpace`,
 ADD COLUMN `sharingComment` varchar(200)  NOT NULL AFTER `noRoomSharing`;
