INSERT INTO `__prefix__roomrequest` (
  `surname` ,
  `firstname`,
  `email`,
  `citycode`,
  `phone`,
  `bedcount`,
  `year`,
  `comment`,
  `kidstext`,
  `additionalSpace`,
  `noRoomSharing`,
  `sharingComment`,
  `isWaitinglistEntry`  
) VALUES
(
 "__surname__" ,
  "__firstname__",
  "__email__",
  "__citycode__",
  "__phone__",
  "__bedcount__",
  "__year__",
  "__comments__",
  "__kidstext__",
  "__additionalSpace__",
  __noRoomSharing__,
  "__sharingComment__",
  __isWaitinglistEntry__
)
