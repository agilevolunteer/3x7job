<div id="RoomRegistrationWrapper" style="text-align: left;">
<font color=red>__FEHLER__</font>
<form action="__ACTION__" method="POST" align="left">
	<input type="hidden" value="" name="trap"/>
	<p>
		Nachname / Surname<br />
		<input type="text" name="surname" value="__surname__"/>
	</p>
	<p>
		Vorname / First Name<br />
		<input type="text" name="firstname" value="__firstname__"/>
	</p>
	<p>
		Email / Email<br />
		<input type="text" name="email" value="__email__"/>
	</p>
	<p>
		Email best&auml;tigen / Confirm Email<br />
		<input type="text" name="email2" />
	</p>
	<p>
		Vorwahl / Country Code and City Code<br />
		<input type="text" name="citycode" value="__citycode__"/>
	</p>
	<p>
		Telefonnr / Phone number<br />
		<input type="text" name="phone" value="__phone__"/>
	</p>
	<p>
		Benötigte Anzahl Betten (die Freakstock stellt) / Amount of beds required (by freakstock)<br />
		<input type="text" name="bedcount" value="__bedcount__"/>
	</p>
	<p>
		Anzahl und Alter der Kinder / number and age of the kids<br />
		<textarea name="kidstext">
		__kidstext__
		</textarea>
	</p>
	<p>
		Zus&auml;tzlicher Platz für eigene Reisebetten (Anzahl) / Additional space for own beds / cribs (quantity)<br />
		<input type="text" name="additionalSpace" value="__additionalSpace__"/>
	</p>
	<p>
	    <input type="checkbox" name="noRoomSharing" /> Einzelunterbringung / single accommodation
	</p>
	<p>
Da wir möglichst vielen Familien ermöglichen wollen in einem Haus untergebracht zu sein und die Nachfrage sehr groß ist, bringen wir wenn dies für die betreffende Familie möglich ist gerne 2 Familien gemeinsam in einem Zimmer unter (besonders für Familien mit älteren Kindern gedacht oder wirklich hochschwangere Frauen). Daher bitte Einzelunterbringung nur anwählen wenn nötig. Im Kommentarfeld ganz unten kann gerne angegeben werden mit welcher Familie sie gern zusammen wohnen würden, wenn sie bereits ne Idee haben. /

As we want to enable as many families as possible to stay in one house and the demand is great, we accommodate, wherever possible 2 families in a shared room (this is an option mailly for families with older children or pregnant woman). So please, only choose single accommodation if it is necessary. In the comment field at the bottom of the page you can name families with whom you would like to share accommodation, if you already have someone in mind. 
	</p>
	<p>
		Begründung für Einzelbelegung /<br>
		reason for single accommodation<br />
		<textarea name="singleReason">
		__singleReason__
		</textarea>
	</p>
	<p>
		Bemerkungen  /<br>
		Comments <br />
		<textarea name="comments">
		__comments__
		</textarea>
	</p>
	<p>
		<input type="submit" value="Abschicken | Send" />
	</p>
	
</form>
</div>
