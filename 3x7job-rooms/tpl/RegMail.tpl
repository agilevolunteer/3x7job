	Diese Daten wurden für die Zimmerreservierung übernommen.
	These information were taken for the room reservation.
	
	<p>
		Nachname / Surname<br />
		__surname__
	</p>
	<p>
		Vorname / First Name<br />
		__firstname__
	</p>
	<p>
		Email / Email<br />
		__email__
	</p>	
	<p>
		Vorwahl / Country Code and City Code<br />
		__citycode__
	</p>
	<p>
		Telefonnr / Phone number<br />
		__phone__
	</p>
	<p>
		Benötige Anzahl Betten / Amount of beds required<br />
		__bedcount__
	</p>
	<p>
		Anzahl und Alter der Kinder / number and age of the kids<br />		
		__kidstext__		
	</p>
	<p>
		Zusätzlicher Platz für Anzahl Reisebetten (die selber mitgebracht werden) / Additional space for own beds / cribs<br />
		__additionalSpace__
	</p>
	<p>
	    Einzelunterbringung / single booking for a room<br />
	    __noRoomSharingText__
	</p>
	<p>
		Begründung für Einzelbelegung /<br>
		reason for single booking<br />
		__sharingComment__
	</p>
	<p>
		Bemerkung / Comments<br />
		__comments__
	</p>	
	
