<?php 


define('X7ROOMSPATH',dirname(__FILE__).'/');
define('X7ROOMSSQL',"sql/");
define('X7ROOMSTPL',"tpl/");
define('X7ROOMSURL', plugins_url( '/' , __FILE__ ));

define('X7ROOMSSUBJECT', "[Freakstock] Reservierung des Familienzimmers");
define('X7ROOMSWAITSUBJECT', "[Freakstock] Familienzimmer Warteliste");
define('X7ROOMSSENDER',  "betten@freakstock.de");
define('X7ROOMSSENDCONFIRM',  true);

//Put this on the Frontend
define("X7ROOMS", "<!--X7ROOMS-->");

require_once(X7ROOMSPATH."init.php");
require_once(X7ROOMSPATH."ajax.php");

function x7RoomsReg($content)
{
	$state = get_option('x7RoomsIsRegistrationOpen');
	
	if($state == TRUE)
	{
	
		
		$validator = new jfbremenValidate();
		$invalid = false;
		//Validate
		
		if (substr_count($content,X7ROOMS) <= 0) return $content;
		if (isset($_POST["trap"]) && (empty($_POST["trap"])))
		{		
		    $alleFehler = array();
		
			//Gleiche Mails	
			if (jfPost("email", "STRING") != jfPost("email2", "STRING"))
			{
				$alleFehler[] = "Die angegebenen Emailadressen müssen identisch sein / Emails should be equal";
				$invalid = true;	
			} 
			
			if (!$validator->isMail(jfPost("email", "STRING"))  )
			{
				$alleFehler[] = "Die angegebene Emailadresse ist keine korrekte Emailadresse / Email is invalid";
				$invalid = true;	
			}
			
			//if (empty(jfPost("surname", "STRING")))
			if (empty($_POST["surname"]))
	        {
		        $alleFehler[] = "Bitte den Familiennamen angeben / Do not forget the surname";
		        $invalid = true;	
	        }
		
	
	
	        if (empty($_POST["firstname"]))
	        {
		        $alleFehler[] = "Bitte den Vorname angeben / Do not forget the first name";
		        $invalid = true;	
	        }
	
	
	        if (empty($_POST["bedcount"]))
	        {
		        $alleFehler[] = "Bitte die Bettenanzahl angeben / Do not forget the surname";
		        $invalid = true;	
	        }
	
	
	        if (empty($_POST["kidstext"]))
	        {
		        $alleFehler[] = "Bitte Anzahl und Alter der Kinder angeben / Do not forget the kids age and quantity";
		        $invalid = true;	
	        }
	
		}
		
		$output = "";
		
		if(get_option('x7RoomsIsWaitingList')){
			$output .= get_option('x7RoomsTextWaitinglist');					
		}else{
			$output .= get_option('x7RoomsTextActive');
		}	
		
		if (!$invalid && isset($_POST["trap"]) && (empty($_POST["trap"])))
		{
			x7RoomsMakeRegistration();
					
			
			if(get_option('x7RoomsIsWaitingList')){
				$output .= "<br />	Ihr Eintrag wurde versendet. Sie erhalten eine Email zur Bestätigung der Eintragung in die Warteliste.";
			}else{
				$output .= "<br />	Ihre Registrierung wurde versendet. Sie erhalten eine Email zur Bestätigung der Reservierung.";
			}		
		}
		else
		{	
			$regTemplate = new x7Template(X7EXPORTTPL."RoomsForm.tpl", X7ROOMSPATH);
			$params = array();
			$params["__ACTION__"] = FsmaAddUrlParam();
			$params["__FEHLER__"] = x7CreateErrorText($alleFehler);
			$params["__prefix__"] = $table_prefix;
			
			if($invalid == true)
			{
				$params["__surname__"]		= jfPost("surname", "STRING");
			    $params["__firstname__"]	= jfPost("firstname", "STRING");
			    $params["__email__"]		= jfPost("email", "STRING");
			    $params["__citycode__"]		= jfPost("citycode", "STRING");
			    $params["__phone__"]		= jfPost("phone", "STRING");
			    $params["__bedcount__"]		= jfPost("bedcount");
			    $params["__kidstext__"]		= jfPost("kidstext", "STRING");	
			    $params["__additionalSpace__"]		= jfPost("additionalSpace");	
	            $params["__noRoomSharing__"]		= jfPost("noRoomSharing", "STRING");
	            $params["__comments__"]		= jfPost("comments", "STRING");	
	            $params["__singleReason__"]		= jfPost("singleReason", "STRING");	
	
			}
			
			$output .= $regTemplate->GetFilteredContent($params, true);
		}
	}
	else {
		$output = get_option('x7RoomsTextNotActive');		
	}
	
	$content = str_replace(X7ROOMS,$output,$content);
	return $content;	
}

function x7CreateErrorText($errorArray)
{
    $text = "";

    for ($i = 0; $i < count($errorArray); $i++)
    {
        $text .= $errorArray[$i];
        $text .= "<br />";
    }

    return $text;
}

function x7RoomsMakeRegistration()
{
	global $table_prefix;
		
	$params = array();
	$params["__prefix__"]		= $table_prefix;
	$params["__surname__"]		= jfPost("surname", "STRING");
    $params["__firstname__"]	= jfPost("firstname", "STRING");
    $params["__email__"]		= jfPost("email", "STRING");
    $params["__citycode__"]		= jfPost("citycode", "STRING");
    $params["__phone__"]		= jfPost("phone", "STRING");
    $params["__bedcount__"]		= jfPost("bedcount");
    $params["__kidstext__"]		= jfPost("kidstext", "STRING");	
	$params["__additionalSpace__"]		= jfPost("additionalSpace");	
    $params["__noRoomSharing__"]		= (isset($_POST["noRoomSharing"])) ? "b'001'" : "b'000'";
    $params["__noRoomSharingText__"]		= (isset($_POST["noRoomSharing"])) ? "Ja" : "Nein";
    $params["__comments__"]		= jfPost("comments", "STRING");	
    $params["__sharingComment__"]		= jfPost("singleReason", "STRING");	
    $params["__year__"]			= get_option('FsmaJahr');
	$params["__isWaitinglistEntry__"]			= (get_option('x7RoomsIsWaitingList')==true) ? "b'001'" : "b'000'";
	
    $insertRegistration = new x7Template(X7ROOMSSQL."InsertRoomsReg.sql", X7ROOMSPATH);
	$results = $insertRegistration->DoMultipleQuery(true, $params, ARRAY_N);

	$subject = X7ROOMSSUBJECT;
	$mailBodyTemplate = new x7Template(X7EXPORTTPL."RegMail.tpl", X7ROOMSPATH);
	if(get_option('x7RoomsIsWaitingList') == true){
		$mailBodyTemplate = new x7Template(X7EXPORTTPL."WaitMail.tpl", X7ROOMSPATH);	
		$subject = X7ROOMSWAITSUBJECT;
	}
	$mailBody = $mailBodyTemplate->GetFilteredContent($params, true);
	
	x7SendHtmlMail($params["__email__"], X7ROOMSSENDER, $mailBody, $subject, "HtmlStandardMail.tpl", 1);
	
	if (X7ROOMSSENDCONFIRM == true)
		x7SendHtmlMail(X7ROOMSSENDER, get_option('FsmaMailAbsender'), $mailBody, $subject, "HtmlStandardMail.tpl", 1);
		
}
?>
