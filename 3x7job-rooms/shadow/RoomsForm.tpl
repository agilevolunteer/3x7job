<div style="text-align: left;">
<font color=red>__FEHLER__</font>
<form action="__ACTION__" method="POST" align="left">
	<input type="hidden" value="" name="trap"/>
	<p>
		Nachname / Surname<br />
		<input type="text" name="surname" value="__surname__"/>
	</p>
	<p>
		Vorname / First Name<br />
		<input type="text" name="firstname" value="__firstname__"/>
	</p>
	<p>
		Email / Email<br />
		<input type="text" name="email" value="__email__"/>
	</p>
	<p>
		Email best&auml;tigen / Confirm Email<br />
		<input type="text" name="email2" />
	</p>
	<p>
		Vorwahl / Country Code and City Code<br />
		<input type="text" name="citycode" value="__citycode__"/>
	</p>
	<p>
		Telefonnr / Phone number<br />
		<input type="text" name="phone" value="__phone__"/>
	</p>
	<p>
		Benötige Anzahl Betten (die Freakstock stellt) / Amount of beds required (by freakstock)<br />
		<input type="text" name="bedcount" value="__bedcount__"/>
	</p>
	<p>
		Anzahl und Alter der Kinder / number and age of the kids<br />
		<input type="text" name="kidstext" value="__kidstext__"/>
	</p>
	<p>
		Zusätzlicher Platz für Anzahl Reisebetten (die selber mitgebracht werden) / Additional space for own beds / cribs<br />
		<input type="text" name="additionalSpace" value="__additionalSpace__"/>
	</p>
	<p>
	    <input type="checkbox" value="noRoomSharing" /> Einzelzimmerbelegung
	</p>
	<p>
	Da wir möglichst vielen Familien ermöglichen wollen in einem Haus untergebracht zu sein und die Nachfrage sehr groß ist, bringen wir wo möglich 2 Familien gemeinsam in einem Zimmer unter. Bitte Einzelunterbringung nur anwählen wenn unbedingt nötig. Im Kommentarfeld ganz unten kann gerne angegeben werden mit welcher Familie sie gern zusammen wohnen würden, wenn sie bereits ne Idee haben.

	</p>
	<p>
		Begründung für Einzelbelegung /<br>
		Comments (childrens age, space for own cribs)<br />
		<textarea name="comments">
		__comments__
		</textarea>
	</p>
	<p>
		Bemerkung (Platz f&uuml;r eigene Kinderbetten, Alter der Kinder, etc.) /<br>
		Comments (childrens age, space for own cribs)<br />
		<textarea name="comments">
		__comments__
		</textarea>
	</p>
	<p>
		<input type="submit" />
	</p>
	
</form>
</div>
