<?php 
//add_action('x7Initialized','xroomsInitialize');

add_action('x7Initialized','x7roomsInitialize');
add_filter("the_content", "x7RoomsReg");
add_action("wp_head","x7RoomsApplyStyleFrontend");
add_action("admin_head","x7RoomsApplyStyleBackend");
add_action("x7AdminMenuGenerated", "x7RoomsGenerateMenu");

function x7roomsInitialize()
{
    $roomVersionNumber = 5;
	$currentVersion = get_option('x7RoomVersion'); 

    if ($currentVersion == "")
    {
        $currentVersion = 1;
    }
    
    
    if ($roomVersionNumber != $currentVersion)
    {
       global $table_prefix;
		
	    for ($i = 1; $i <= $roomVersionNumber; $i++)
	    {	
	        $installDb = new x7Template(X7ROOMSSQL."/install/$i.sql", X7ROOMSPATH);
	        //$installDb->showErrors = false;
	        $results = $installDb->DoMultipleQuery(true, array("__prefix__" => $table_prefix), ARRAY_N);	     
	    }
	    
	    FsmaNewCap("Export_Room_Requests");
	    update_option('x7RoomVersion', $roomVersionNumber);
	}
	
	wp_register_style( 'x7RoomsStyleFrontend', X7ROOMSURL.'/style/x7rooms.css' ); 
	wp_register_style( 'x7RoomsStyleBackend', X7ROOMSURL.'/style/x7roomsAdmin.css' ); 
}

function x7RoomsApplyStyleFrontend()
{
	wp_enqueue_style('x7RoomsStyleFrontend');
}

function x7RoomsApplyStyleBackend()
{
	wp_enqueue_style('x7RoomsStyleBackend');
}

function x7RoomsGenerateMenu(){
	x7GenerateX7SubMenuPage("Familienzimmer", X7ROOMSPATH.'/admin/overview.php');
}

?>
