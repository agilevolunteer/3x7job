jQuery(function() {	
	// Here's a custom Knockout binding that makes elements shown/hidden via jQuery's fadeIn()/fadeOut() methods
	// Could be stored in a separate utility library
	ko.bindingHandlers.fadeVisible = {
	    init: function(element, valueAccessor) {
	        // Initially set the element to be instantly visible/hidden depending on the value
	        var value = valueAccessor();
	        $(element).toggle(ko.utils.unwrapObservable(value)); // Use "unwrapObservable" so we can handle values that may or may not be observable
	    },
	    update: function(element, valueAccessor) {
	        // Whenever the value subsequently changes, slowly fade the element in or out
	        var value = valueAccessor();
	        ko.utils.unwrapObservable(value) ? $(element).fadeIn() : $(element).fadeOut();
	    }
	};
	
	
	function RoomsOverviewModel() {
		self.isOptionsLoaded = ko.observable(false);
		self.isRegistrationOpen = ko.observable();
		self.isWaitinglistMode = ko.observable();
		self.textActive = ko.observable("");
		self.textNotActive = ko.observable("");	
		self.textWaitinglist = ko.observable("");	
		
		self.messages = ko.observableArray([]);
		self.errors = ko.observableArray([]);
		self.addMessage = function(message){
			self.messages.push(message);	
			setTimeout(function(){
				self.messages.remove(message);	
			},1500);		
		}
		self.showMessage = function(elem) { 
			if (elem.nodeType === 1) 
				jQuery(elem).hide().slideDown(); 
		}
		
		self.hideMessage = function(elem) { 
			if (elem.nodeType === 1) 
				jQuery(elem).slideUp(function() { jQuery(elem).remove(); })
		}
	    		
		self.loadState = function() {
			self.isOptionsLoaded(false);
			jQuery.ajax({
				url : wpajax,
				type : 'POST',
				data : {
					'action' : "3x7Rooms_loadRegistrationState"
				},
				dataType : 'JSON',
				success : function(data) {					
					if (data.state == 1)
					{
						self.isRegistrationOpen(true);
					}
					else
					{
						self.isRegistrationOpen(false);	
					}
					
					self.isOptionsLoaded(true);
				},
				error : function(errorThrown) {
					self.errors.push(errorThrown);					
				},
				cache: false
			});

			//self.isRegistrationOpen(true);
			
		}
		
		self.loadWaitinglistMode = function() {
			self.isOptionsLoaded(false);
			jQuery.ajax({
				url : wpajax,
				type : 'POST',
				data : {
					'action' : "wp_ajax_3x7Rooms_loadWaitinglistMode"
				},
				dataType : 'JSON',
				success : function(data) {
					console.log("Waitingslist: "+ data);					
					if (data.state == 1)
					{
						self.isWaitinglistMode(true);
					}
					else
					{
						self.isWaitinglistMode(false);	
					}
					
					self.isOptionsLoaded(true);
				},
				error : function(errorThrown) {
					self.errors.push(errorThrown);
				},
				cache: false
			});	
		}
		
		self.toggleState = function(){
			jQuery.ajax({
				url : wpajax,
				type : 'POST',
				data : {
					'action' : "3x7Rooms_toggleRegistrationState"
				},
				dataType : 'JSON',
				success : function(data) {
					
					if (data.state == 1)
					{
						self.isRegistrationOpen(true);
						self.addMessage({
							message: "Anmeldung wurde geöffnet.",
							type: "success"						
						});
					}
					else
					{
						self.isRegistrationOpen(false);	
						self.addMessage({
							message: "Anmeldung wurde erfolgreich geschlossen.",
							type: "success"						
						});
					}
				},
				error : function(errorThrown) {
					self.errors.push(errorThrown);	
				},
				cache: false
			});
		}
		
		self.toggleWaitinglist = function(){
			jQuery.ajax({
				url : wpajax,
				type : 'POST',
				data : {
					'action' : "3x7Rooms_toggleWaitinglistMode"
				},
				dataType : 'JSON',
				success : function(data) {
					if (data.state == 1)
					{
						self.isWaitinglistMode(true);
						self.addMessage({
							message: "Warteliste wurde aktiviert.",
							type: "success"						
						});
					}
					else
					{
						self.isWaitinglistMode(false);	
						self.addMessage({
							message: "Warteliste erfolgreich geschlossen.",
							type: "success"						
						});
					}
				},
				error : function(errorThrown) {
					self.errors.push(errorThrown);					
					console.log(errorThrown);
				},
				cache: false
			});
		}
		
		self.loadTexts = function(){
			jQuery.post(wpajax, {"action" : "3x7Rooms_getOptionsText"},function(data){
				console.log(data);
				self.textActive(data.textActive);
				self.textNotActive(data.textNotActive);
				self.textWaitinglist(data.textWaitinglist);
			}, "JSON");
		}
		
		self.saveTexts = function(){
			var params = {
				"action" : "3x7Rooms_setOptionsText",
				"textActive": self.textActive(),
				"textNotActive" : self.textNotActive(),
				"textWaitinglist" : self.textWaitinglist()
			};
			
			jQuery.ajax({
				url : wpajax,
				type : 'POST',
				data : params,
				dataType : 'JSON',
				success : function(data){
					console.log(data);
					self.textActive(data.textActive);
					self.textNotActive(data.textNotActive);
					self.addMessage({
						message: "Daten wurden erfolgreich gespeichert.",
						type: "success"						
					});
				},
				error : function(errorThrown) {
					self.errors.push(errorThrown);					
					console.log(errorThrown);
				},
				cache: false
			});
			
		}

		self.loadState();
		self.loadWaitinglistMode();
		self.loadTexts();
	}

	var overviewModel = new RoomsOverviewModel();

	// Activates ViewModel
	ko.applyBindings(overviewModel);

}); 