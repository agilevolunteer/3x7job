<div class="wrap" >
	<h2>Familienzimmer</h2>
	<div data-bind="template: { foreach: errors,
						beforeRemove: hideMessage,
                        afterAdd: showMessage }">
		<div class="error below-h2">
			<p data-bind="html: $data"></p>	
		</div>	
	</div>
	<div data-bind="template: { foreach: messages,
						beforeRemove: hideMessage,
                        afterAdd: showMessage }">		
		<div class="updated below-h2" style="position: fixed; top: 32px;">
			<p data-bind="text: message"></p>	
		</div>
	</div>
	<div class="roomsOptionsContainer">
		<div class="buttons">		
			<button data-bind="visible: (isRegistrationOpen() == true), css: { 'button-primary' : (isRegistrationOpen() == true && isWaitinglistMode() == true)}, click: toggleState" class="button button-primary">
				Anmeldung schließen
			</button>
			<button data-bind="visible: (isRegistrationOpen() == false), css: { 'button-primary' : (isRegistrationOpen() == false)}, click: toggleState" class="button button-primary">
				Anmeldung öffnen</button>	
				
			<button data-bind="visible: (isWaitinglistMode() == true && isRegistrationOpen() == true), click: toggleWaitinglist" class="button">
				Warteliste beenden
			</button>
			<button data-bind="visible: (isWaitinglistMode() == false && isRegistrationOpen() == true), css: { 'button-primary' : (isRegistrationOpen() == true && isWaitinglistMode() == false)}, click: toggleWaitinglist" class="button">
				Warteliste aktivieren
			</button>	
		</div>
		
		
		<label>Status:</label>
		<span data-bind="visible: (isRegistrationOpen() == true)">
			<span data-bind="visible: (isWaitinglistMode() == true)">
					Warteliste
			</span>
			<span data-bind="visible: (isWaitinglistMode() == false)">
					Normale Anmeldung
			</span>
		</span>
		<span data-bind="visible: (isRegistrationOpen() == false)">
				Anmeldung ist geschlossen.
		</span>
			
		<label for="roomsTextNotActive">Anzeige Anmeldung inaktiv:</label>
		<textarea name="roomsTextNotActive" data-bind="value: textNotActive"></textarea>
			
		<label for="roomsTextActive">Anzeige Normale Anmeldung:</label>
		<textarea name="roomsTextActive" data-bind="value: textActive"></textarea>					
	
		<label for="roomsTextActive">Anzeige Warteliste:</label>
		<textarea name="roomsTextActive" data-bind="value: textWaitinglist"></textarea>		

		<div class="buttons">
			<button class="button button-primary" data-bind="click: saveTexts">Änderungen speichern</button>
		</div>
		<br class="clear" />
	</div>
</div>

<script type="text/javascript">var wpajax =  "<?php echo get_bloginfo("wpurl"); ?>/wp-admin/admin-ajax.php";</script>
<script type="text/javascript" src="<?php echo X7URL; ?>scripts/knockout.js"></script>
<script type="text/javascript" src="<?php echo X7URL; ?>scripts/knockout.mapping.js"></script>
<script type="text/javascript" src="<?php echo X7ROOMSURL; ?>scripts/overviewModel.js"></script>