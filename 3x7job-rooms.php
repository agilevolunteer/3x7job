<?php
/*
Plugin Name: 3x7job-rooms
Plugin URI: https://bitbucket.org/RunwaySolutions/3x7job
Description: Stellt die Anmeldeoberfläche für Familienzimmer zur Verfügung
Version: 2014.5
Author: Marcel Henning - runway solutions
Author URI: http://www.runway-solutions.de
*/

include("3x7job-rooms/rooms.php");
?>