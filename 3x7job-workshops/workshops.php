<?php
//echo "worjshops";
// Register Custom Post Type 
function x7RegisterWorkshopPosttype() {
    $labels = array( 
	    'name' => 'Workshops', 
	    'singular_name' => 'Workshop', 
	    'menu_name' => 'Workshops', 
	    'parent_item_colon' => 'Parent Item:', 
	    'all_items' => 'Alle Workshops',
	    'view_item' => 'Workshop anzeigen', 
	    'add_new_item' => 'Neuen Workshop hinzufügen', 
	    'add_new' => 'Neuer Workshop', 
	    'edit_item' => 'Workshop bearbeiten', 
	    'update_item' => 'Workshop aktualisieren', 
	    'search_items' => 'Workshop suchen' 
	    
     );
     $capabilities = array( 
	    'edit_post' => 'edit_x7ws', 
	    'read_post' => 'read_x7ws', 
	    'delete_post' => 'delete_x7ws', 
	    'edit_posts' => 'edit_x7wss', 
	    'edit_others_posts' => 'edit_others_x7wss', 
	    'publish_posts' => 'publish_x7ws', 
	    'read_private_posts' => 'read_private_x7ws'
     );
     $args = array( 
     	'label' => 'x7ws', 
     	'description' => 'Workshops Beschreibung', 
     	'labels' => $labels, 
     	'supports' => array( 'title', 'editor', 'author', 'custom-fields', ), 
     	'taxonomies' => array( 'x7time', 'x7place' ), 
     	'hierarchical' => false, 
     	'public' => true, 
     	'show_ui' => true, 
     	'show_in_menu' => true, 
     	'show_in_nav_menus' => true, 
     	'show_in_admin_bar' => true, 
     	'menu_position' => 5, 
     	'menu_icon' => '', 
     	'can_export' => true, 
     	'has_archive' => true, 
     	'exclude_from_search' => false, 
     	'publicly_queryable' => true, 
     	'capabilities' => $capabilities, );
     
     register_post_type( 'x7ws', $args );
     x7RegisterWorkshopCapabilities();
 }

function x7RegisterWorkshopCapabilities() { 
	$role = get_role( 'administrator' ); 
	$role->add_cap( 'edit_x7ws' ); 
	$role->add_cap( 'edit_x7wss' ); 
	$role->add_cap( 'edit_others_x7wss' ); 
	$role->add_cap( 'publish_events' ); 
	$role->add_cap( 'read_x7ws' ); 
	$role->add_cap( 'read_private_events' ); 
	$role->add_cap( 'delete_x7ws' ); 
	$role->add_cap( 'publish_x7ws' );
}

// Register Custom Taxonomy
function x7RegisterPlaceAndTimeTaxonomy() {

	$labelsPlace = array(
			'name'                       => _x( 'Ort', 'Taxonomy General Name', 'x7Workshops' ),
			'singular_name'              => _x( 'Orte', 'Taxonomy Singular Name', 'x7Workshops' ),
			'menu_name'                  => __( 'Orte', 'x7Workshops' ),
			'all_items'                  => __( 'Alle Orte', 'x7Workshops' ),
			'parent_item'                => __( 'Parent Item', 'x7Workshops' ),
			'parent_item_colon'          => __( 'Parent Item:', 'x7Workshops' ),
			'new_item_name'              => __( 'Neuer Ort', 'x7Workshops' ),
			'add_new_item'               => __( 'Neuen Ort hinzufügen', 'x7Workshops' ),
			'edit_item'                  => __( 'Ort bearbeiten', 'x7Workshops' ),
			'update_item'                => __( 'Ort aktualisieren', 'x7Workshops' ),
			'separate_items_with_commas' => __( 'Separate items with commas' ),
			'search_items'               => __( 'Orte suchen', 'x7Workshops' ),
			'add_or_remove_items'        => __( 'Orte hinzufügen oder löschen', 'x7Workshops' ),
			'choose_from_most_used'      => __( 'Aus den häufig genutzten Orten auswählen' ),
			'not_found'                  => __( 'Keine Orte gefunden')
	);
	
	$labelsTime = array(
			'name'                       => _x( 'Zeiten', 'Taxonomy General Name', 'x7Workshops' ),
			'singular_name'              => _x( 'Zeit', 'Taxonomy Singular Name', 'x7Workshops' ),
			'menu_name'                  => __( 'Zeiten', 'x7Workshops' ),
			'all_items'                  => __( 'Zeiten', 'x7Workshops' ),
			'parent_item'                => __( 'Parent Item', 'x7Workshops' ),
			'parent_item_colon'          => __( 'Parent Item:', 'x7Workshops' ),
			'new_item_name'              => __( 'Neuer Zeitpunkt', 'x7Workshops' ),
			'add_new_item'               => __( 'Neuen Zeitpunkt hinzufügen', 'x7Workshops' ),
			'edit_item'                  => __( 'Zeitpunkt bearbeiten', 'x7Workshops' ),
			'update_item'                => __( 'Zeitpunkt aktualisieren', 'x7Workshops' ),			
			'add_or_remove_items'        => __( 'Zeiten hinzufügen oder löschen', 'x7Workshops' ),
			'choose_from_most_used'      => __( 'Aus den häufig genutzten Zeiten auswählen' ),
			'not_found'                  => __( 'Keine Zeiten gefunden')
			
	);
	
	$capabilities = array(
			'manage_terms'               => 'edit_x7wss',
			'edit_terms'                 => 'edit_x7wss',
			'delete_terms'               => 'edit_x7wss',
			'assign_terms'               => 'edit_x7ws',
	);
	$argsPlace = array(
			'labels'                     => $labelsPlace,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'capabilities'               => $capabilities,
	);
	
	$argsTime = array(
			'labels'                     => $labelsTime,
			'hierarchical'               => true,
			'public'                     => true,
			'show_ui'                    => true,
			'show_admin_column'          => true,
			'show_in_nav_menus'          => true,
			'show_tagcloud'              => true,
			'capabilities'               => $capabilities,
	);
	
	register_taxonomy( 'x7place', array( 'x7workshop' ),  $argsPlace);
	register_taxonomy( 'x7time', array( 'x7workshop' ), $argsTime );
}

 // Hook into the 'init' action
add_action( 'init', 'x7RegisterPlaceAndTimeTaxonomy', 0 );
add_action( 'init', 'x7RegisterWorkshopPosttype', 0 );
?>