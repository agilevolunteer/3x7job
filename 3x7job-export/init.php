<?php 
require_once(X7EXPORTPATH."export-utils.php");

add_action('x7AdminMenuGenerated', 'x7exportMenu');
add_action('x7ExportDoExport', 'x7exportDoExport');
add_action('x7SessionStarted','x7writeExcelFile');
add_action('x7Initialized','x7exportInitialize');

function x7exportMenu()
{
	x7GenerateX7SubMenuPage("Export", X7EXPORTPATH.'/admin/choose.php');
//	echo "Brot";
//	add_submenu_page(FSMAPATH.'/admin/ma.php', 'Export', 'Export', "use_fsma", FSMAPATH.'/admin/bereiche.php');
}

function x7exportInitialize()
{
	global $table_prefix;
	$installDb = new x7Template(X7EXPORTSQL."Install.sql", X7EXPORTPATH);
	$installDb->showErrors = false;
	$results = $installDb->DoMultipleQuery(true, array("__prefix__" => $table_prefix), ARRAY_N);	
}
?>