select ab.value, ud.*
from __PREFIX__anmeldung a
inner join __PREFIX__userdata ud
on a.MitarbeiterID = ud.id
left outer join __PREFIX__abbau ab
on a.AbbauBereich = ab.ID
where a.jahr = __YEAR__ and AufAbbau = true
order by ab.value, ud.Nachname, ud.Vorname
