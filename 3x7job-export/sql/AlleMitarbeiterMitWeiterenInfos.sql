select b.Bezeichnung, ud.*, a.FSErfahrung as 'Erfahrung', a.bemerkungen as 'Bemerkungen', IF(a.AufAbbau= b'001','Ja','Nein') as 'Aufbau', IF(a.Abbau= b'001','Ja','Nein') as 'Abbau'
from __PREFIX__bereich4ma bma
inner join __PREFIX__anmeldung a
on bma.Ma = a.MitarbeiterID and bma.Jahr = a.Jahr
inner join __PREFIX__userdata ud
on bma.Ma = ud.id
inner join __PREFIX__bereiche b
on bma.BID = b.BID
where a.Jahr = __YEAR__ AND b.BID != __CANCELPOOL__
order by b.Bezeichnung, ud.Nachname, ud.Vorname
