SELECT f.value as "Art", count(*) as "Anzahl"  FROM __PREFIX__userdata ud
inner join __PREFIX__bereich4ma a
on ud.id = a.Ma
inner join __PREFIX__anmeldung anm
on a.Ma = anm.MitarbeiterID and a.Jahr = anm.Jahr
left outer join __PREFIX__food f
on f.id = ud.food
where a.jahr = __YEAR__ and a.bid not in (50, 41, 47, 34) and anm.AufAbbau = true
group by f.id
