CREATE TABLE IF NOT EXISTS `__prefix__exporte` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `display` varchar(200) NOT NULL,
  `file` varchar(60) NOT NULL,
  `capa` varchar(60) NOT NULL,
  PRIMARY KEY (`id`)
);
INSERT INTO `__prefix__exporte` (`id`, `display`, `file`, `capa`) VALUES
(1, 'Alle Bereichsleiter mit Kontaktinformationen', 'Bereichsleiter.sql', 'use_fsma'),
(2, 'Alle Mitarbeiter dieses Jahres', 'AlleMitarbeiter.sql', 'edit_all_ma'),
(3, 'Mitarbeiterliste mit allen Kontaktinformationen', 'MitarbeiterMeinesBereiches.sql', 'edit_own_ma'),
(4, 'Familienzimmerreservierungen', 'Familienzimmer.sql', 'export_room_requests'),
(5, 'Mitarbeiter mit Bettenanfrage', 'MitarbeiterMitBett.sql', 'export_room_requests');