SELECT s.value as Bereich, u.*
FROM __PREFIX__bereich4ma b4m
INNER JOIN __PREFIX__userdata u
ON b4m.Ma = u.id
INNER JOIN __PREFIX__status s
ON b4m.status_id = s.id 
INNER JOIN __PREFIX__bereichsleiter bl
ON b4m.BID = bl.BID_Bereiche
WHERE bl.Uid_users = __BLID__ AND b4m.Jahr = __YEAR__
