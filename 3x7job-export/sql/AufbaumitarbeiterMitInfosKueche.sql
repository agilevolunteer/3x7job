select ab.value, ud.*
from __PREFIX__anmeldung a
inner join __PREFIX__userdata ud
on a.MitarbeiterID = ud.id
inner join __PREFIX__abbau ab
on a.AbbauBereich = ab.ID
where a.jahr = __YEAR__ and AufAbbau = true and a.AbbauBereich = 1
order by ab.value, ud.Nachname, ud.Vorname
