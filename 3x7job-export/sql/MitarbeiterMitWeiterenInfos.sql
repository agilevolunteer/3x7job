select ud.*, a.FSErfahrung as 'Erfahrung', a.bemerkungen as 'Bemerkungen', IF(a.AufAbbau= b'001','Ja','Nein') as 'Aufbau', IF(a.Abbau= b'001','Ja','Nein') as 'Abbau'
from __PREFIX__bereich4ma bma
inner join __PREFIX__anmeldung a
on bma.Ma = a.MitarbeiterID and bma.Jahr = a.Jahr
inner join __PREFIX__userdata ud
on bma.Ma = ud.id
inner join fsma_bereichsleiter bl
on bl.BID_Bereiche = bma.BID
where bl.Uid_Users = __BLID__ and a.Jahr = __YEAR__
