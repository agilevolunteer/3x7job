SELECT b.Bezeichnung, u.*, s.value as 'Status'
FROM __PREFIX__bereich4ma b4m
INNER JOIN __PREFIX__userdata u
ON b4m.Ma = u.id
INNER JOIN __PREFIX__status s
ON b4m.status_id = s.id
INNER JOIN __PREFIX__bereiche b
ON b4m.BID = b.BID
WHERE b4m.Jahr = 2012
ORDER BY b.Bezeichnung, u.Nachname, u.Vorname
