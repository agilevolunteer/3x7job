select distinct a.Bett, ud.*
from __PREFIX__bereich4ma b4m
inner join __PREFIX__mitarbeiterpools p
on b4m.PoolID = p.PoolId and p.jahr = b4m.Jahr
inner join __PREFIX__anmeldung a
on p.MitarbeiterID = a.MitarbeiterID and a.Jahr = p.Jahr
inner join __PREFIX__userdata ud
on ud.id = a.MitarbeiterID
where b4m.Jahr = __YEAR__ and a.Bett = true and b4m.BID != __CANCELEDPOOL__
order by ud.Nachname, ud.Vorname