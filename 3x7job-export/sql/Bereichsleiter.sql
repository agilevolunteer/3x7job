SELECT b.Bezeichnung, u.* 
FROM __PREFIX__bereiche b
INNER JOIN __PREFIX__bereichsleiter bl2b
ON b.BID = bl2b.BID_Bereiche
INNER JOIN __PREFIX__userdata u
ON bl2b.Uid_users = u.id
ORDER BY b.Bezeichnung
