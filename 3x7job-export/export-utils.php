<?php 
function x7exportDoExport()
{
	//echo "Hier werden jetzt ganz tolle Exceldateien erzeugt :D";	
	//x7exportWriteToExcelFile();
	//FsmaMessage("Guckstu Datei <a href=\"".X7EXPORTDOWNLOADURL."dinbgs.xls\">File</a>" );
	$_SESSION["x7DoExport"] = 0;
	if ($_SESSION["x7ExportDone"] == 0)
	{		
		$_SESSION["x7DoExport"] = $_SESSION["x7ExportDoExportOUT"];
		//echo '<meta http-equiv="refresh" content="0;">';
		 
		echo "<script type='text/javascript'> window.location = '".FsmaAddUrlParam()."'	</script>";
		//echo "<script type='text/javascript'> window.location = 'http://google.de'</script>";
		FsmaMessage("Die Datei kann jetzt heruntergeladen werden: <a href='".FsmaAddUrlParam()."'>Download</a>");	
			
	}
}

function x7writeExcelFile()
{	
	$_SESSION["x7ExportDone"] = 0;
		
	if ($_SESSION["x7DoExport"] != 0)	
	{	
		$id = $_SESSION["x7DoExport"];		
		$_SESSION["x7DoExport"] = 0;
		$_SESSION["x7ExportDone"] = 1;				
		x7exportWriteToExcelFile($id);
		
	}
}

function x7exportWriteToExcelFile($exportID)
{		
	global $table_prefix;
	$CurrentUser = wp_get_current_user();
	
	$query = new x7Template(X7EXPORTSQL."GetQueryFile.sql", X7EXPORTPATH);
	$queryParams = array();
	$queryParams["__PREFIX__"] = $table_prefix;
	$queryParams["__ID__"] = $exportID;
	
	$fileResult = $query->DoMultipleQuery(true, $queryParams, ARRAY_A);

	$file = $fileResult[0][0];
	$exportParams = array();
	$exportParams["__YEAR__"] 			= get_option('FsmaJahr');
	$exportParams["__CANCELEDPOOL__"] 	= get_option('FsmaCanceledPool');
	$exportParams["__PREFIX__"] 		= $table_prefix;
	$exportParams["__BLID__"] 			= $CurrentUser->ID;
	
	$export = new x7Template(X7EXPORTSQL.$file["file"], X7EXPORTPATH);
	$results = $export->DoMultipleQuery(true, $exportParams, ARRAY_A);
	
	//echo "muhg";
	
	if (count($results[0]) == 0)
	{
		FsmaError('Der Export brachte keine Ergebnismenge. Es steht keine Datei zum Download bereit');
	}
	else
	{
		$name = str_replace(" ", "", $fileResult[0][0]["display"]);
			
		x7exportMakeXlsHeader($name);
		x7exportArrayToXls($results[0]);
	    x7exportMakeXlsFooter();
	}    
}

function x7exportArrayToXls($array)
{
	$keys = array_keys($array[0]);
			
	//Spaltenueberschriften
	for ($i=0; $i<=count($keys); $i++)
	{
		//$csv .= '"'.$keys[$i].'",';
		xlsWriteLabel(0,$i,$keys[$i]);			
	}
		
	for ($i = 0; $i <= count($array); $i++)
	{
		$Ma = $array[$i];

		for ($j = 0; $j < count($Ma); $j++)
		{
			//$csv .= '"'.$Ma[$keys[$j]].'",';
			xlsWriteLabel($i+1,$j,$Ma[$keys[$j]]);	
		}		
	}	
}

//BEGIN SECTION
//These functions are taken from http://www.appservnetwork.com/modules.php?name=News&file=article&sid=8

function xlsBOF() {
    echo pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);  
    return;
}

function xlsEOF() {
    echo pack("ss", 0x0A, 0x00);
    return;
}

function xlsWriteNumber($Row, $Col, $Value) {
    echo pack("sssss", 0x203, 14, $Row, $Col, 0x0);
    echo pack("d", $Value);
    return;
}

function xlsWriteLabel($Row, $Col, $Value ) {
    $L = strlen($Value);
    echo pack("ssssss", 0x204, 8 + $L, $Row, $Col, 0x0, $L);
    echo $Value;
return;
} 
//END SECTION

function x7exportMakeXlsHeader($filename)
{
	 	// Send Header
    header("Pragma: public");
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Content-Type: application/force-download");
    header("Content-Type: application/octet-stream");
    header("Content-Type: application/download");;
    header("Content-Disposition: attachment;filename=$filename.xls "); 
    header("Content-Transfer-Encoding: binary ");

    // XLS Data Cell

    xlsBOF();
}

function x7exportMakeXlsFooter()
{
	xlsEOF();
    exit();
}
?>