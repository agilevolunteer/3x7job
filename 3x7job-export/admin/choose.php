<?php 
global $table_prefix;
if (isset($_GET["xpID"]))
{
	x7RegisterAction("x7ExportDoExport", jfGet("xpID"));		
}

$chooseHeader       = new x7Template(X7EXPORTTPL."ChooseHeader.tpl", X7EXPORTPATH);
$chooseItem   	    = new x7Template(X7EXPORTTPL."ChooseItem.tpl", X7EXPORTPATH);
$chooseAltItem      = new x7Template(X7EXPORTTPL."ChooseAltItem.tpl", X7EXPORTPATH);
$chooseFooter       = new x7Template(X7EXPORTTPL."ChooseFooter.tpl", X7EXPORTPATH);
$export 			= new x7Template(X7EXPORTSQL."GetAvailableExports.sql", X7EXPORTPATH);
	
$results = $export->DoMultipleQuery(true, array("__PREFIX__" => $table_prefix), ARRAY_A);

echo $chooseHeader->GetFilteredContent(array(), true);

for ($i=0; $i < count($results[0]); $i++)
{
	$singleExport = $results[0][$i];
	
	if(current_user_can($singleExport["capa"]))
	{
		$params = array();
		$params["__URL__"]        	= FsmaAddUrlParam("xpID",$singleExport["id"]) ;
		$params["__DISPLAYNAME__"]  = $singleExport["display"];
		$params["__ID__"]  			= $singleExport["id"];
		
		echo $chooseItem->GetFilteredContent($params, true);
	}		
}

echo $chooseFooter->GetFilteredContent(array(), true);


?>
