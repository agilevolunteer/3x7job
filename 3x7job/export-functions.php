<?php
function FsmaExportFileOptions($path = "")
{
	if (empty($path))
	{
		$path = FSMAPATH."sql/export";
	}
	
	$options 		= "";
	$dir 			= opendir($path);
	$option 		= new x7Template(X7TPL."option.tpl");

	while ( $file = readdir ( $dir ) )
	{
		if (substr($file,0,1)!=".")
		{
			$params = array();
			$params["__VALUE__"] =  $file;
			$params["__TEXT__"]  =  FsmaFilenameToText($file);
			$options .= $option->GetFilteredContent($params, true);
		}
	}
	
	closedir($dir);
	return $options;
}

function FsmaFilenameToText($filename)
{
	$noExtension = explode(".",$filename);
	
	return str_replace("_", " ", $noExtension[0]);
}

function FsmaArrayToCsv($array = array())
{
	$keys = array_keys($array[0]);
	$csv = "";
	
	//Spaltenueberschriften
	for ($i=0; $i<=count($keys); $i++)
	{
		$csv .= '"'.$keys[$i].'",';	
			
	}
	$csv .= chr(10);
	
	for ($i = 0; $i <= count($array); $i++)
	{
		$Ma = $array[$i];

		for ($j = 0; $j < count($Ma); $j++)
		{
			$csv .= '"'.$Ma[$keys[$j]].'",';
		}

		$csv .= chr(10);
	}
	
	return $csv;
}











?>