<?php
	include(FSMAPATH."export-functions.php");
	global $wpdb;
	global $table_prefix;

	$Choice			       		= new x7Template(X7TPL."BackendExportChoice.tpl");
	$Params 					= array();
	$Params['__ACTIONURL__'] 	= FsmaAddUrlParam();
	$Params['__OUTPUT__'] 		= "";
	$Params['__FILEOPTIONS__'] 	= FsmaExportFileOptions();
	
	if (isset($_POST["export"])) 
	{
		$output 			= "";
		$AlleMA       		= new jfbremenTemplate(FSMASQL."export/".$_POST["queries"]);
		$CurrentUser  		= wp_get_current_user();
		$BID				= FsmaGetBereichByLeiter($CurrentUser->ID, "BID");
		$DismissedStatus    = get_option("FsmaDismissedStatus");
		$where				= $_SESSION["where"];
		
		$SqlParams 					= array();		
		$SqlParams["__prefix__"]  	= $table_prefix;
		$SqlParams["__LIMIT__"]   	= "";
		$SqlParams["__ORDER__"]   	= (isset($_GET["sort"])) ? str_replace("-", " ",$_GET["sort"]) :"Bereich, AltBereich, Name";
		$SqlParams["__JAHR__"]    	= (empty($Jahr)) ? get_option('FsmaJahr') : $Jahr;

		//Nur die MA  anzeigen, die gesehen werden d�fen, NeuLink, nur wenn man  alle MA  administrieren darf
		if (current_user_can('read_all_ma'))
		{
			$SqlParams["__WHERE__"]   = "$where 1=1";
		}
		else
		{
			$SqlParams["__WHERE__"]   = "$where ((bpr.BID = $BID AND mpr.status_id <> $DismissedStatus) or (balt.BID = $BID AND malt.status_id <> $DismissedStatus))";
		}

		$result = $AlleMA->DoMultipleQuery(true, $SqlParams, ARRAY_A);

		$output = FsmaArrayToCsv($result[0]);
		$Params['__OUTPUT__'] 		= $output;
		$Params['__FILEPATH__'] 	= "../wp-content/export/Export-".$CurrentUser->user_nicename.".csv";
		//den ganzen kram nicht nur anzeigen, sondern auch in ner datei zum download anbieten...

		$file = fopen($Params['__FILEPATH__'], "w+");
		fputs($file, $output);
		fclose($file);
	}

		
	echo $Choice->GetFilteredContent($Params, true);
	
?>