<?php
// Liste von 2008
$AlleMA       = new jfbremenTemplate(FSMASQL."AdminAlleMA.sql");
$SurParams                     = array();
$SurParams["__CleanSortUrl__"] = $cleanSortUrl;
$SurParams["__FSMAURL__"]      =  FSMAURL;

$SqlParams = array();
$SqlParams["__prefix__"]  = $table_prefix;
$SqlParams["__LIMIT__"]   = "";
$SqlParams["__ORDER__"]   = (isset($_GET["sort"])) ? str_replace("-", " ",$_GET["sort"]) :"Bereich, AltBereich, Name";
$SqlParams["__JAHR__"]    = (empty($Jahr)) ? get_option('FsmaJahr') : $Jahr;
$DismissedStatus          = get_option("FsmaDismissedStatus");
$NewStatus                = get_option("FsmaDefStatus");
$AcceptStatus             = get_option("FsmaAngenommenStatus");

//Nur die MA  anzeigen, die gesehen werden d�fen, NeuLink, nur wenn man  alle MA  administrieren darf
if (current_user_can('read_all_ma'))
{
	$SqlParams["__WHERE__"]   = "$where 1=1";
	$SurParams["__newlink__"] = "";//FsmaAddUrlParam("UID",-1);
}
else
{
	$SqlParams["__WHERE__"]   = "$where ((bpr.BID = $BID AND mpr.status_id <> $DismissedStatus) or (balt.BID = $BID AND malt.status_id <> $DismissedStatus))";
	$SurParams["__newlink__"] = "";
}


$result = $AlleMA->DoMultipleQuery(true, $SqlParams, ARRAY_A);

if (count($result[0])>0)
{
	//Tabelle anzeigen, wenn Datens�tze vorhanden
	$Header       = new x7Template(X7TPL."BackendMAHeader.tpl");
	$Footer       = new x7Template(X7TPL."BackendMAFooter.tpl");
	$ListItem     = new x7Template(X7TPL."BackendMAListItem.tpl");
	$ListItemAlt  = new x7Template(X7TPL."BackendMAListItemAlt.tpl");
	$alt = false;
	$oldOrder = "";

	echo $Header->GetFilteredContent($SurParams, true);
	for ($i=0;$i<count($result[0]);$i++)
	{
		$SingleMA = $result[0][$i];

		if ((($SingleMA["PrBID"] == $BID) || (($SingleMA["AltBID"] == $BID) && ($SingleMA["Status"] == $DismissedStatus))) || (current_user_can('edit_all_ma')))
		{
			$params = array();
			$params["__FSMAURL__"]        =  FSMAURL;
			$params["__morelink__"]       =  FsmaAddUrlParam("UID",$SingleMA["id"]) ;
			$params["__morelink__"]       =  (empty($Jahr)) ? $params["__morelink__"] : FsmaAddUrlParam("jahr",$Jahr, $params["__morelink__"]) ;
			$params["__Name__"]           =  $SingleMA["Name"];
			$params["__Bereich__"]        =  $SingleMA["Bereich"];
			$params["__AltBereich__"]     =  $SingleMA["AltBereich"];
			$params["__Eingecheckt__"]    =  $SingleMA["Eingecheckt"];
			$params["__Bezahlt__"]        =  $SingleMA["Bezahlt"];
			$params["__AID__"]        =  $SingleMA["aid"];
				
			//F�r die StatusIcons
			$params["__Dismissed__"]      =  ($SingleMA["Status"]    == $DismissedStatus);
			$params["__AltDismissed__"]   =  ($SingleMA["AltStatus"] == $DismissedStatus);
				
			$params["__New__"]            =  ($SingleMA["Status"]    == $NewStatus);
			$params["__AltNew__"]         =  ($SingleMA["AltStatus"] == $NewStatus);
				
			$params["__Accept__"]         =  ($SingleMA["Status"]    == $AcceptStatus);
			$params["__AltAccept__"]      =  ($SingleMA["AltStatus"] == $AcceptStatus);
				
				
			$params["__Changed__"]        = ($SingleMA["aid"] == $_GET["aid"]) ? "change" : "";
				
			//Link zum Bearbeiten des Eingechecktstatus
			$params["__EditEingechecktUrl__"] = FsmaAddUrlParam("chg", "check");
			$params["__EditEingechecktUrl__"] = FsmaAddUrlParam("aid", $SingleMA["aid"], $params["__EditEingechecktUrl__"]);
			$params["__EditEingechecktUrl__"] = ($SingleMA["Eingecheckt"]) ? FsmaAddUrlParam("val", 0, $params["__EditEingechecktUrl__"]) : FsmaAddUrlParam("val", 1, $params["__EditEingechecktUrl__"]);
				
			//Link zum Bearbeiten des Bezahlstatus
			$params["__EditBezahltUrl__"] = FsmaAddUrlParam("chg", "zahl");
			$params["__EditBezahltUrl__"] = FsmaAddUrlParam("aid", $SingleMA["aid"], $params["__EditBezahltUrl__"]);
			$params["__EditBezahltUrl__"] = ($SingleMA["Bezahlt"]) ? FsmaAddUrlParam("val", 0, $params["__EditBezahltUrl__"]) : FsmaAddUrlParam("val", 1, $params["__EditBezahltUrl__"]);
				
			$ThisOrderExp = FsmaSwitchOrderExp($orderExp, $SingleMA[$orderExp]);
				
			if ($showGroups)
			{
				if ($ThisOrderExp  == $oldOrder)
				{
					$params["__ORDEREXP__"]       =  "";
				}
				else
				{
					$params["__ORDEREXP__"]       =  $ThisOrderExp ;
					$alt = false;
				}
			}
			else
			{
				$params["__ORDEREXP__"]       =  "";
			}
				
				
			$oldOrder                     =  $ThisOrderExp ;
				
			if (!$alt)
			{
				echo $ListItem->GetFilteredContent($params);
			}
			else
			{
				echo $ListItemAlt->GetFilteredContent($params);
			}
				
			$alt = !$alt;
		}
	}

	echo $Footer->GetFilteredContent($SurParams, true);
}

?>