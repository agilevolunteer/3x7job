<?php
function FsmaGetMaList4DropDown($text)
{
	$optionsString = "";
	$option = new x7Template(X7TPL."option.tpl");
	$params = array();
	
	//Vorm Freakstock	
	$params["__VALUE__"] =  "beforeFestival";
	$params["__TEXT__"]  =  "Vor dem Event";
	$params["__SELECTED__"]  =  ($params["__VALUE__"] == $text) ? 'SELECTED="selected"' : '';
	$optionsString .= $option->GetFilteredContent($params, true);
	
	//W�hrend Freakstock
	$params["__VALUE__"] =  "duringFestival";
	$params["__TEXT__"]  =  "W&auml;hrend des Events";
	$params["__SELECTED__"]  =  ($params["__VALUE__"] == $text) ? 'SELECTED="selected"' : '';
	$optionsString .= $option->GetFilteredContent($params, true);
	
	return $optionsString;
}

	$validator = new jfbremenValidate();
	$valid = true;
	$error = new x7Template(X7TPL."BackendError.tpl");
	global $table_prefix;
	
	//Validate FsmaTermin
	if ($validator->isDate($_POST['FsmaTermin']) == false)
	{
		$valid = false;		
		$errorParams["__TEXT__"] = "Kein g&uuml;ltiges Datum f&uuml;r <b>Erster Freakstocktag:</b>!";
	}
	
	if ((isset($_POST['pagesIdent'])) && ($_POST['pagesIdent'] == "FsmaOptions"))
	{
		if ($valid == true)
		{
			//$Message       = new jfbremenTemplate(FSMATPL."BackendMessage.tpl");
			update_option('FsmaJahr',                 $_POST['FsmaJahr']);
			update_option('FsmaDismissedPool',        $_POST['FsmaDismissedPool']);
			update_option('FsmaCanceledPool',         $_POST['FsmaCanceledPool']);
			update_option('FsmaTermin',               $_POST['FsmaTermin']);
			update_option('FsmaActive',               isset($_POST['FsmaActive']));
			update_option('FsmaOfflineText',          $_POST['FsmaOfflineText']);
			update_option('FsmaSuccessText',          $_POST['FsmaSuccessText']);
			update_option('FsmaSuccessUpdateText',    $_POST['FsmaSuccessUpdateText']);
			update_option('FsmaDefStatus',            $_POST['FsmaDefStatus']);
			update_option('FsmaDismissedStatus',      $_POST['FsmaDismissedStatus']);
			update_option('FsmaAngenommenStatus',     $_POST['FsmaAngenommenStatus']);
			
			update_option('FsmaSuccessMailBody',      $_POST['FsmaSuccessMailBody']);
			update_option('FsmaSuccessMailBetreff',   $_POST['FsmaSuccessMailBetreff']);
			update_option('FsmaMailAbsender',         $_POST['FsmaMailAbsender']);
			update_option('FsmaMaList',			      $_POST['FsmaMaList']);
			update_option('FsmaPageItemCount',		  $_POST['FsmaPageItemCount']);
			update_option('FsmaAngenommenMailText',		  $_POST['FsmaAngenommenMailText']);
			update_option('FsmaAngenommenMailBetreff',		  $_POST['FsmaAngenommenMailBetreff']);
			update_option('FsmaAngenommenMailMitAufbauText',		  $_POST['FsmaAngenommenMailMitAufbauText']);
			update_option('FsmaIsBedActive',               isset($_POST['FsmaIsBedActive']));
			
			//View f�r aktuellen Bereich von Mitarbeitern anpassen
			$view = new jfbremenTemplate(FSMASQL."ViewAktuellerBereich4Ma.sql");
			$viewparam = array();
			$viewparam["__jahr__"] 			= $_POST['FsmaJahr'];
			$viewparam["__prefix__"] 		= $table_prefix;
			$viewparam["__abgewiesen__"] 	= $_POST['FsmaDismissedStatus'];
			$viewparam["__angenommen__"] 	= $_POST['FsmaDefStatus'];
			
			$view->DoMultipleQuery(true, $viewparam);
			
			echo FsmaMessage("Einstellungen wurden gespeichert");
		}
		else
		{
			echo $error->GetFilteredContent($errorParams, true);
		}
	}
	if ((isset($_POST['pagesIdent'])) && ($_POST['pagesIdent'] == "FsmaStatus"))
	{
		$Message       = new x7Template(X7TPL."BackendMessage.tpl");
		FsmaInsertNewStatus($_POST["newStatus"]);
		$MessageParams = array();
		$MessageParams["__TEXT__"] = "Status <i>".$_POST["newStatus"]."</i> wurde hinzugef&uuml;gt";
		
		echo $Message->GetFilteredContent($MessageParams, true);		
	}
	$OptionParams = array();
	$OptionParams["__BereichsOptions__"]           = FsmaGetBereiche4DropDown(get_option('FsmaDismissedPool'));
	$OptionParams["__AbgesagtBereichsOptions__"]   = FsmaGetBereiche4DropDown(get_option('FsmaCanceledPool'));
	$OptionParams["__StatusOptions__"]             = FsmaGetStatus4DropDown(get_option('FsmaDefStatus'));
	$OptionParams["__DismissedStatusOptions__"]    = FsmaGetStatus4DropDown(get_option('FsmaDismissedStatus'));
	$OptionParams["__AngenommenStatusOptions__"]   = FsmaGetStatus4DropDown(get_option('FsmaAngenommenStatus'));
	$OptionParams["__FsmaJahr__"]                  = get_option('FsmaJahr');
	$OptionParams["__FsmaTermin__"]                = get_option('FsmaTermin');
	$OptionParams["__FsmaOfflineText__"]           = get_option('FsmaOfflineText');
	$OptionParams["__FsmaSuccessText__"]           = get_option('FsmaSuccessText');
	$OptionParams["__FsmaSuccessUpdateText__"]     = get_option('FsmaSuccessUpdateText');
	$OptionParams["__FsmaActive__"]                = (get_option('FsmaActive') == 1) ? 'checked' : '';
	$OptionParams["__FsmaIsBedActive__"]                = (get_option('FsmaIsBedActive') == 1) ? 'checked' : '';
	$OptionParams["__FsmaVersion__"]               = get_option('FsmaVersion')." | DB v".get_option('FsmaDbVersion');
	$OptionParams["__ACTION__"]                    = FsmaAddUrlParam();
	$OptionParams["__FsmaSuccessMailBody__"]       = get_option('FsmaSuccessMailBody');
	$OptionParams["__FsmaMailAbsender__"]          = get_option('FsmaMailAbsender');
	$OptionParams["__FsmaSuccessMailBetreff__"]    = get_option('FsmaSuccessMailBetreff');
	$OptionParams["__FsmaMaList__"]         	   = FsmaGetMaList4DropDown(get_option('FsmaMaList'));
	$OptionParams["__FsmaPageItemCount__"]    	   = get_option('FsmaPageItemCount');
	$OptionParams["__FsmaAngenommenMailText__"]	= get_option('FsmaAngenommenMailText');
	$OptionParams["__FsmaAngenommenMailBetreff__"]	= get_option('FsmaAngenommenMailBetreff');
	$OptionParams["__FsmaAngenommenMailMitAufbauText__"]	= get_option('FsmaAngenommenMailMitAufbauText');
	
	$OptionParams["__x7URL__"]    	   			   = FSMAURL;
	
	//BackendManageOptions.tpl
	$ManageOptions = new x7Template(X7TPL."BackendManageOptions.tpl");
	echo $ManageOptions->GetFilteredContent($OptionParams);
?>
