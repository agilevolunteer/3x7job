<?php
//Anmeldedaten
$Anmeldung = new x7Template(X7SQL."MitarbeiterAnmeldung.sql");
$AnmeldungParams = array();
$AnmeldungParams["__Mitarbeiter__"] = $UID;
$AnmeldungParams["__Jahr__"]        = (empty($Jahr)) ? get_option('FsmaJahr') : $Jahr;
$AnmeldungParams["__prefix__"]      = $table_prefix;
$AnmeldungParams["__WsBereich__"]   = get_option('AgvSection4Workshop');

$AnmeldeData = $Anmeldung->DoMultipleQuery(true, $AnmeldungParams);
$Data = $AnmeldeData[0][0];
$DismissedPoolArray = FsmaGetDismissedBereich4DropDown($UID, $AnmeldungParams["__Jahr__"], $Data->Bereich, true);
$DismissedPool = $DismissedPoolArray[0];

$canReadView = false;
if(current_user_can('read_all_ma')){
	$canReadView = true;
} elseif (in_array($Data->Bereich, $BID )){
	$canReadView = true;
} elseif (in_array($Data->AltBereich, $BID )){
	$canReadView = true;
} elseif (in_array($DismissedPool->BID_Bereiche, $BID )){
	$canReadView = true;
}


//if (((current_user_can('read_own_ma')) && (($Data->Bereich == $BID) || ($Data->AltBereich == $BID)|| ($DismissedPool->BID_Bereiche == $BID))) || (current_user_can('read_all_ma')))
if($canReadView)
{
	//Persönliche Daten
	$PersDataTemplate  = new x7Template(X7TPL."BackendMitarbeiterPersData.tpl");
	$AnmeldungTemplate = new x7Template(X7TPL."BackendMitarbeiterAnmeldung.tpl");
	$CustomFieldsTemplate    = new x7Template(X7TPL."BackendMitarbeiterCustomFields.tpl");
	$PersDataParams    = array();
	$AnmeldeParams   = array();
	$CustomFieldParams = array();
	$MA = get_userdata($UID);
	//echo $MA->user_email;
	//$Anmeldung->printDebugArray($MA);
	$PersDataParams["__email__"]       = $MA->user_email;
    //jfPrintDebugArray($MA);
	if ($fehler==true)
	{
		$PersDataParams["__gebdat__"]        = $_POST["gebdat"];
		$PersDataParams["__Handy__"]         = $_POST["Handy"];
		$PersDataParams["__VorwahlHandy__"]  = $_POST["VorwahlHandy"];
		$PersDataParams["__Tele__"]          = $_POST["Tele"];
		$PersDataParams["__VorwahlTele__"]   = $_POST["VorwahlTele"];
		$PersDataParams["__ort__"]           = $_POST["ort"];
		$PersDataParams["__plz__"]           = $_POST["plz"];
		$PersDataParams["__strassehausnr__"] = $_POST["strassehausnr"];
		$PersDataParams["__nachname__"]      = $_POST["nachname"];
		$PersDataParams["__vorname__"]       = $_POST["vorname"];
		$PersDataParams["__Anmerkungen__"]       = $_POST["Anmerkungen"];
		$PersDataParams["__food__"]          = x7GetFood4DropDown($_POST["food"]);
	}
	else
	{
		$PersDataParams["__gebdat__"]        = $MA->gebdat;
		$PersDataParams["__Handy__"]         = $MA->Handy;
		$PersDataParams["__VorwahlHandy__"]  = $MA->VorwahlHandy;
		$PersDataParams["__Tele__"]          = $MA->Tele;
		$PersDataParams["__VorwahlTele__"]   = $MA->VorwahlTele;
		$PersDataParams["__ort__"]           = $MA->ort;
		$PersDataParams["__plz__"]           = $MA->zip;
		$PersDataParams["__strassehausnr__"] = $MA->strassehausnr;
		$PersDataParams["__nachname__"]      = $MA->last_name;
		$PersDataParams["__vorname__"]       = $MA->first_name;
		$PersDataParams["__Anmerkungen__"]   = $MA->Anmerkungen;
		$PersDataParams["__food__"]          = x7GetFood4DropDown($MA->food);
	}



	//$Anmeldung->printDebugArray($Data);
	//Angaben evtl. aus DB lesen

	

	$AnmeldeParams["__AnmeldeID__"]    			= $Data->id;
	$AnmeldeParams["__BereichOptions__"]    	= FsmaGetBereiche4DropDown($Data->Bereich);
	$AnmeldeParams["__AltBereichOptions__"] 	= FsmaGetBereiche4DropDown($Data->AltBereich);
	$AnmeldeParams["__AufbauBereichOptions__"]     = x7GetAufbauBereiche4DropDown($Data->AufbauBereich);
	$AnmeldeParams["__FsErfahrungen__"]     	= $Data->FSErfahrung;
	$AnmeldeParams["__Mitgearbeitet__"]     	= ($Data->BereitsMitgearbeitet==true) ? "checked" : "";
	$AnmeldeParams["__AufbauAbbau__"]       	= ($Data->AufAbbau==true) ? "checked" : "";
	$AnmeldeParams["__Abbau__"]       	= ($Data->Abbau==true) ? "checked" : "";
	$AnmeldeParams["__Bett__"]       			= ($Data->Bett==true) ? "checked" : "";
	$AnmeldeParams["__AufbauAbbauConfirm__"]    = ($Data->AufAbbauConfirm==true) ? "checked" : "";
	$AnmeldeParams["__Eingecheckt__"]       	= ($Data->Eingecheckt==true) ? "checked" : "";
	$AnmeldeParams["__Bezahlt__"]           	= ($Data->Bezahlt==true) ? "checked" : "";
	$AnmeldeParams["__Bemerkungen__"]       	= $Data->bemerkungen;


	$AnmeldeParams["__Jahr__"] 		      = $AnmeldungParams["__Jahr__"];
	$AnmeldeParams["__BereichStatusOptions__"]    = FsmaGetStatus4DropDown($Data->PoolStatus);
	$AnmeldeParams["__AltBereichStatusOptions__"] = FsmaGetStatus4DropDown($Data->AltPoolStatus);
	$AnmeldeParams["__DismissedBereichOptions__"] = $DismissedPoolArray[1];
	if (current_user_can("edit_all_ma"))
	{
		$AnmeldeParams["__PrPool__"]            = $Data->PrPool;
		$AnmeldeParams["__AltPool__"]           = $Data->AltPool;
		$AnmeldeParams["__DismissedPool__"]     = $DismissedPool->PoolID;
	}
	else
	{
		//$AnmeldeParams["__PrPool__"]            = $Data->PrPool;
		//$AnmeldeParams["__AltPool__"]           = $Data->AltPool;
		//$AnmeldeParams["__DismissedPool__"]     = $DismissedPool->PoolID;
		$AnmeldeParams["__PrPool__"]            = (current_user_can('edit_own_ma') && in_array($Data->Bereich, $BID))               ? $Data->PrPool : "" ;
		$AnmeldeParams["__AltPool__"]           = (current_user_can('edit_own_ma') && in_array($Data->AltBereich,$BID))            ? $Data->AltPool : "" ;
		$AnmeldeParams["__DismissedPool__"]     = (current_user_can('edit_own_ma') && in_array($DismissedPool->BID_Bereiche, $BID)) ? $Data->DismissedPool : "" ;
			
		$AnmeldeParams["__EnablePrPool__"]            = (current_user_can('edit_own_ma') && in_array($Data->Bereich, $BID))               ? "" : "disabled";
		$AnmeldeParams["__EnableAltPool__"]           = (current_user_can('edit_own_ma') && in_array($Data->AltBereich, $BID))           ? "" : "disabled";
		$AnmeldeParams["__EnableDismissedPool__"]     = (current_user_can('edit_own_ma') && in_array($DismissedPool->BID_Bereiche, $BID)) ? "" : "disabled";
		
		if ($AnmeldeParams["__EnablePrPool__"] != "disabled")
			$AnmeldeParams["__BereichOptions__"]  	= FsmaGetBereiche4DropDown($Data->Bereich, "BID in( $bidList)");
			
		if ($AnmeldeParams["__EnableAltPool__"] != "disabled")
			$AnmeldeParams["__AltBereichOptions__"] 	= FsmaGetBereiche4DropDown($Data->AltBereich, "BID in ( $bidList)");
	}

	if ($AnmeldeParams["__DismissedPool__"] == $AnmeldeParams["__PrPool__"])
		$AnmeldeParams["__DismissedPool__"] = "";
	//CustomFields
	$CustomItems = "";
		
	for($i = 0; $i < count($BID); $i++){
		$CustomItems .= FsmaParseCustomFieldsBackend($BID[$i], -1, $UID);	
	}
	 
	
	$FieldsOutput = "";

	if (!empty($CustomItems))
	{
		$CustomFieldParams = array("__ACTION__" => FsmaAddUrlParam());
		$CustomFieldParams["__CustomFieldItems__"] = $CustomItems;
		$FieldsOutput = $CustomFieldsTemplate->GetFilteredContent($CustomFieldParams, true);
		//break;
	}

	//Ausgabe
	echo $PersDataTemplate->GetFilteredContent($PersDataParams, true);
	echo $FieldsOutput;
	echo $AnmeldungTemplate->GetFilteredContent($AnmeldeParams, true);
}


else
{
	$Fehler       = new x7Template(X7TPL."BackendError.tpl");
	$FehlerParams = array();
	$FehlerParams["__TEXT__"] = "Du hast nicht die Berechtigung diesen Mitarbeiter zu sehen!";
	echo $Fehler->GetFilteredContent($FehlerParams);
}


/*	{

$Anmeldungw = new jfbremenTemplate(FSMATPL."BackendAnmeldungDetailView.tpl");
$params = array();
$params["__BID__"]                    =  $BID;
$params["__ACTION__"]		          =  FsmaEraseUrlParam("BID",FsmaAddUrlParam());//FsmaEraseUrlParam
$params["__BACKURL__"]                =  FsmaEraseUrlParam("BID",FsmaAddUrlParam());

if ($BID == -1)
{
$params["__SingleMAsleiterOptions__"]  =  FsmaGetUsers4DropDown();
$params["__Bezeichnung__"]            = "&lt;Neuer SingleMA&gt;";
}
$params["__Bezeichnung__"]            =  $SingleMA["Bezeichnung"];
$params["__SingleMAsleiterOptions__"]  =  FsmaGetUsers4DropDown($SingleMA["bereichsleiter"]);
$params["__SingleMAsleiter__"]         =  $SingleMA["bereichsleiter"];
$params["__Description__"]            =  $SingleMA["Description"];
$params["__Preview__"]                =  $SingleMA["Preview"];
$params["__Arbeitszeit__"]            =  $SingleMA["Arbeitszeit"];
$params["__FSK__"]                    =  $SingleMA["FSK"];
$params["__kommentar__"]              =  $SingleMA["kommentar"];
$params["__LOCKED__"]                 =  $SingleMA["locked"];
$params["__CustomFields__"]           =  $SingleMA["CustomFields"];
$params["__SingleMAsleiterEnabled__"]  =  (current_user_can('set_bereichsleiter')) ? "" : "disabled";

echo $DetailView->GetFilteredContent($params, true);
}*/
?>
