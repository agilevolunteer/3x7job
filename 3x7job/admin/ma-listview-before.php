<?php
	$DismissedStatus          = get_option("FsmaDismissedStatus");
	$NewStatus                = get_option("FsmaDefStatus");
	$AcceptStatus             = get_option("FsmaAngenommenStatus");

	//jfPrintDebugArray($_POST);
	if (isset($_POST["page"]) && $_POST["page"]=='listView')
	{
		if ($_POST["action"] == 'Annehmen')
		{
			FsmaChangePool($_POST["pid"], $_POST["bid"], $AcceptStatus);
			FsmaMessage("Mitarbeiter wurde angenommen.");
		}
		elseif($_POST["action"] == 'Ablehnen')
		{
			FsmaChangePool($_POST["pid"], $_POST["bid"], $DismissedStatus );
			FsmaMessage("Mitarbeiter wurde abgewiesen.");

			$dismissedJahr = (empty($_SESSION["FILTER"]["JAHR"]) || $_SESSION["FILTER"]["JAHR"] == -1) ? get_option('FsmaJahr') : $_SESSION["FILTER"]["JAHR"];
			if ((FsmaIsAllDismissed($_POST["uid"], $dismissedJahr)==true) && (empty($_POST["DismissedPool"])))
			{
				FsmaSendToDismissedPool($_POST["uid"], $dismissedJahr);
				FsmaMessage("Mitarbeiter wurde dem Schwimmbereich zugewiesen");
			}
		}
	}


	//SQL
	$SqlParams = array();
	$SqlParams["__prefix__"]  = $table_prefix;
	if ($startLimit != $endLimit && $endLimit != 0)
	{
		$SqlParams["__LIMIT__"]   = "$startLimit, $endLimit";
	}
	else
	{
		FsmaError("Die Option <b>Mitarbeiter pro Seite</b> wurde nicht gesetzt. <br>
		Es werden nur die ersten 10 Mitarbeiter anzeigt.<br> Option setzen um das Paging zu aktivieren!");
		$SqlParams["__LIMIT__"]   = "0,10";
	}
	$SqlParams["__ORDER__"]   = (isset($_GET["sort"])) ? str_replace("-", " ",$_GET["sort"]) :"Bereich";
	$SqlParams["__JAHR__"]    = (empty($_SESSION["FILTER"]["JAHR"])) ? get_option('FsmaJahr') : $_SESSION["FILTER"]["JAHR"];

	if (current_user_can('read_all_ma'))
		$SqlParams["__WHERE__"] = ((empty($_SESSION["FILTER"]["JAHR"])) || ($_SESSION["FILTER"]["BID"] == -1)) ? "(1=1)" : " (b.BID = ".$_SESSION["FILTER"]["BID"].")";
	else
	{
		//$BID 			        = FsmaGetBereichByLeiter($CurrentUser->ID, "BID");
		$SqlParams["__WHERE__"] = " (b.BID in ($bidList)) ";

	}

	$SqlParams["__WHERE__"] .= (empty($_POST["display_name"])) ? "" : (" AND u.display_name like '%".$_SESSION["FILTER"]["NAME"]."%'");

	$anmeldungenSQL = new jfbremenTemplate(FSMASQL."AlleAnmeldungenBefore.sql");

	$result = $anmeldungenSQL->DoMultipleQuery(true, $SqlParams, ARRAY_A);

	//Den ganzen Kram ausgeben
	//$SurParams kommen aus der ma-listview.php
	if (count($result[0])>0)
	{
		$noResult = false;
		$Header       = new x7Template(X7TPL."BackendMABeforeHeader.tpl");
		$Footer       = new x7Template(X7TPL."BackendMAFooter.tpl");
		$ListItem     = new x7Template(X7TPL."BackendMABeforeListItem.tpl");
		$ListItemAlt  = new x7Template(X7TPL."BackendMABeforeListAltItem.tpl");


		//Paging
		$SurParams["__NEXTPAGE__"]       = $paging + 1;
		$SurParams["__PREVIOUSPAGE__"]   = $paging - 1;

		echo $Header->GetFilteredContent($SurParams, true);
		$showAlt = false;
		//$showGroups = true;
		$oldOrder = "";
		$mitarbeiterID = -1;

		for ($i=0;$i<count($result[0]);$i++)
		{
			$SingleMA = $result[0][$i];
			if($mitarbeiterID == $SingleMA["ID"]){
				continue;
			}
			$mitarbeiterID = $SingleMA["ID"];

			$params = array();
			$params["__FSMAURL__"]        =  FSMAURL;
			$params["__morelink__"]       =  FsmaAddUrlParam("UID",$SingleMA["ID"]) ;
			$params["__morelink__"]		  =  FsmaAddUrlParam("jahr",$SqlParams["__JAHR__"],$params["__morelink__"]) ;
			$params["__Name__"]           =  $SingleMA["Name"];
			$params["__BID__"]            =  $SingleMA["BID"];
			$params["__Bereich__"]        =  $SingleMA["Bereich"];
			$params["__Bemerkungen__"]    =  $SingleMA["Bemerkungen"];
			$params["__ANRISS__"] 		  =  substr($SingleMA["Bemerkungen"],0,50)."...";
			$params["__mabemerkung__"]    =  get_user_meta($SingleMA["ID"], "Anmerkungen", true);
			$params["__mabemerkunganriss__"] 		  =  substr($params["__mabemerkung__"],0,50)."...";

			//F�r die StatusIcons
			$params["__Dismissed__"]      =  ($SingleMA["Status"]    == $DismissedStatus);
			$params["__New__"]            =  ($SingleMA["Status"]    == $NewStatus);
			$params["__Accept__"]         =  ($SingleMA["Status"]    == $AcceptStatus);
			$params["__PID__"]			  =  $SingleMA["PID"];
			$params["__UID__"]			  =  $SingleMA["ID"];
			$params["__AID__"]			  =  $SingleMA["AID"];

			//AufAbbau
			$params["__AufAbbau__"]       	=  $SingleMA["AufAbbau"];
			$params["__AufAbbauConfirm__"]  =  $SingleMA["Abbau"];
			//Link zum Bearbeiten des Bezahlstatus
			$params["__EditConfirmUrl__"] = FsmaAddUrlParam("chg", "conf");
			$params["__EditConfirmUrl__"] = FsmaAddUrlParam("aid", $SingleMA["AID"], $params["__EditConfirmUrl__"]);
			$params["__EditConfirmUrl__"] = ($SingleMA["AufAbbauConfirm"]) ? FsmaAddUrlParam("val", 0, $params["__EditConfirmUrl__"]) : FsmaAddUrlParam("val", 1, $params["__EditConfirmUrl__"]);

			$ThisOrderExp = FsmaSwitchOrderExp($orderExp, $SingleMA[$orderExp]);

			if ($showGroups )
			{
				if ($ThisOrderExp  == $oldOrder)
    		{
					$params["__ORDEREXP__"]       =  "";
				}
				else
				{
					$params["__ORDEREXP__"]       =  $ThisOrderExp ;
					$showAlt = false;

					if ($orderExp == "Bereich")
						$params["__ITEMCOUNT__"] = GetAnzahlMa4Bereich($params["__BID__"] , $SqlParams["__JAHR__"]);
					else
						$params["__ITEMCOUNT__"] = "";
				}
			}
			else
			{
				$params["__ORDEREXP__"]       =  "";
			}

			$oldOrder =  $ThisOrderExp ;

			if ($showAlt == true)
				echo $ListItemAlt->GetFilteredContent($params, true);
			else
				echo $ListItem->GetFilteredContent($params, true);

			$showAlt = !$showAlt;
		}

		echo $Footer->GetFilteredContent();
	}
?>
