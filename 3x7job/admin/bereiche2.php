<style>
button {
width: 75px;
height: 30px;
}
</style>
<?php

require_once FSMAPATH.'modules/bereiche-utils.php';
$CurrentUser = wp_get_current_user();
?>
	
<div class="wrap">
	<h2>Bereiche</h2>
	<table class="wp-list-table widefat plugins" cellpadding=0 cellspacing=0>
	<tr>
		<th>
			Name
		</th>
		<th>
			Kurzbeschreibung
		</th>
		<th>
			Anmeldungen m&ouml;glich
		</th>
        <th>Beschreibung sichtbar</th>
		<th>Löschen</th>
	</tr>
		<tbody data-bind="foreach: bereichsliste" id="the-list">
			<tr data-bind="attr: { 'class' : className }">
				<td><b><span data-bind="html: bezeichnung"></b></td>
				<td><span data-bind="html: preview"</td>
				<td>
					<button class="button agv-actions__button" data-bind="visible: geschlossen() == true, click: $root.changeLocked">Nein</button>
					<button class="button agv-actions__button" data-bind="visible: geschlossen() == false, click: $root.changeLocked">Ja</button>
				</td>			
                <td>
					<button class="button agv-actions__button" data-bind="visible: listensichtbar() == true, click: $root.changeDesc">Ja</button>
					<button class="button agv-actions__button" data-bind="visible: listensichtbar() == false, click: $root.changeDesc">Nein</button>
				</td>
				<td>
					<button class="button agv-actions__button" data-bind="visible: geloescht() == true, click: $root.changeDelete">Rückgängig</button>
					<button class="button agv-actions__button" data-bind="visible: geloescht() == false, click: $root.changeDelete">Löschen</button>

				</td>
			</tr>
		</tbody>
	</table>
	<div class="agv-actions agv-actions--small">
		<button class="agv-actions__button button button-small" data-bind="click: toggleDeleted">Alle anzeigen</button>
	</div>
</div>
<script type="text/javascript">
var wpajax = "<?php echo get_bloginfo("wpurl"); ?>/wp-admin/admin-ajax.php";
</script>
<script type="text/javascript" src="<?php echo X7URL; ?>/scripts/knockout.js"></script>
<script type="text/javascript" src="<?php echo X7URL; ?>/scripts/knockout-jquery-ui-widget.js "></script>
<script type="text/javascript" src="<?php echo X7URL; ?>/scripts/bereichsliste.js"></script>