<?php
require_once FSMAPATH.'modules/bereiche-utils.php';
$CurrentUser = wp_get_current_user();

if (isset($_GET["BID"]) && isset($_GET["bl"]))
{
	$b = jfGet("BID");
	$bl  = jfGet("bl");
	x7PromoteAsOnlySender($b, $bl);
}

//Bereich speichern
if (isset($_POST["BID"]))
{
	/*
	FsmaMessage("Dings");
	
	$BID = $_POST["BID"];
	$Message       = new x7Template(X7TPL."BackendMessage.tpl");
	$MessageParams = array();
	$MessageParams["__TEXT__"] = "Bereich <i>".$_POST["bezeichnung"]."</i> wurde gespeichert";
	*/
	$SqlParams                       = array();
	$SqlParams["__prefix__"]         = $table_prefix;
	$SqlParams["__BID__"]            = $_POST["BID"];
	$SqlParams["__bezeichnung__"]    = trim($_POST["bezeichnung"]);
	$SqlParams["__kurztext__"]       = trim($_POST["kurztext"]);
	$SqlParams["__langtext__"]       = trim($_POST["langtext"]);
	$SqlParams["__FSK__"]            = trim($_POST["FSK"]);
	$SqlParams["__Arbeitszeit__"]    = $_POST["Arbeitszeit"];
	$SqlParams["__MailNormal__"]     = trim($_POST["MailNormal"]);
	$SqlParams["__MailAufbau__"]     = trim($_POST["MailAufbau"]);
	$SqlParams["__bereichsleiter__"] = (isset($_POST["bereichsleiter"])) ? $_POST["bereichsleiter"] : $_POST["oldBereichsleiter"] ;
	$SqlParams["__CustomFields__"]   = trim($_POST["CustomFields"]);
	$SqlParams["__CustomHint__"]   = trim($_POST["CustomHint"]);
	
	$SqlParams["__locked__"] = (isset($_POST["locked"])) ? "b'001'" : "b'000'";
	$SqlParams["__VisInList__"] = (isset($_POST["VisInList"])) ? "b'001'" : "b'000'";
	
	/*
	if ($BID == -1)
	{
		$SaveBereich = new jfbremenTemplate(FSMASQL."InsertNewBereich.sql");
	}
	else 
	{
		$SaveBereich = new jfbremenTemplate(FSMASQL."UpdateBereich.sql");
	}
		
	//$result = $SaveBereich->DoMultipleQuery(true, $SqlParams, ARRAY_A, true);
	//echo $SaveBereich->GetFilteredContent($SqlParams, true);
	echo $Message->GetFilteredContent($MessageParams, true);
	*/
	x7RegisterAction("x7BackendBereicheSaveBereich", $SqlParams, $_POST);
}


//spezifische Anzeige
if (!isset($_GET["BID"]))
{
	//ListView
	$Header       = new x7Template(X7TPL."BackendBereicheHeader.tpl");
	$ListItem     = new x7Template(X7TPL."BackendBereicheListItem.tpl");
	$ListItemAlt  = new x7Template(X7TPL."BackendBereicheListItemAlt.tpl");
	$Footer       = new x7Template(X7TPL."BackendBereicheFooter.tpl");
	$AlleBereiche = new x7Template(X7SQL."AdminAlleBereiche.sql");

	$SurParams                = array();
	$SurParams["__X7URL__"]	= X7URL;
		
	$SqlParams = array();
	$SqlParams["__prefix__"]  = $table_prefix;
	$SqlParams["__LIMIT__"]   = "";

	//Nur die Bereiche  anzeigen, die gesehen werden d�fen, NeuLink, nur wenn man  alle Bereiche  administrieren darf
	if (current_user_can('edit_all_bereiche'))
	{
		$SqlParams["__WHERE__"]   = "1=1";
		$SurParams["__newlink__"] = FsmaAddUrlParam("BID",-1);
	}
	else
	{
		$SqlParams["__WHERE__"]   = "bl.Uid_users = ".$CurrentUser->ID;
		$SurParams["__newlink__"] = "";
		
	}

	$result = $AlleBereiche->DoMultipleQuery(true, $SqlParams, ARRAY_A);
	
	x7RegisterAction("x7BackendBereicheBeforeHeader", $SurParams);
	echo $Header->GetFilteredContent($SurParams, true);
	$alt = true;
	$priorBID = -1;
	
	for ($i=0;$i<count($result[0]);$i++)
	{			
		$Bereich = $result[0][$i];
		$params = array();
		$params["__SENDER__"]	= $Bereich["sender"];
		$params["__X7URL__"]	= X7URL;
		
		if ($priorBID != $Bereich["BID"])
		{
			$params["__Bezeichnung__"]     =  $Bereich["Bezeichnung"];
			$params["__BID__"]             =  $Bereich["BID"];
			$params["__Preview__"]         =  $Bereich["Preview"];
			$params["__morelink__"]        =  FsmaAddUrlParam("BID",$Bereich["BID"]) ;
			$params["__Arbeitszeit__"]     =  $Bereich["Arbeitszeit"];
			$params["__Bereichsleiter__"]  =  $Bereich["Leiter"];
			
			
			$priorBID = $Bereich["BID"];
			$alt = !$alt;
		}
		else
		{			
			$params["__Bereichsleiter__"]  =  $Bereich["Leiter"];
			$params["__BID__"]             =  -1;			
		}

		x7RegisterAction("x7BackendBereicheDetailItem", $params, $Bereich);		
		
		if (!$alt)
		{
			echo $ListItem->GetFilteredContent($params, true);
		}
		else
		{
			echo $ListItemAlt->GetFilteredContent($params, true);
		}
		
		
	}	

	x7RegisterAction("x7BackendBereicheBeforeFooter", $SurParams);
	echo $Footer->GetFilteredContent($SurParams, true);
}
else
{
	//DetailView
	global $wpdb;
	$Bereich    = new jfbremenTemplate(FSMASQL."AdminSingleBereich.sql");
	$BID        = $_GET["BID"];
	
	$result = $Bereich->DoMultipleQuery(true, array("__prefix__" => $table_prefix, "__BID__" => $BID), ARRAY_A);
	//jfPrintDebugArray($result);
	$Bereich = $result[0][0];			
	$isBereichsleiter = $wpdb->get_var(
			"SELECT count(*) FROM ".$table_prefix."bereichsleiter f where BID_Bereiche = $BID AND Uid_users = $CurrentUser->ID"
			);
	if(((current_user_can('edit_own_bereich')) && ($isBereichsleiter != 0)) || (current_user_can('edit_all_bereiche')))
	{	
		$DetailView = new x7Template(X7TPL."BackendBereicheDetailView.tpl");
		$params = array();
		$params["__BID__"]                    =  $BID;
		$params["__ACTION__"]		          =  FsmaAddUrlParam();//FsmaEraseUrlParam
		$params["__BACKURL__"]                =  FsmaEraseUrlParam("BID",FsmaAddUrlParam());
	
		$params["__Bezeichnung__"]            =  $Bereich["Bezeichnung"];
		$params["__BereichsleiterOptions__"]  =  x7GetBereichsleiter4DropDown($BID);
		
		if ($BID == -1)
		{
			$params["__BereichsleiterOptions__"]  =  FsmaGetUsers4DropDown();
			$params["__Bezeichnung__"]            = "&lt;Neuer Bereich&gt;"; 
		}
			
		$params["__Bereichsleiter__"]         =  $Bereich["bereichsleiter"];
		$params["__Description__"]            =  $Bereich["Description"];
		$params["__Preview__"]                =  $Bereich["Preview"];
		$params["__Arbeitszeit__"]            =  $Bereich["Arbeitszeit"];
		$params["__FSK__"]                    =  $Bereich["FSK"];
		$params["__MailNormal__"]             =  $Bereich["MailNormal"];
		$params["__MailAufbau__"]             =  $Bereich["MailAufbau"];
		$params["__LOCKED__"]                 =  $Bereich["locked"];
		$params["__VisInList__"]              =  $Bereich["VisInList"];
		$params["__CustomFields__"]           =  $Bereich["CustomFields"];
		$params["__CustomHint__"]             =  $Bereich["CustomHint"];
		
		$params["__BereichsleiterEnabled__"]  =  (current_user_can('set_bereichsleiter')) ? "" : "disabled";
		
		$params["__BereichsleiterCheckbox__"] = x7GetLeiterByBereichsId4Checkbox($BID, "BLs");

		$mainMail = get_option('FsmaAngenommenMailText');
		$mainMailAufbau = get_option('FsmaAngenommenMailMitAufbauText');
		
		$mainMail = str_replace("__bereichsname__", $Bereich["Bezeichnung"], $mainMail);
		$mainMailAufbau = str_replace("__bereichsname__", $Bereich["Bezeichnung"], $mainMailAufbau);
		
		$params["__EmailNormalText__"] = str_replace("__bereichstext__", $Bereich["MailNormal"], $mainMail);
		$params["__EmailAufbauText__"] = str_replace("__bereichstext__", $Bereich["MailAufbau"], $mainMailAufbau);

		echo $DetailView->GetFilteredContent($params, true);
	}
	else
	{
		$Fehler       = new x7Template(X7TPL."BackendError.tpl");
		$FehlerParams = array();
		$FehlerParams["__TEXT__"] = "Du hast nicht die Berechtigung diesen Bereich zu sehen!";
		echo $Fehler->GetFilteredContent($FehlerParams);
	}
}
?>
