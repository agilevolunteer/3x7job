<?php
$UID          = jfGet("UID");

$Jahr         = $_GET["jahr"];
$CurrentUser  = wp_get_current_user();
//jfPrintDebugArray($CurrentUser);
$BID          = FsmaGetBereichByLeiter($CurrentUser->ID, "BID_Bereiche", true);
$bidList = implode($BID, ",");

if (empty($BID) and !current_user_can("edit_all_ma"))
{
	FsmaError("Dem Nutzer <b>".$CurrentUser->user_login."</b> wurde kein Bereich zugeordnet. 
				Die Verarbeitung wird abgebrochen. <br> Wende dich bei Fragen an den Admin.");
	
	die;	
}

//jfPrintDebugArray($_SESSION["FILTER"]);
global $wpdb;
global $table_prefix;

//Sachen bearbeiten
if ((isset($_POST["ThisPage"])) && ($_POST["ThisPage"] == "MA"))
{
	//Mitarbeiterinformationen
	update_usermeta($UID,'gebdat',          $_POST['gebdat']);
	update_usermeta($UID,'Handy',           $_POST['Handy']);
	update_usermeta($UID,'VorwahlHandy',    $_POST['VorwahlHandy']); 
	update_usermeta($UID,'Tele',            $_POST['Tele']);
	update_usermeta($UID,'VorwahlTele',     $_POST['VorwahlTele']);
	update_usermeta($UID,'ort',             $_POST['ort']);
	update_usermeta($UID,'zip',             $_POST['plz']);
	update_usermeta($UID,'strassehausnr',   $_POST['strassehausnr']);
	update_usermeta($UID,'last_name',       $_POST['nachname']);
	update_usermeta($UID,'first_name',      $_POST['vorname']);
	update_usermeta($UID,'Anmerkungen',      $_POST['Anmerkungen']);
	update_usermeta($UID,'food',      $_POST['food']);
	$displayname = $wpdb->escape($_POST['nachname'].", ".$_POST['vorname']);
	
	$UpdateDisplayName = "UPDATE ".$wpdb->users." SET display_name='$displayname' WHERE ID=$UID";
	$wpdb->query($UpdateDisplayName);

	FsmaMessage("Der Mitarbeiter <i><b>$displayname</b></i> wurde gespeichert.");	
}
elseif ((isset($_POST["ThisPage"])) && ($_POST["ThisPage"] == "Anmeldung"))
{
	//Anmeldung
	if (isset($_POST["abwBereich"]))
	{
		
		FsmaChangePoolStatus($_POST["PrPool"], get_option("FsmaDismissedStatus"));
		FsmaMessage("Der Mitarbeiter wurde in seinem Prim&auml;rbereich abgewiesen.");
	}
	elseif (isset($_POST["abwAltBereich"]))
	{
		FsmaChangePoolStatus($_POST["AltPool"], get_option("FsmaDismissedStatus"));
		FsmaMessage("Der Mitarbeiter wurde in seinem Alternativbereich abgewiesen.");
	}
	elseif (isset($_POST["save"]))
	{
	
		//Anmeldedaten speichern
		//jfPrintDebugArray($_POST);
		//$Anmeldedaten = $_POST;
		$Anmeldedaten["__prefix__"] = $table_prefix;
		$Anmeldedaten["__UserID__"] = $UID;
		//$Anmeldedaten["__STATUS__"] = get_option("FsmaDefStatus");
		$Anmeldedaten["__Jahr__"]   = (empty($_POST["Jahr"])) ? get_option('FsmaJahr') : $_POST["Jahr"];
		$Anmeldedaten["__AufbauAbbau__"]   = (isset($_POST["AufbauAbbau"])) ? "b'001'" : "b'000'";
		$Anmeldedaten["__Abbau__"]   = (isset($_POST["Abbau"])) ? "b'001'" : "b'000'";
		$Anmeldedaten["__Bett__"]   				= (isset($_POST["Bett"])) ? "b'001'" : "b'000'";
		$Anmeldedaten["__AufbauAbbauConfirm__"]   	= (isset($_POST["AufbauAbbauConfirm"])) ? "b'001'" : "b'000'";
		$Anmeldedaten["__Mitgearbeitet__"] = (isset($_POST["Mitgearbeitet"])) ? "b'001'" : "b'000'";
		$Anmeldedaten["__Eingecheckt__"]   = (isset($_POST["Eingecheckt"])) ? "b'001'" : "b'000'";
		$Anmeldedaten["__Bezahlt__"]       = (isset($_POST["Bezahlt"])) ? "b'001'" : "b'000'";
		$Anmeldedaten["__FsErfahrungen__"] = $_POST["FsErfahrungen"];
		$Anmeldedaten["__Bemerkungen__"]   = $_POST["Bemerkungen"];
		$Anmeldedaten["__AnmeldeID__"]     = $_POST["AnmeldeID"];
		$Anmeldedaten["__AufbauBereich__"]     = $_POST["AufbauBereich"];
		//jfPrintDebugArray($Anmeldedaten);
		
		if (!empty($_POST["AnmeldeID"]))
		{
			//echo "update";
			$AnmeldeQuery = new jfbremenTemplate(FSMASQL."BackendUpdateAnmeldung.sql");		
			FsmaChangePool($_POST["PrPool"], $_POST["Bereich"], $_POST["BereichStatus"]);
			FsmaChangePool($_POST["AltPool"], $_POST["AltBereich"], $_POST["AltBereichStatus"]);
		
			if (!empty($_POST["DismissedPool"]))
				FsmaChangePool($_POST["DismissedPool"], $_POST["DismissedBereich"], get_option("FsmaDefStatus"));
		}
		else
		{
			/*$Anmeldedaten["__BereichsID__"]     = $_POST["Bereich"];
			$Anmeldedaten["__AltBereichsID__"]  = $_POST["AltBereich"];
			$Anmeldedaten["__Status__"]     	= $_POST["BereichStatus"];
			$Anmeldedaten["__AltStatus__"]     	= $_POST["AltBereichStatus"];*/
			//echo "BackendInsertAnmeldung";
			$AnmeldeQuery = new jfbremenTemplate(FSMASQL."BackendInsertAnmeldung.sql");
			
		}
		
		//~ //Emails versenden, wenn Sie in einem Bereich angenommen wurden
		//~ FsmaSendNotificationMail($_POST["PrPool"], $_POST["Bereich"], $_POST["BereichStatus"]);
		//~ FsmaSendNotificationMail($_POST["AltPool"], $_POST["AltBereich"], $_POST["AltBereichStatus"]);
		//~ FsmaSendNotificationMail($_POST["DismissedPool"], $_POST["DismissedBereich"], get_option("FsmaDefStatus"));
		
		//echo $AnmeldeQuery->GetFilteredContent($Anmeldedaten, false);
		$AnmeldeQuery->DoMultipleQuery(true, $Anmeldedaten);
	
		FsmaMessage("Anmeldung wurde gespeichert!");
//		FsmaMessage("Der Mitarbeiter muss ggf. &uuml;ber seine Teilnahme am Aufbau informiert werden!");
	}
	
	if ((FsmaIsAllDismissed($UID, $_POST["Jahr"])==true) && (empty($_POST["DismissedPool"])))
	{		
		FsmaSendToDismissedPool($UID, $_POST["Jahr"]);
		FsmaMessage("Mitarbeiter wurde dem Schwimmbereich zugewiesen");
	}
	

}
elseif ((isset($_POST["ThisPage"])) && ($_POST["ThisPage"] == "CustomFields"))
{
	//Customfields
	$keys = array_keys($_POST);
	
	//Platzhalter ersetzen
	foreach ($keys as $key)
	{
		if ( $key != "ThisPage")
			update_usermeta($UID, $key, $_POST[$key]);
	}
	
	FsmaMessage("Zusatzinformationen wurden gespeichert");
}
elseif ((isset($_POST["ThisPage"])) && ($_POST["ThisPage"] == "Filter"))
{
	$FilterBID    = $_POST["bereich"];
//	$FilterAltBID = $_POST["altbereich"];
	$Jahr         = (empty($_POST["jahr"]) || $_POST["jahr"] == -1) ? get_option('FsmaJahr') : $_POST["jahr"];
	
/*	$where = ($FilterBID == -1) ? "((1=1) " : " ((bpr.BID = $FilterBID) ";
	//$where .= ($where == "") ?
	$where .= ($FilterAltBID == -1) ? ") AND " : "OR (balt.BID = $FilterAltBID)) AND ";
	$where .= (empty($_POST["display_name"])) ? "" : ("ma.display_name like '%".$_POST["display_name"]."%' AND ");
*/
	$_SESSION["FILTER"]["BID"]  	= $_POST["bereich"];
	$_SESSION["FILTER"]["JAHR"]  = $Jahr;
	$_SESSION["FILTER"]["NAME"]  = $_POST["display_name"];
	
	$showGroups = (($FilterBID != $FilterAltBID) || ($FilterAltBID + $FilterBID == -2));
}

//$where = $_SESSION["where"];

if ((isset($_GET["chg"])) && ($_GET["chg"] == "check"))
{
	$value = $_GET["val"];
	$aid   = $_GET["aid"];
	
	$query = "UPDATE ".$table_prefix."anmeldung set Eingecheckt = b'".$wpdb->escape($value)."' WHERE id = ".$wpdb->escape($aid);
	$wpdb->query($query);
}
elseif ((isset($_GET["chg"])) && ($_GET["chg"] == "zahl"))
{
	$value = $_GET["val"];
	$aid   = $_GET["aid"];
	
	$query = "UPDATE ".$table_prefix."anmeldung set Bezahlt = b'".$wpdb->escape($value)."' WHERE id = ".$wpdb->escape($aid);
	$wpdb->query($query);
}
elseif ((isset($_GET["chg"])) && ($_GET["chg"] == "conf"))
{
	$value = $_GET["val"];
	$aid   = $_GET["aid"];
	
	$query = "UPDATE ".$table_prefix."anmeldung set AufAbbauConfirm = b'".$wpdb->escape($value)."' WHERE id = ".$wpdb->escape($aid);
	$wpdb->query($query);
	
	FsmaMessage("Der Mitarbeiter muss auf gesondertem Wege &uuml;ber die Teilnahme am Aufbau informiert werden, sofern er bereits angenommen und per Mail informiert wurde!");
}

//Suche
$SuchTemplate = new x7Template(X7TPL."BackendMaFilter.tpl");
$SuchParams   = array();
$SuchParams["__BereichsOptions__"]    = FsmaGetBereiche4DropDown($_SESSION["FILTER"]["BID"]);
//$SuchParams["__AltBereichsOptions__"] = FsmaGetBereiche4DropDown($FilterAltBID);
$SuchParams["__JahrOptions__"]        = FsmaGetJahr4DropDown($_SESSION["FILTER"]["JAHR"]);
$SuchParams["__ACTION__"]             = FsmaAddUrlParam();
$SuchParams["__DISPLAYNAME__"]        = $_SESSION["FILTER"]["NAME"];
$SuchParams["__inDetailView__"]       = isset($_GET["UID"]);
$SuchParams["__backurl__"]       	  = FsmaDeleteUrlParams("UID");

 

echo $SuchTemplate->GetFilteredContent($SuchParams);

//spezifische Anzeige
if (!isset($_GET["UID"]))
{
	//ListView	
	?>
	
	<div class="wrap">	
	<div style="float: right;"><a href="javascript:window.print()">Drucken</a></div>
	<h2>Mitarbeiter</h2>	
	<?php
		//ListView
		include_once 'ma-listview.php';
	?></div><?php
}
else
{
	include_once 'ma-detailview.php';
}

?>

