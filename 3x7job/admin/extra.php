<?php
	include(FSMAPATH."extras-functions.php");
	global $wpdb;
	global $table_prefix;
	
	// Neuen Nutzer ohne Wizard anlegen
	if (isset($_POST["uid"]))
	{
		//Neuen Mitarbeiter anmelden
		if (current_user_can("edit_all_ma"))
		{
			//In den Schwimmerpool, wenn vom Mitarbeiteradmin angelegt
			$BID = get_option('FsmaDismissedPool');
		}
		else
		{
			//Direkt in den Bereich, wenn von Bereichsleiter angelegt wird
			$CurrentUser  = wp_get_current_user();
			$BID          = FsmaGetBereichByLeiter($CurrentUser->ID, "BID");
		}
		
		FsmaInsertLightAnmeldung($BID, $_POST["uid"]);
		FsmaMessage("Nutzer wurde als Mitarbeiter angemeldet.");
	}

	//Ausgabe
	$newMA			       		= new x7Template(X7TPL."BackendExtraNewMa.tpl");
	$Params 					= array();
	$Params['__ACTIONURL__'] 	= FsmaAddUrlParam();
	$Params['__USEROPTIONS__']	= FsmaGetUsersOhneAnmeldung();
	$Params['__USERPATH__']		= get_option('siteurl')."/wp-admin/users.php";
	
	echo $newMA->GetFilteredContent($Params, true);
	
	
	if (current_user_can('rename_user'))
	{
		// Nutzer umbenennen
		echo "<br>";
		$chosenUserId 						= -1;
		$renameUserTemplate 				= new x7Template(X7TPL."BackendExtraRenameMa.tpl");
		$Params['__USERPATH__']				= get_option('siteurl')."/wp-admin/user-edit.php";
		$Params['__USEROPTIONS__']			= "";
		$Params['__SEARCHUSERNAME__']		= "";
		$Params['__CHOSENUSER__']			= "";
		$Params['__CHOSENUSERNAME__']		= "";
			
		if (!empty($_POST['searchName']))
		{
			$chosenUserId 					= (empty($_POST['uidChoose'])) ? -1 : $_POST['uidChoose'];
			$Params['__USEROPTIONS__']		= FsmaGetUsers($_POST['searchName'], $chosenUserId);
			$Params['__SEARCHUSERNAME__']	= $_POST['searchName'];		
		}		
		
		if (!empty($_POST['btnChooseUser']))
		{
			$Params['__CHOSENUSER__']		= $chosenUserId;
			$Params['__CHOSENUSERNAME__']	= FsmaGetUserName4ID($chosenUserId);
		}		
		
		if (isset($_POST["btnRenameUser"]))
		{
			$oldName = FsmaGetUserName4ID($chosenUserId);
			FsmaRenameUser($chosenUserId, $_POST['chosenUserName']);
			FsmaMessage("Nutzer $chosenUserId ($oldName) wurde in ".$_POST['chosenUserName'].") umbenannt.");					
		}	
		
		echo $renameUserTemplate->GetFilteredContent($Params, true);
	}
?>