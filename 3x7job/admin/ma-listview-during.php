
<?php
	//SQL
	$DismissedStatus          = get_option("FsmaDismissedStatus");
	$NewStatus                = get_option("FsmaDefStatus");
	$AcceptStatus             = get_option("FsmaAngenommenStatus");	

	$SqlParams = array();
	$SqlParams["__prefix__"]  = $table_prefix;
	$SqlParams["__LIMIT__"]   = "$startLimit, $endLimit";
	$SqlParams["__ORDER__"]   = (isset($_GET["sort"])) ? str_replace("-", " ",$_GET["sort"]) :"Bereich";
	$SqlParams["__JAHR__"]    = (empty($_SESSION["FILTER"]["JAHR"])) ? get_option('FsmaJahr') : $_SESSION["FILTER"]["JAHR"];
	
	if (current_user_can('read_all_ma'))
		$SqlParams["__WHERE__"] = ((empty($_SESSION["FILTER"]["JAHR"])) || ($_SESSION["FILTER"]["BID"] == -1)) ? "(1=1)" : " (b.BID = ".$_SESSION["FILTER"]["BID"].")";
	else
		$SqlParams["__WHERE__"] = " (b.BID IN ($bidList)) ";

	
	//Nach Mitarbeiternamen filtern
	$SqlParams["__WHERE__"] .= (empty($_SESSION["FILTER"]["NAME"])) ? "" : (" AND u.display_name like '%".$_SESSION["FILTER"]["NAME"]."%'");
	$anmeldungenSQL = new jfbremenTemplate(FSMASQL."AlleAnmeldungenDuring.sql");
	
	$result = $anmeldungenSQL->DoMultipleQuery(true, $SqlParams, ARRAY_A);
	
	//Den ganzen Kram ausgeben
	//$SurParams kommen aus der ma-listview.php
	if (count($result[0])>0)
	{
		$noResult = false;
		$Header       = new x7Template(X7TPL."BackendMADuringHeader.tpl");
		$Footer       = new x7Template(X7TPL."BackendMAFooter.tpl");
		$ListItem     = new x7Template(X7TPL."BackendMADuringListItem.tpl");
		$ListItemAlt  = new x7Template(X7TPL."BackendMADuringListAltItem.tpl");
		
					
		//Paging
		$SurParams["__NEXTPAGE__"]       = $paging + 1;
		$SurParams["__PREVIOUSPAGE__"]   = $paging - 1;
		
		echo $Header->GetFilteredContent($SurParams, true);
		$showAlt = false;
		//$showGroups = true;
		$oldOrder = "";
		$oldMaId = -1;
		
		for ($i=0;$i<count($result[0]);$i++)
		{
			$SingleMA = $result[0][$i];
			if ($SingleMA["ID"] != $oldMaId) {

				$oldMaId = $SingleMA["ID"];
				$params                 = array();
				$params["__FSMAURL__"]  = FSMAURL;
				$params["__morelink__"] = FsmaAddUrlParam( "UID", $SingleMA["ID"] );
				$params["__Name__"]     = $SingleMA["Name"];
				$params["__BID__"]      = $SingleMA["BID"];
				$params["__Bereich__"]  = $SingleMA["Bereich"];

				$params["__AufAbbau__"] = $SingleMA["AufAbbau"];
				$params["__Abbau__"]    = $SingleMA["Abbau"];
				$params["__Bett__"]     = $SingleMA["Bett"];
				$params["__Bezahlt__"]  = $SingleMA["Bezahlt"];
				$params["__AID__"]      = $SingleMA["aid"];

				//F�r die StatusIcons
				$params["__Dismissed__"] = ( $SingleMA["Status"] == $DismissedStatus );
				$params["__New__"]       = ( $SingleMA["Status"] == $NewStatus );
				$params["__Accept__"]    = ( $SingleMA["Status"] == $AcceptStatus );

				//Link zum Bearbeiten des Bezahlstatus
				$params["__EditBezahltUrl__"] = FsmaAddUrlParam( "chg", "zahl" );
				$params["__EditBezahltUrl__"] = FsmaAddUrlParam( "aid", $SingleMA["aid"], $params["__EditBezahltUrl__"] );
				$params["__EditBezahltUrl__"] = ( $SingleMA["Bezahlt"] ) ? FsmaAddUrlParam( "val", 0, $params["__EditBezahltUrl__"] ) : FsmaAddUrlParam( "val", 1, $params["__EditBezahltUrl__"] );

				//Bemerkungen und Anriss für die Liste
				$params["__Bemerkungen__"] = get_user_meta( $SingleMA["ID"], "Anmerkungen", true );
				$params["__ANRISS__"]      = substr( $params["__Bemerkungen__"], 0, 50 ) . "...";


				$ThisOrderExp = FsmaSwitchOrderExp( $orderExp, $SingleMA[ $orderExp ] );

				if ( $showGroups ) {
					if ( $ThisOrderExp == $oldOrder ) {
						$params["__ORDEREXP__"] = "";
					} else {
						$params["__ORDEREXP__"] = $ThisOrderExp;
						$showAlt                = false;

						if ( $orderExp == "Bereich" ) {
							$params["__ITEMCOUNT__"] = GetAnzahlMa4Bereich( $params["__BID__"], $SqlParams["__JAHR__"] );
						} else {
							$params["__ITEMCOUNT__"] = "";
						}
					}
				} else {
					$params["__ORDEREXP__"] = "";
				}

				$oldOrder = $ThisOrderExp;

				if ( $showAlt == true ) {
					echo $ListItemAlt->GetFilteredContent( $params, true );
				} else {
					echo $ListItem->GetFilteredContent( $params, true );
				}

				$showAlt = ! $showAlt;
			}
		}
		
		echo $Footer->GetFilteredContent();
	}
?>
	
