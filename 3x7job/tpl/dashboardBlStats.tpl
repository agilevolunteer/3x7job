	<div width="100%" align="center" id="dashboard" >
	<table width="95%" cellpadding="0" cellspacing="0" border=0>
		<!-- Bereichsspezifisch -->
		
		<tr [sif:__BEREICH__:neq:]><td class="space" colspan="2"><b>Bereich __BEREICH__</b></td></tr>
		
		<tr [sif:__BEREICH__:neq:]>
			<td>
				Anzahl angemeldete Mitarbeiter:
			</td>
			<td class="count">
				__MITARBEITERCOUNTBEREICH__
			</td>
		</tr>
		<tr [sif:__BEREICH__:neq:]>
			<td>
				davon Teilnahme am Aufbau:
			</td>
			<td class="count">
				__AUFBAUCOUNTBEREICH__
			</td>
		</tr>
		<tr [sif:__BEREICH__:neq:]>
			<td>
				Anzahl eingecheckter Mitarbeiter:
			</td>
			<td class="count">
				__CHECKINCOUNTBEREICH__
			</td>
		</tr>	
		
	</table>	
	</div>
