
<tr [sif:__ORDEREXP__:neq:] class="FsmaGroupSpacer">
	<td colspan="6">
		&nbsp;
	</td>
</tr>
<tr [sif:__ORDEREXP__:neq:] class="FsmaGroupHeaderRow">
	<td colspan="6">
		__ORDEREXP__
	</td>
</tr>
<tr class="FsmaMaAnchor">
	<td>
		<a name="__AID__"></a>
	</td>
</tr>

<tr class="alternate __Changed__">
	<td>
		__Name__	
	</td>
	<td>		
		<img src="__FSMAURL__style/icons/cancel_round.png" [sif:__Dismissed__:neq:] title="abgewiesen" \>
		<img src="__FSMAURL__style/icons/new.png" [sif:__New__:neq:] title="angemeldet" \>
		<img src="__FSMAURL__style/icons/accept_green.png" [sif:__Accept__:neq:] title="angenommen" \>
		&nbsp;__Bereich__
	</td>
	<td>		
		<img src="__FSMAURL__style/icons/cancel_round.png" [sif:__AltDismissed__:neq:] title="abgewiesen" \>
		<img src="__FSMAURL__style/icons/new.png" [sif:__AltNew__:neq:] title="angemeldet" \>
		<img src="__FSMAURL__style/icons/accept_green.png" [sif:__AltAccept__:neq:] title="angenommen" \>
		&nbsp;__AltBereich__
	</td>
	<td>
		<a href="__EditEingechecktUrl__#__AID__" [sif:__Eingecheckt__:eq:0] class="Icon" onfocus="if(this.blur)this.blur();"><img src="__FSMAURL__style/icons/cancel.png"\></a>
		<a href="__EditEingechecktUrl__#__AID__" [sif:__Eingecheckt__:eq:1] class="Icon" onfocus="if(this.blur)this.blur();"><img src="__FSMAURL__style/icons/accept.png"\></a>
	</td>
	<td>		
		<a href="__EditBezahltUrl__#__AID__" [sif:__Bezahlt__:eq:0] class="Icon" onfocus="if(this.blur)this.blur();"><img src="__FSMAURL__style/icons/cancel.png"\></a>
		<a href="__EditBezahltUrl__#__AID__" [sif:__Bezahlt__:eq:1] class="Icon" onfocus="if(this.blur)this.blur();"><img src="__FSMAURL__style/icons/accept.png"\></a>
	</td>
	<td>
		<a href="__morelink__" id="morelink">Mehr..</a>
	</td>
</tr>
