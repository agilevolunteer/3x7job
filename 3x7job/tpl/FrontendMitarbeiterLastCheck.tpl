<h2>Angaben &uuml;berpr&uuml;fen</h2>
<a name="lastcheck"></a>
<form action="__ACTION__" method="POST">
<input type="hidden" name="ThisPage" value="5">
<input type="hidden" name="NextPage" value="6">
<span [sif:__ERROR__:neq:] class="agv-error">__ERROR__</span>
	<div class="agv-form__row">
		<input type="submit" value="Verbindlich anmelden" class="agv-button agv-button--full-attention">
	</div>
	<div class="agv-form__row">
		<h4>Pers&ouml;nliche Daten</h4>
	</div>
	<div class="agv-form__row agv-form__row--note">
		Name:
	</div>
	<div class="agv-form__row">
		__vorname__ __nachname__
	</div>
	<div class="agv-form__row agv-form__row--note">
		Postleitzahl:
	</div>
	<div class="agv-form__row">
		__plz__
	</div>
	<div class="agv-form__row agv-form__row--note">
		Telefon:
	</div>
	<div class="agv-form__row">
		__VorwahlTele__ / __Tele__
	</div>
	<div class="agv-form__row agv-form__row--note" [sif:__VorwahlHandy__:neq:]>
		Handy:
	</div>
	<div class="agv-form__row" [sif:__VorwahlHandy__:neq:]>
		__VorwahlHandy__ / __Handy__
	</div>
	<div class="agv-form__row agv-form__row--note">
		Geburtsdatum:
	</div>
	<div class="agv-form__row">
		__gebdat__
	</div>
	<div class="agv-form__row">
		<h4>Anmeldung</h4>
	</div>
	<div class="agv-form__row agv-form__row--note">
		Bereich:
	</div>
	<div class="agv-form__row">
		__BereichsID__
	</div>
	<div class="agv-form__row agv-form__row--note"  [sif:__AltBereichsID__:neq:]>
		Alternativbereich:
	</div>
	<div class="agv-form__row"  [sif:__AltBereichsID__:neq:]>
		__AltBereichsID__
	</div>
	<div class="agv-form__row agv-form__row--note">
		Teilnahme am Aufbau:
	</div>
	<div class="agv-form__row">
		__Aufbau__
	</div>
	<div class="agv-form__row agv-form__row--note">
		Teilnahme am Abbau:
	</div>
	<div class="agv-form__row">
		__Abbau__
	</div>
	<div class="agv-form__row agv-form__row--note" [sif:__AufbauBereichID__:neq:]>
		Team Auf-/Abbau:
	</div>
	<div class="agv-form__row" [sif:__AufbauBereichID__:neq:]>
		__AufbauBereichID__
	</div>
	<div class="agv-form__row agv-form__row--note">
		Erfahrungen:
	</div>
	<div class="agv-form__row">
		__FsErfahrungen__
	</div>
	<div class="agv-form__row agv-form__row--note">
		Bemerkungen:
	</div>
	<div class="agv-form__row">
		__Bemerkungen__
	</div>
	<div class="agv-form__row"  [sif:__CustomFields__:neq:]>
		<h4>Sonderangaben</h4>
	</div>
	__CustomFields__
	<div class="agv-form__row">
		<input type="submit" value="Verbindlich anmelden" class="agv-button agv-button--full-attention">
	</div>
</form>


