<a name="registration"></a>
<h3>Neues Konto anlegen</h3>
<form action="__ACTION__#registration" method="POST" class="agv-form">
  <input type="hidden" name="ThisPage" value="1">
  <input type="hidden" name="NextPage" value="3">
  <span [sif:__ERROR__:neq:] class="agv-error">__ERROR__</span>

  <div class="agv-form__row">
    <label for="email">Email:</label>
    <input type="email" name="email" value="__email__" class="agv-input"  placeholder="Email" required>
  </div>
  <div class="agv-form__row">
    <label for="password">Passwort:</label>
    <input type="password" name="password" value="__password__" class="agv-input" placeholder="Passwort" required>
  </div>
  <div class="agv-form__row agv-form__row--cta">
    <input type="submit" value="Weiter mit neuem Konto" class="agv-button agv-button--primary">
  </div>
</form>
<a name="login"></a>
<h3>Mit eigenem Konto anmelden</h3>
<form action="__ACTION__#login" method="POST">
  <input type="hidden" name="ThisPage" value="99">
  <input type="hidden" name="NextPage" value="3">
  <span [sif:__LOGINERROR__:neq:] class="agv-error">__LOGINERROR__</span>

  <div class="agv-form__row">
    <label for="email">Email:</label>
    <input type="email" name="email" value="__email__" class="agv-input" placeholder="Email" required>
  </div>
  <div class="agv-form__row">
    <label for="password">Passwort:</label>
    <input type="password" name="password" value="__password__" class="agv-input" placeholder="Passwort" required>
  </div>
  <div class="agv-form__row agv-form__row--cta">
    <input type="submit" value="Weiter mit eigenem Konto" class="agv-button">
  </div>
</form>