<span [sif:__newlink__:neq:]><h3><a href="__newlink__">Neuen Mitarbeiter anlegen...</a></h3></span>
<div style="float: right;"><a href="__CleanSortUrl__&print=1" target="_blank">Drucken</a></div><br><br>
<table class="widefat" cellpadding=0 cellspacing=0>
	<thead>
	<tr>
		<th width="300px">
			Name &nbsp;
			<a href="__CleanSortUrl__&sort=Name-asc" [sifui:sort:neq:Name-asc] class="arrow"><img src="__FSMAURL__/style/icons/green_down.gif"></a>
			<img src="__FSMAURL__/style/icons/red_down.gif" [sifui:sort:eq:Name-asc]>

			<a href="__CleanSortUrl__&sort=Name-desc" [sifui:sort:neq:Name-desc] class="arrow"><img src="__FSMAURL__/style/icons/green_up.gif"></a>
			<img src="__FSMAURL__/style/icons/red_up.gif" [sifui:sort:eq:Name-desc] >
		</th>
		<th>
			Bereich
			<a href="__CleanSortUrl__&sort=Bereich-asc" [sifui:sort:neq:Bereich-asc] class="arrow"><img src="__FSMAURL__/style/icons/green_down.gif"></a>
			<img src="__FSMAURL__/style/icons/red_down.gif" [sifui:sort:eq:Bereich-asc]>
			
			<a href="__CleanSortUrl__&sort=Bereich-desc" [sifui:sort:neq:Bereich-desc] class="arrow"><img src="__FSMAURL__/style/icons/green_up.gif"></a>
			<img src="__FSMAURL__/style/icons/red_up.gif"[sifui:sort:eq:Bereich-desc]>
		</th>
		<th>
			Alternativbereich
			<a href="__CleanSortUrl__&sort=AltBereich-asc" [sifui:sort:neq:AltBereich-asc] class="arrow"><img src="__FSMAURL__/style/icons/green_down.gif"></a>
			<img src="__FSMAURL__/style/icons/red_down.gif" [sifui:sort:eq:AltBereich-asc]>
			
			<a href="__CleanSortUrl__&sort=AltBereich-desc" [sifui:sort:neq:AltBereich-desc] class="arrow"><img src="__FSMAURL__/style/icons/green_up.gif"></a>
			<img src="__FSMAURL__/style/icons/red_up.gif" [sifui:sort:eq:AltBereich-desc] >			
		</th>
		<th align="center">
			Eingecheckt
			<a href="__CleanSortUrl__&sort=Eingecheckt-asc" [sifui:sort:neq:Eingecheckt-asc] class="arrow"><img src="__FSMAURL__/style/icons/green_down.gif"></a>
			<img src="__FSMAURL__/style/icons/red_down.gif" [sifui:sort:eq:Eingecheckt-asc] >
			
			<a href="__CleanSortUrl__&sort=Eingecheckt-desc" [sifui:sort:neq:Eingecheckt-desc] class="arrow"><img src="__FSMAURL__/style/icons/green_up.gif"></a>
			<img src="__FSMAURL__/style/icons/red_up.gif" [sifui:sort:eq:Eingecheckt-desc] >		
		</th>
		<th align="center">
			Bezahlt
			<a href="__CleanSortUrl__&sort=Bezahlt-asc" [sifui:sort:neq:Bezahlt-asc] class="arrow"><img src="__FSMAURL__/style/icons/green_down.gif"></a>
			<img src="__FSMAURL__/style/icons/red_down.gif" [sifui:sort:eq:Bezahlt-asc] >

			<a href="__CleanSortUrl__&sort=Bezahlt-desc" [sifui:sort:neq:Bezahlt-desc] class="arrow"><img src="__FSMAURL__/style/icons/green_up.gif"></a>
			<img src="__FSMAURL__/style/icons/red_up.gif" [sifui:sort:eq:Bezahlt-desc] >
		</th>
		<th>
			&nbsp;
		</th>
	</tr>
	</thead>
	<tbody>