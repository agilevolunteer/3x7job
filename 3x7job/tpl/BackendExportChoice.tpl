<div class="wrap submit">
	<h2>Export</h2>
	
	<a href="__FILEPATH__" [sif:__OUTPUT__:neq:]>Link zur Datei</a>
	
	<form action="__ACTION__" method="POST">
	
	<table>
		<tr>
			<td>
				Zielformat:
			</td>
			<td>
				<select name="export">
					<option value="csv">CSV</option>
					<option value="txt">TXT</option>
				</select>	
			</td>
		</tr>
		<tr>
			<td>
				Auswertungsanweisungen:
			</td>
			<td>
				<select name="queries">
					__FILEOPTIONS__
				</select>			
			</td>
		</tr>
		<tr class="submit">
			<td>
				
			</td>
			<td>
				<input type="submit" value="Exportieren...">
			</td>
		</tr>
	</table>
		
	</form>
	<br><br>
	<textarea cols="60" rows="20">
__OUTPUT__
	</textarea>
	
	
</div>