<h2>Pers&ouml;nliche Daten</h2>
<form action="__ACTION__" method="POST">
<input type="hidden" name="ThisPage" value="3">
<input type="hidden" name="NextPage" value="2">
<span class="agv-error">__ERROR__</span>
	<div class="agv-form__row">
		<label for="vorname">Vorname:</label>
		<input type="text" class="agv-input" name="vorname" placeholder="Vorname" value="__vorname__" required>
	</div>
	<div class="agv-form__row">
		<label for="nachname">Nachname:</label>
		<input type="text" class="agv-input" name="nachname" value="__nachname__" placeholder="Nachname" required>
	</div>
	<div class="agv-form__row">
		<label for="plz">Postleitzahl:</label>
		<input type="tel" class="agv-input" name="plz" value="__plz__" placeholder="Postleitzahl" required>


	</div>
	<div class="agv-form__row agv-form__row--phone">
		<label for="VorwahlTele">Telefon:</label>
	</div>
	<div class="agv-form__row agv-form__row--phone">
		<input type="tel" class="agv-input agv-input--citycode" name="VorwahlTele" value="__VorwahlTele__" placeholder="Vorwahl" required>
		<input type="tel" class="agv-input agv-input--phone" name="Tele" value="__Tele__" placeholder="Telefonnummer" required>
	</div>
	<div class="agv-form__row agv-form__row--phone">
		<label for="VorwahlTele">Handy (optional):</label>
	</div>
	<div class="agv-form__row agv-form__row--phone">
		<input type="tel" class="agv-input agv-input--citycode" name="VorwahlHandy" value="__VorwahlHandy__" placeholder="Vorwahl">
		<input type="tel" class="agv-input agv-input--phone" name="Handy" value="__Handy__" placeholder="Handynummer">
	</div>
	<div class="agv-form__row">
		<label for="gebdat">Geburtsdatum: (YYYY-MM-DD)</label>
		<input type="text" class="agv-input" name="gebdat" value="__gebdat__" placeholder="Geburtsdatum" required>
	</div>
	<div class="agv-form__row">
		<label for="food">Ernährungsweise:</label>
		<select name="food" class="agv-input">
			__food__
		</select>
	</div>
	<div class="agv-form__row agv-form__row--cta">
		<input type="submit" value="Weiter zur Bereichsauswahl..." class="agv-button agv-button--primary">
	</div>
</form>
