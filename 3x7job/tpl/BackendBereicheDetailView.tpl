<div class="wrap submit">
<h2>__Bezeichnung__</h2>

<input type="hidden" name="Arbeitszeit" value="0" class="TextBoxShort">
<input type="hidden" name="BID" value="__BID__">

<!-- Leitung -->
<form action="__ACTION__" method="POST">
<table name="BackendFormTable" cellpadding="0" cellspacing="0" width="100%">
		<tr>		
			<td class="label" valign="top">
				Bereichsleiter: 
			</td>
			<td>
				<select name="bereichsleiter" lines="10" class="TextBox" __BereichsleiterEnabled__>
					__BereichsleiterOptions__
				</select>
				<!--INPUT type="hidden" name="oldBereichsleiter" value="__Bereichsleiter__"-->
				
				<span style="float: right;">
					<input type="submit" value="Ernennen" align="right">
				</span>
			</td>
		</tr>
		<tr>
			<td>
			</td>
			<td>
				<br>
				<input type="checkbox" name="BL[]" value="BL1"> BL1<br>
			    <input type="checkbox" name="BL[]" value="BL2"> BL2<br>
    			<input type="checkbox" name="BL[]" value="BL3"> BL3<br>
				
				<div style="float: right;">
					<input type="submit" value="Entlassen" align="right">
				</div>
			</td>
		</tr>
		<tr>
			<td colspan="2">
			<hr>
			</td>
		</tr>
<!-- /table-->
</form>
<!-- Ende Leitung -->
<form action="__ACTION__" method="POST">
	<!-- table name="BackendFormTable" cellpadding="0" cellspacing="0" width="100%"-->
	
		<tr class="submit">
			<td>
			
			</td>
			<td class="ButtonsRight">
				<input type="submit" value="Speichern"><br><br>
			</td>
		</tr>
	
		<tr>
			<td class="label">
				Bezeichnung: 
			</td>
			<td>
				<INPUT TYPE="text" name="bezeichnung" value="__Bezeichnung__" class="TextBox">
			</td>
		</tr>
		
		
		
		<tr>
			<td class="label">
				Kurzbeschreibung:
			</td>
			<td>
				<textarea name="kurztext" class="TextAreaShort">__Preview__</textarea>
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Beschreibung:
			</td>
			<td>
				<textarea name="langtext" class="TextArea">__Description__</textarea>
			</td>
		</tr>
		
		<tr>
			<td class="label">
				
			</td>
			<td>
				<input type="checkbox" name="VisInList" value="false" __VisInList__> Wird in Bereichsliste geposted
			</td>
		</tr>
		
		<tr>
			<td class="label">
				
			</td>
			<td>
				<input type="checkbox" name="locked" value="false" __LOCKED__> In Anmeldung nicht sichtbar
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Mindesalter:
			</td>
			<td>
				<input type="text" name="FSK" value="__FSK__" class="TextBoxShort"> Jahre
			</td>
		</tr>
		
		<!--tr >
			<td class="label">
				Arbeitszeit:
			</td>
			<td>
				<input type="text" name="Arbeitszeit" value="__Arbeitszeit__" class="TextBoxShort"> Arbeitsstunden
			</td>
		</tr-->
		
		<tr>
			<td class="label">
				Text f&uuml;r Angenommenmail:<br>
				(ohne Aufbau)
			</td>
			<td>
				<textarea name="MailNormal" class="TextArea">__MailNormal__</textarea>
			</td>
		</tr>
		
				<tr>
			<td class="label">
				Text f&uuml;r Angenommenmail:<br>
				(mit Aufbau)
			</td>
			<td>
				<textarea name="MailAufbau" class="TextArea">__MailAufbau__</textarea>
			</td>
		</tr>
		
		<tr [sif:__BereichsleiterEnabled__:eq:]>
			<td class="label">
				Benutzerdefinierte Felder<br> f&uuml;r Mitarbeiter:
			</td>
			<td>
				<textarea name="CustomFields" class="TextAreaShort">__CustomFields__</textarea>
			</td>
		</tr>
		
		<tr [sif:__BereichsleiterEnabled__:eq:]>
			<td class="label">
				Bereichsspezifischer Hinweis:
			</td>
			<td>
				<textarea name="CustomHint" class="TextAreaShort">__CustomHint__</textarea>
			</td>
		</tr>
		
		<tr class="submit">
			<td>
			
			</td>
			<td class="ButtonsRight">
				<br><br><input type="submit" value="Speichern">
			</td>
		</tr>
		
		<tr>
			<td>
				<a href="__BACKURL__">Zur&uuml;ck zur Liste...</a>
			</td>
			<td>
			
			</td>
		</tr>
	</table>
</form>
</div>