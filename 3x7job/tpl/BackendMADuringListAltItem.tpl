<tr [sif:__ORDEREXP__:neq:] class="FsmaGroupSpacer">
	<td colspan="7">
		&nbsp;
	</td>
</tr>
<tr [sif:__ORDEREXP__:neq:] class="FsmaGroupHeaderRow">
	<td colspan="7">
		__ORDEREXP__ <span [sif:__ITEMCOUNT__:neq:]>(__ITEMCOUNT__)</span>
	</td>
</tr>
<tr class="alternate">
	<!--td class="submit" nowrap>
		<input type="button" value="Annehmen">
		<input type="button" value="Ablehnen">
	</td-->
	<td nowrap>
		__Name__
	</td>
	<td nowrap>
		<img src="__FSMAURL__style/icons/cancel_round.png" [sif:__Dismissed__:neq:] title="abgewiesen" \>
		<img src="__FSMAURL__style/icons/new.png" [sif:__New__:neq:] title="angemeldet" \>
		<img src="__FSMAURL__style/icons/accept_green.png" [sif:__Accept__:neq:] title="angenommen" \>
		__Bereich__
		<span [sif:__BID__:eq:0]>KEIN BEREICH ANGEGEBEN</span>
	</td>
	<!--td>
		<div id="box" [sif:__Bemerkungen__:neq:]>
			<a href="#"><img src="__FSMAURL__style/icons/comment_blue.png" \>&nbsp;__ANRISS__<span>__Bemerkungen__</span></a>
		</div>
	</td-->
	<td>
		<img src="__FSMAURL__style/icons/cancel.png" [sif:__AufAbbau__:eq:0] \>
		<img src="__FSMAURL__style/icons/accept.png" [sif:__AufAbbau__:eq:1] \>
	</td>
	<td>
		<img src="__FSMAURL__style/icons/cancel.png" [sif:__Abbau__:eq:0] \>
		<img src="__FSMAURL__style/icons/accept.png" [sif:__Abbau__:eq:1] \>
	</td>
	<td>
		<a href="__EditBezahltUrl__#__AID__" [sif:__Bezahlt__:eq:0] class="Icon" onfocus="if(this.blur)this.blur();"><img src="__FSMAURL__style/icons/cancel.png"\></a>
		<a href="__EditBezahltUrl__#__AID__" [sif:__Bezahlt__:eq:1] class="Icon" onfocus="if(this.blur)this.blur();"><img src="__FSMAURL__style/icons/accept.png"\></a>
	</td>
	<td>
		<div class="agv-bemerkung__preview" [sif:__ANRISS__:neq:...]>
			__ANRISS__
			<div class="agv-bemerkung__content">
				__Bemerkungen__
			</div>
		</div>
	</td>
	<td>
		<a href="__morelink__" id="morelink">Mehr..</a>
	</td>
</tr>
