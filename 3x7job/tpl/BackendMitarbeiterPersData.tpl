<div class="wrap">
<h2>__vorname__ __nachname__ (__email__)</h2>
<form action="__ACTION__" method="POST">
<input type="hidden" name="ThisPage" value="MA">
<span class="FsmaError">__ERROR__</span>
	<table class="BackendFormTable">
		<tr>
			<td>
			
			</td>
			<td class="InputCell submit">
				<input type="submit" class="FsmaButton" value="Speichern">
			</td>
		</tr>
	
		<tr>
			<td>
			Vorname*:
			</td>
			<td class="InputCell">
				<input type="text" class="TextBox" name="vorname" value="__vorname__">
			</td>
		</tr>
		
		<tr>
			<td>
			Nachname*:
			</td>
			<td class="InputCell">
				<input type="text" class="TextBox" name="nachname" value="__nachname__">
			</td>
		</tr>
		
		<tr>
			<td>
			Stra&szlig;e / Hausnr.*:
			</td>
			<td class="InputCell">
				<input type="text" class="TextBox" name="strassehausnr" value="__strassehausnr__">
			</td>
		</tr>		
		
		<tr>
			<td>
			PLZ*:
			</td>
			<td class="InputCell">
				<input type="text" class="TextBox" name="plz" value="__plz__">
			</td>
		</tr>
		
		<tr>
			<td>
			Ort*:
			</td>
			<td class="InputCell">
				<input type="text" class="TextBox" name="ort" value="__ort__" readonly>
			</td>
		</tr>
		
		<tr>
			<td>
			Telefon:
			</td>
			<td class="InputCell">
				<input type="text" class="TextBoxVorwahl" name="VorwahlTele" value="__VorwahlTele__"> / 
				<input type="text" class="TextBoxTele" name="Tele" value="__Tele__">
			</td>
		</tr>
		
		<tr>
			<td>
			Handy:
			</td>
			<td class="InputCell">
				<input type="text" class="TextBoxVorwahl" name="VorwahlHandy" value="__VorwahlHandy__"> / 
				<input type="text" class="TextBoxTele" name="Handy" value="__Handy__">
			</td>
		</tr>
		
		<tr>
			<td>
			Geburtsdatum*:
			(YYYY-MM-DD)
			</td>
			<td class="InputCell">
				<input type="text" class="TextBox" name="gebdat" value="__gebdat__">
			</td>
		</tr>
		
    	<tr>
			<td>
            Ernährungstyp:			
			</td>
			<td align="right">
				<select name="food" class="DropDown">
				    __food__
				</select>
			</td>
		</tr>
		
		<tr>
			<td>
			Email:
			</td>
			<td class="InputCell">
				<a href="mailto:__email__">__email__</a>
			</td>
		</tr>
		
		<tr>
			<td valign="top">
			Anmerkungen f&uuml;r die MA-Admin:<br>			
			</td>
			<td class="InputCell">
				<textarea name="Anmerkungen" class="TextAreaShort">__Anmerkungen__</textarea>
			</td>
		</tr>
		
		
		<tr>
			<td>
			
			</td>
			<td class="InputCell submit">
				<input type="submit" class="FsmaButton" value="Speichern">
			</td>
		</tr>	
	</table>
</form>
</div>
