<h2>Anmeldedetails</h2>
<!--__Mitarbeiter__-->
<a name="registration"></a>
<form action="__ACTION__#lastcheck" method="POST" class="agv-form">
<input type="hidden" name="ThisPage" value="2">
<input type="hidden" name="NextPage" value="4">
<input type="hidden" name="oldBereich" value="__oldBereich__">
<input type="hidden" name="oldAltBereich" value="__oldAltBereich__">
<span [sif:__ERROR__:neq:] class="agv-error">__ERROR__</span>
	<div class="agv-form__row">
		<label for="Bereich">Bereich:</label>
		<select name="Bereich" class="agv-input" __BereichReadonly__ required>
			__BereichOptions__
		</select>
	</div>
	<div class="agv-form__row">
		<label for="AltBereich">Alternativbereich:</label>
		<select name="AltBereich" class="agv-input" __AltBereichReadonly__>
			__AltBereichOptions__
		</select>
	</div>
	<div class="agv-form__row agv-form__row--checkbox">
		<input type="checkbox" name="Aufbau" value="true" __Aufbau__  class="agv-input agv-input--checkbox">
		<label for="Aufbau" class="agv-label agv-label--checkbox">
			Ich möchte am Aufbau teilnehmen.
		</label>
	</div>
	<div class="agv-form__row agv-form__row--abbau agv-form__row--checkbox">
		<input type="checkbox" name="Abbau" value="true" __Abbau__ class="agv-input agv-input--checkbox">
		<label for="Abbau" class="agv-label agv-label--checkbox">
			Ich m&ouml;chte zus&auml;tzlich auch am Abbau teilnehmen.
		</label>
	</div>
	<div class="agv-form__row agv-form__row--abbau">
		<label for="">Auf- und Abbaubereich:</label>
		<select name="AufbauBereich" class="agv-input" __AbbauBereichReadonly__>
			__AufbauBereichOptions__
		</select>
	</div>
	<div class="agv-form__row agv-form__row--abbau agv-form__row--note">
		Während des Auf- und Abbaus werden Mitarbeiter, die auch während des Festivals in der Küche gemeldet sind, automatisch in der Küche eingesetzt.
	</div>
	<div class="agv-form__row">
		<label for="FsErfahrungen">Freakstock-Erfahrungen:</label>
		<textarea name="FsErfahrungen" class="agv-input agv-input--textarea">__FsErfahrungen__</textarea>
	</div>
	<div class="agv-form__row">
		<label for="Bemerkungen">Bemerkungen:</label>
		<textarea name="Bemerkungen" class="agv-input agv-input--textarea">__Bemerkungen__</textarea>
	</div>
	<div class="agv-form__row agv-form__row--cta">
		<input type="submit" value="Weiter..." class="agv-button agv-button--primary">
	</div>
</form>
