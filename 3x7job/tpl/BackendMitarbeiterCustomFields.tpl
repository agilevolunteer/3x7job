<div class="wrap">
<h2>Zus&auml;tzliche Informationen f&uuml;r bestimmte Bereiche</h2>
<form action="__ACTION__" method="POST">
<input type="hidden" name="ThisPage" value="CustomFields">
<span [sif:__ERROR__:neq:] class="FsmaError">__ERROR__</span>	
	<table class="BackendFormTable">
	
		__CustomFieldItems__
	
		<tr class="submit">	
			<td>
			
			</td>
			<td align="right">
				<input type="submit" value="Speichern" class="FsmaButton">
			</td>
		</tr>	
	</table>
</form>
</div>