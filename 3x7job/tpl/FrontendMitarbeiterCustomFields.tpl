<a name="lastcheck"></a>
<h2>Zus&auml;tzliche Informationen f&uuml;r bestimmte Bereiche</h2>
<form action="__ACTION__#lastcheck" method="POST">
<input type="hidden" name="ThisPage" value="4">
<input type="hidden" name="NextPage" value="5">
<span [sif:__ERROR__:neq:] class="agv-error">__ERROR__</span>
	__CustomFieldItems__
	<div class="agv-form__row" [sif:__CustomHint0__:neq:]>
		__CustomHint0__
	</div>
	<div class="agv-form__row" [sif:__CustomHint1__:neq:]>
		__CustomHint1__
	</div>

	<div class="agv-form__row agv-form__row--cta">
		<input type="submit" value="Angaben überprüfen" class="agv-button agv-button--primary">
	</div>
</form>