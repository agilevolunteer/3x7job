<!--div style="float: right;"><a href="javascript:window.print()">Drucken</a></div><br><br-->
<table class="imagenav">
	<tr>
		<td class="spacer"></td>
		<td class="left">
			<a href="__CleanSortUrl__&pg=__PREVIOUSPAGE__" onfocus="if(this.blur)this.blur();" [sif:__PREVIOUSPAGE__:neq:0]><img src="__FSMAURL__/style/icons/arrow_left_blue_round.png"></a>
			<img src="__FSMAURL__/style/icons/arrow_2_left_round.png" [sif:__PREVIOUSPAGE__:eq:0]>
		</td>
		<td class="right">
			<a href="__CleanSortUrl__&pg=__NEXTPAGE__" onfocus="if(this.blur)this.blur();"><img src="__FSMAURL__/style/icons/arrow_right_blue_round.png"></a>
		</td>
	</tr>
</table>

<table class="widefat" cellpadding=0 cellspacing=0>
	<thead>
	<tr>
		<!--th width="170px" nowrap>
			Aktionen
		</th-->
		<th nowrap>
			Name
			<a href="__CleanSortUrl__&sort=Name-asc" [sifui:sort:neq:Name-asc] class="agv-arrow" onfocus="if(this.blur)this.blur();" ><img src="__FSMAURL__/style/icons/green_down.gif"></a>
			<img src="__FSMAURL__/style/icons/red_down.gif" [sifui:sort:eq:Name-asc]>

			<a href="__CleanSortUrl__&sort=Name-desc" [sifui:sort:neq:Name-desc] class="agv-arrow" onfocus="if(this.blur)this.blur();" ><img src="__FSMAURL__/style/icons/green_up.gif"></a>
			<img src="__FSMAURL__/style/icons/red_up.gif" [sifui:sort:eq:Name-desc] >
		</th>
		<th nowrap>
			Bereich

			<a href="__CleanSortUrl__&sort=Bereich-asc" [sifui:sort:neq:Bereich-asc] class="agv-arrow" onfocus="if(this.blur)this.blur();" ><img src="__FSMAURL__/style/icons/green_down.gif"></a>
			<img src="__FSMAURL__/style/icons/red_down.gif" [sifui:sort:eq:Bereich-asc]>

			<a href="__CleanSortUrl__&sort=Bereich-desc" [sifui:sort:neq:Bereich-desc] class="agv-arrow" onfocus="if(this.blur)this.blur();" ><img src="__FSMAURL__/style/icons/green_up.gif"></a>
			<img src="__FSMAURL__/style/icons/red_up.gif" [sifui:sort:eq:Bereich-desc] >
		</th>
		<th nowrap>
			Aufbau
		</th>
		<th nowrap>
			Abbau
		</th>
		<th nowrap>
			Bezahlt
		</th>
		<th>
			Bemerkungen MA Admin
		</th>
		<th nowrap>
			Details
		</th>
	</tr>
</thead>
<tbody>
