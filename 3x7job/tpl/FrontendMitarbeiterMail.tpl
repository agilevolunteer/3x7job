__ANMELDETEXT__


Persönliche Daten
=================

Vorname:		__vorname__
Nachname:		__nachname__
Straße / Hausnr.:	__strassehausnr__
PLZ:			__plz__
Ort:			__ort__
Telefon:		__VorwahlTele__ / __Tele__
Handy:			__VorwahlHandy__ / __Handy__
Geburtsdatum:		__gebdat__


Freakstockanmeldung
===================

Bereich:		__BereichsID__
Alternativbereich:	__AltBereichsID__
Bereits mitgearbeitet:	__Mitgearbeitet__
Teilnahme Aufbau: __Aufbau__
Teilnahme Abbau: __Abbau__
Aufbauteam: __AufbauBereichID__
Bett im Mehrbettzimmer: __Bett__
Erfahrungen:

__FsErfahrungen__

Bemerkungen:

__Bemerkungen__

Sonderangaben
=============

__CustomFields__



