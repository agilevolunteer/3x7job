<div class="wrap submit" id="maSearch">
	<h2>Nutzer umbenennen</h2>
	<br>
	<form action="__ACTIONURL__" method="POST">
		<input type="hidden" value="__ACTION__" 		name="action">
		<input type="hidden" value="__CHOSENUSER__" name="uidChosenUser">
		
		<input type="text" 		value="__SEARCHUSERNAME__" name="searchName" class="TextBox">
		<input type="submit" 	value="Suchen" name="btnSearch">
		<br><br>
	
		<span  [sif:__SEARCHUSERNAME__:neq:]>
		<span  [sif:__USEROPTIONS__:neq:]>				
			<select name="uidChoose" class="TextBox">
				__USEROPTIONS__
			</select>
			<input type="submit" value="Nutzer auswaehlen" name="btnChooseUser">
			<br><br>
	
			<span  [sif:__CHOSENUSERNAME__:neq:]>	
				<input type="text" value="__CHOSENUSERNAME__" class="TextBox" name="chosenUserName">
				<input type="submit" value="Nutzer umbenennen" name="btnRenameUser"><br>
				<a href="__USERPATH__?user_id=__CHOSENUSER__">Zum Profil des Nutzers</a>
			</span>
		</span>
		</span>				
		
		
	</form>
	<br>
</div>