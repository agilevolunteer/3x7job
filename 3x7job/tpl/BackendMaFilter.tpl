<div class="wrap"  id="maSearch">
	<form action="__ACTION__" method="POST">
	<input type="hidden" name="ThisPage" value="Filter">
		<table>
		<tr  [sif:__inDetailView__:eq:]>
			<td>
				__inDetailView__Hauptbereich
			</td>
			<!--td>
				Alternativbereich
			</td-->
			<td>
				Name
			</td>
			<td>
				Jahr
			</td>
			<td>
			
			</td>
		</tr>
		<tr class="submit">
			<td [sif:__inDetailView__:eq:]>
				<select name="bereich" type="TextBox">
					__BereichsOptions__
				</select>				
			</td>
			<!--td [sif:__inDetailView__:eq:]>
				<select name="altbereich" type="TextBox">
					__AltBereichsOptions__
				</select>
			</td-->
			
			<td [sif:__inDetailView__:eq:]>
				<input type="text" name="display_name" class="AdminTextBox" value="__DISPLAYNAME__">
			</td>
			
			<td>
				<select name="jahr" type="TextBox">
					__JahrOptions__
				</select>
			</td>
			
			<td>
				<input type="submit" value="Filtern" class="button">
			</td>
		</tr>
		<tr [sif:__inDetailView__:neq:]>
			<td colspan="2">
				<a href="__backurl__">Zur&uuml;ck zur Liste</a>
			</td>
		</tr>
		</table>
	</form>
</div>