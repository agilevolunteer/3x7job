<tr [sif:__ORDEREXP__:neq:] class="FsmaGroupSpacer">
	<td colspan="7">
		&nbsp;
	</td>
</tr>
<tr [sif:__ORDEREXP__:neq:] class="FsmaGroupHeaderRow">
	<td colspan="7">
		__ORDEREXP__ <span [sif:__ITEMCOUNT__:neq:]>(__ITEMCOUNT__)</span>
	</td>
</tr>
<tr>
	<td class="submit" nowrap>
		<span[sif:__BID__:neq:34]>
			<form action="__ACTION__" method="POST">
				<input type="hidden" name="pid" value="__PID__">
				<input type="hidden" name="bid" value="__BID__">
				<input type="hidden" name="uid" value="__UID__">
				<input type="hidden" name="page" value="listView">
				<input type="submit" name="action" value="Annehmen">
				<input type="submit" name="action" value="Ablehnen">
			</form>
		</span>
	</td>
	<td nowrap>
		__Name__
	</td>
	<td nowrap>
		<img src="__FSMAURL__style/icons/cancel_round.png" [sif:__Dismissed__:neq:] title="abgewiesen" \>
		<img src="__FSMAURL__style/icons/new.png" [sif:__New__:neq:] title="angemeldet" \>
		<img src="__FSMAURL__style/icons/accept_green.png" [sif:__Accept__:neq:] title="angenommen" \>
		__Bereich__
		<span [sif:__BID__:eq:0]>KEIN BEREICH ANGEGEBEN</span>
	</td>
	<td nowrap align="left">
		<img src="__FSMAURL__style/icons/cancel.png" [sif:__AufAbbau__:eq:0] \>
		<img src="__FSMAURL__style/icons/accept.png" [sif:__AufAbbau__:eq:1] \>
		<img class="Icon" src="__FSMAURL__style/icons/cancel.png"\ [sif:__AufAbbauConfirm__:eq:0]>
		<img class="Icon" src="__FSMAURL__style/icons/accept.png"\ [sif:__AufAbbauConfirm__:eq:1]>
	</td>
	<td>
		<div class="agv-bemerkung__preview" [sif:__ANRISS__:neq:...]>
			__ANRISS__
			<div class="agv-bemerkung__content">
				__Bemerkungen__
			</div>
		</div>
	</td>
	<td>
		<div class="agv-bemerkung__preview" [sif:__mabemerkunganriss__:neq:...]>
			__mabemerkunganriss__
			<div class="agv-bemerkung__content">
				__mabemerkung__
			</div>
		</div>
	</td>
	<td>
		<a href="__morelink__" id="morelink">Mehr..</a>
	</td>
</tr>
