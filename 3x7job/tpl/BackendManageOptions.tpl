<div class="wrap">
<h2 style="vertical-align: center;"><img src="__x7URL__/style/icons/threexsevenjob_HG.gif" /> Optionen</h2>
<form action="__ACTION__" method="POST">
	<input type="hidden" name="pagesIdent" value="FsmaOptions">
	<table class="BackendFormTable">
	
		<tr>
			<td>
			
			</td>
			<td class="InputCell">
				<input type="submit" value="Speichern" class="button button-primary">
			</td>
		</tr>

		<tr>
			<td class="label">
				Anmeldungen f&uuml;r das Jahr: 
			</td>
			<td class="InputCell">
				<INPUT TYPE="text" name="FsmaJahr" value="__FsmaJahr__" class="TextBox">
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Erster Eventtag: 
			</td>
			<td class="InputCell">
				<INPUT TYPE="text" name="FsmaTermin" value="__FsmaTermin__" class="TextBox">
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Ansicht f&uuml;r die Mitarbeiterliste: 
			</td>
			<td class="InputCell">
				<select name="FsmaMaList" size="1" class="TextBox">
					__FsmaMaList__
				</select>
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Mitarbeiter pro Seite: 
			</td>
			<td class="InputCell">
				<INPUT TYPE="text" name="FsmaPageItemCount" value="__FsmaPageItemCount__" class="TextBox">
			</td>
		</tr>
		
		<tr>
			<td class="label">
				
			</td>
			<td class="InputCell">
				<input type="checkbox" name="FsmaActive" value="false" __FsmaActive__> Anmeldung aktiviert
			</td>
		</tr>
		
		<tr>
			<td class="label">
				
			</td>
			<td class="InputCell">
				<input type="checkbox" name="FsmaIsBedActive" value="false" __FsmaIsBedActive__> Mitarbeiterbetten aktiviert
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Auffangpool f&uuml;r Mitarbeiter: 
			</td>
			<td class="InputCell">
				<select name="FsmaDismissedPool" size="1" class="TextBox">
					__BereichsOptions__
				</select>
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Pool f&uuml;r Absagen: 
			</td>
			<td class="InputCell">
				<select name="FsmaCanceledPool" size="1" class="TextBox">
					__AbgesagtBereichsOptions__
				</select>
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Defaultstatus: 
			</td>
			<td class="InputCell">
				<select name="FsmaDefStatus" size="1" class="TextBox">
					__StatusOptions__
				</select>
			</td>
		</tr>

		<tr>
			<td class="label">
				Abgelehntstatus: 
			</td>
			<td class="InputCell">
				<select name="FsmaDismissedStatus" size="1" class="TextBox">
					__DismissedStatusOptions__
				</select>
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Angenommenstatus: 
			</td>
			<td class="InputCell">
				<select name="FsmaAngenommenStatus" size="1" class="TextBox">
					__AngenommenStatusOptions__
				</select>
			</td>
		</tr>

		<tr>
			<td class="label">
				Offline-Text:
			</td>
			<td class="InputCell">
				<textarea name="FsmaOfflineText" class="TextAreaShort">__FsmaOfflineText__</textarea>
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Text f&uuml;r erfolgreiche Anmeldung:
			</td>
			<td class="InputCell">
				<textarea name="FsmaSuccessText" class="TextAreaShort">__FsmaSuccessText__</textarea>
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Text f&uuml;r erfolgreiche Anmeldungaktualisierung:
			</td>
			<td class="InputCell">
				<textarea name="FsmaSuccessUpdateText" class="TextAreaShort">__FsmaSuccessUpdateText__</textarea>
			</td>
		</tr>
		
		<tr>
			<td colspan="2" class="label">
				<h3>Anmeldungsmails</h3>
			</td>
			<td class="InputCell">
				
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Absender:
			</td>
			<td class="InputCell">
				<INPUT TYPE="text" name="FsmaMailAbsender" value="__FsmaMailAbsender__" class="TextBox">
			</td>
		</tr>

		<tr>
			<td class="label">
				Betreff:
			</td>
			<td class="InputCell">
				<INPUT TYPE="text" name="FsmaSuccessMailBetreff" value="__FsmaSuccessMailBetreff__" class="TextBox">
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Mailtext:<br>
				Mögliche Platzhalter<br>
				__vorname__<br>
				__nachname__
			</td>
			<td class="InputCell">
				<textarea name="FsmaSuccessMailBody" class="TextArea">__FsmaSuccessMailBody__</textarea>
			</td>
		</tr>

		<tr>
			<td colspan="2" class="label">
				<h3>Mail, wenn Nutzer angenommen</h3>
			</td>
			<td class="InputCell">
				
			</td>
		</tr>

		<tr>
			<td class="label">
				Betreff:
			</td>
			<td class="InputCell">
				<INPUT TYPE="text" name="FsmaAngenommenMailBetreff" value="__FsmaAngenommenMailBetreff__" class="TextBox">
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Mailtext:<br />
				(ohne Aufbau)<br />
				<br />Unbedingt verwenden: __bereichstext__<br />
				<br />Optional: __bereichsname__
			</td>
			<td class="InputCell">
				<textarea name="FsmaAngenommenMailText" class="TextArea">__FsmaAngenommenMailText__</textarea>
			</td>
		</tr>
		
		<tr>
			<td class="label">
				Mailtext:<br />
				(mit Aufbau)<br />
				<br />Unbedingt verwenden: __bereichstext__<br /> <br />
			</td>
			<td class="InputCell">
				<textarea name="FsmaAngenommenMailMitAufbauText" class="TextArea">__FsmaAngenommenMailMitAufbauText__</textarea>
			</td>
		</tr>
		
		<tr>
			<td>
			
			</td>
			<td class="InputCell">
				<input type="submit" value="Speichern" class="button button-primary">
			</td>
		</tr>
	</table>
</form>
<form action="__ACTION__" method="POST">
<input type="hidden" name="pagesIdent" value="FsmaStatus">
	<h2>Neuer Status</h2>
	<input type="text" name="newStatus" value="">
	<input type="submit" value="Hinzuf&uuml;gen" class="button">
</form>
</div>
<div class="wrap">Version: __FsmaVersion__</div>
