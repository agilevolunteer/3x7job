<?php
//Some useful things
define('FSMADBVERSION', 20);
define('FSMAVERSION', 2016.1);
define('FSMAURL', plugins_url( '/' , __FILE__ ));
define('FSMAPATH',dirname(__FILE__).'/');
define('FSMASQL',FSMAPATH."/sql/");
define('FSMATPL',FSMAPATH."/tpl/");
//define("WP_FSMA", "<!--subscribeNL-->");
define("FSMADEBUG",false);
//ab Version 0.9.4 gibts den Shadowordner
define('X7PATH',dirname(__FILE__).'/');
define('X7SHADOW',dirname(__FILE__).'/modules/shadow/');
define('X7SQL',"sql/");
define('X7TPL',"tpl/");
define('X7URL',FSMAURL);
define('X7BASE', plugin_basename( __FILE__ ));

//Put this on the Frontend
define("FSMAWORKERSREG", "<!--FSMAWORKERSREG-->");
define("FSMABEREICHE", "<!--FSMABEREICHE-->");
//definde("FSMAVAL", new jfbremenValidate());

require_once(ABSPATH."wp-includes/registration.php");
require_once(FSMAPATH."core/init.php");

function FsmaInsertLightAnmeldung($BID, $UID)
{
	global $table_prefix;

	if (!empty($BID) && !empty($UID))
	{
		$query 						= new jfbremenTemplate(FSMASQL."InsertLightAnmeldung.sql");
		$params 					= array();
		$params['__prefix__'] 		= $table_prefix;
		$params['__UserID__'] 		= $UID;
		$params['__BereichsID__'] 	= $BID;
		$params['__STATUS__'] 		= get_option("FsmaDefStatus");
		$params['__Jahr__'] 		= get_option("FsmaJahr");

		$query->DoMultipleQuery(true, $params);
	}

}


function FsmaPluginBasename($file) {
	$file = preg_replace('/^.*wp-content[\\\\\/]plugins[\\\\\/]/', '', $file);
	return $file;
}

//Frontendfunctions
function FsmaNoReg($content)
{
	$output = get_option('FsmaOfflineText');
	$content = str_replace(FSMAWORKERSREG,$output,$content);
	$content = str_replace(FSMABEREICHE,$output,$content);

	return $content;
}

function FsmaOnlyMe4DropDown($user)
{
	$option = new x7Template(X7TPL."option.tpl");
	$html   = "";

	$params = array();
	$params["__VALUE__"] =  $user->id;
	$params["__TEXT__"]  =  $user->display_name;
	$params["__SELECTED__"]  =  "SELECTED";

	$html .= $option->GetFilteredContent($params, true);
	return $html;
}


//TODO: Raus ins Module!
function FsmaGetUsersOhneAnmeldung()
{
	global $table_prefix;

	$option = new x7Template(X7TPL."option.tpl");
	$users  = new jfbremenTemplate(FSMASQL."UserOhneAnmeldung.sql");
	$html   = "";

	$users = $users->DoMultipleQuery(true, array("__PREFIX__" => $table_prefix, "__WHERE__" => $where), ARRAY_A);
	for ($i=0;$i<count($users[0]);$i++)
	{
		$user = $users[0][$i];

		$params = array();
		$params["__VALUE__"] =  $user["ID"];
		$params["__TEXT__"]  =  $user["display_name"];
		if ($user["ID"] == $selectedID)
			$params["__SELECTED__"]  =  "SELECTED";

		$html .= $option->GetFilteredContent($params, true);
	}

	return $html;

}

function FsmaGetUsers4DropDown($selectedID=-1, $where = "0=0")
{
	global $table_prefix;

	$option = new x7Template(X7TPL."option.tpl");
	$users  = new jfbremenTemplate(FSMASQL."Users4DropDown.sql");
	$html   = "";

	$users = $users->DoMultipleQuery(true, array("__PREFIX__" => $table_prefix, "__WHERE__" => $where), ARRAY_A);
	for ($i=0;$i<count($users[0]);$i++)
	{
		$user = $users[0][$i];

		$params = array();
		$params["__VALUE__"] =  $user["ID"];
		$params["__TEXT__"]  =  $user["display_name"];
		if ($user["ID"] == $selectedID)
			$params["__SELECTED__"]  =  "SELECTED";

		$html .= $option->GetFilteredContent($params, true);
	}

	return $html;
}

function FsmaGetDismissedBereich4DropDown($id, $Jahr, $BID = "", $returnPool = false)
{
	global $wpdb;
	global $table_prefix;
	$query = new jfbremenTemplate(FSMASQL."GetDismissedBereich.sql");
	$queryParams["__prefix__"] = $table_prefix;
	$queryParams["__UID__"]    = $id;
	$queryParams["__Jahr__"]   = $Jahr;
	$queryParams["__BID__"]   = (empty($BID)) ? -1 : $BID;


	$result = $query->DoMultipleQuery(true, $queryParams);
	$Pool = $result[0][0];

	//$BID = $wpdb->get_var($queryText);
	if ($returnPool == true)
	{
		return array($Pool, FsmaGetBereiche4DropDown($Pool->BID_Bereiche));
	}
	else
	{
		return FsmaGetBereiche4DropDown($Pool->BID_Bereiche);
	}
}

function FsmaGetJahr4DropDown($selectedJahr = "", $where = "0=0")
{
	$selectedJahr = (empty($selectedJahr)) ? get_option('FsmaJahr') : $selectedJahr;
	return FsmaGetBereiche4DropDown($selectedJahr, $where, "Jahre4DropDown.sql");
}

function FsmaGetStatus4DropDown($selectedID = -1, $where = "0=0")
{
	return FsmaGetBereiche4DropDown($selectedID, $where, "Status4DropDown.sql");
}

function x7GetFood4DropDown($selectedID = -1, $where = "0=0")
{
	return FsmaGetBereiche4DropDown($selectedID, $where, "Food4DropDown.sql");
}


function x7GetAufbauBereiche4DropDown($selectedID = -1, $where = "0=0")
{
	return FsmaGetBereiche4DropDown($selectedID, $where, "AbbauBereiche4DropDown.sql");
}

function FsmaGetBereiche4DropDown($selectedID = -1, $where = "0=0", $prod = "Bereiche4DropDown.sql")
{
	global $table_prefix;
	//echo $selectedID;
	$params = array();
	$option = new x7Template(X7TPL."option.tpl");
	$values  = new jfbremenTemplate(FSMASQL.$prod);
	$html   = "";

	$params["__VALUE__"] =  -1;
	$params["__TEXT__"]  =  "";
	if ($selectedID == -1)
			$params["__SELECTED__"]  =  'SELECTED="selected"';

	$html .= $option->GetFilteredContent($params, true);

	$values = $values->DoMultipleQuery(true, array("__PREFIX__" => $table_prefix, "__WHERE__" => $where), ARRAY_A);
	for ($i=0;$i<count($values[0]);$i++)
	{
		$value = $values[0][$i];

		$params = array();
		$params["__VALUE__"] =  $value["id"];
		$params["__TEXT__"]  =  $value["text"];
		if ($value["id"] == $selectedID)
		{
			$params["__SELECTED__"]  =  'SELECTED="selected"';
			//echo $value["id"];
		}
		else
			$params["__SELECTED__"]  =  '';

		$html .= $option->GetFilteredContent($params, true);
	}

	return $html;
}

function FsmaParseCustomFieldsBackend($BereichsID = -1, $AltBereichsID = -1, $UID, $template = "BackendMitarbeiterCustomItems.tpl")
{
	$val = new jfbremenValidate();
	$values = array();
	global $table_prefix;
	$CustomItemsTemplate              = new x7Template(X7TPL.$template);
	$CustomFields                     = new jfbremenTemplate(FSMASQL."CustomFieldsBackend.sql");
	$CustomFieldsParams               = array();
	$CustomFieldsParams["__prefix__"] = $table_prefix;


	if (current_user_can("edit_all_ma"))
	{
		$CustomFieldsParams["__BID1__"]   = -1;
		$CustomFieldsParams["__BID2__"]   = -1;
		$CustomFieldsParams["__ALLES__"]   = "true";
	}
	else
	{
		$CustomFieldsParams["__BID1__"]   = $BereichsID;
		$CustomFieldsParams["__BID2__"]   = $AltBereichsID;
		$CustomFieldsParams["__ALLES__"]   = "false";
	}

	$output = "";

	$result = $CustomFields->DoMultipleQuery(true, $CustomFieldsParams, ARRAY_N);

	for ($i=0;$i<count($result[0]);$i++)
	{
		$Bereich = $result[0][$i][0];
		$Items = explode(",", $Bereich);

		for ($j=0; $j<count($Items); $j++)
		{
			$Item = $Items[$j];

			if (strlen(trim($Item)) > 0)
			{
				$CustomItems = array();
				$key = FsmaEncode($Item);
				$CustomItems["__TEXT__"]  = attribute_escape($Item);
				$CustomItems["__NAME__"]  = $key;
				$CustomItems["__VALUE__"] = get_usermeta($UID,$key);

				$output .= $CustomItemsTemplate->GetFilteredContent($CustomItems, true);
			}
		}
	}


	return $output;
}

function FsmaParseCustomFields($BereichsID = -1, $AltBereichsID = -1, $template = "FrontendMitarbeiterCustomItems.tpl", $values = array())
{
	$val = new jfbremenValidate();
	global $table_prefix;
	$CustomItemsTemplate              = new x7Template(X7TPL.$template);
	$CustomFields                     = new jfbremenTemplate(FSMASQL."CustomFields.sql");
	$CustomFieldsParams               = array();
	$CustomFieldsParams["__prefix__"] = $table_prefix;
	$CustomFieldsParams["__BID1__"]   = $BereichsID;
	$CustomFieldsParams["__BID2__"]   = $AltBereichsID;
	$output = "";

	$result = $CustomFields->DoMultipleQuery(true, $CustomFieldsParams, ARRAY_N);

	for ($i=0;$i<count($result[0]);$i++)
	{
		$Bereich = $result[0][$i][0];
		$Items = explode(",", $Bereich);

		foreach($Items as $Item)
		{
			if (strlen(trim($Item)) > 0)
			{
				$CustomItems = array();
				$key = FsmaEncode($Item);
				$CustomItems["__TEXT__"]  = attribute_escape($Item);
				$CustomItems["__NAME__"]  = $key;
				$CustomItems["__VALUE__"] = $values[$key];

				$output .= $CustomItemsTemplate->GetFilteredContent($CustomItems, true);
			}
		}
	}

	return $output;
}

function FsmaLoadCustomHints($BereichsID = -1, $AltBereichsID = -1) {
	global $table_prefix;
    $CustomFields                     = new jfbremenTemplate(FSMASQL."CustomHints.sql");
	$CustomFieldsParams               = array();
	$CustomFieldsParams["__prefix__"] = $table_prefix;
	$CustomFieldsParams["__BID1__"]   = $BereichsID;
	$CustomFieldsParams["__BID2__"]   = $AltBereichsID;
	$result = $CustomFields->DoMultipleQuery(true, $CustomFieldsParams, ARRAY_N);
	$output = array();

	for ($i=0;$i<count($result[0]);$i++){
		$output[$i] = $result[0][$i][0];
	}

	return $output;
}

function FsmaGetContent4ID($id = -1, $col = "value", $sql="GetAufbauBereich4ID.sql")
{
	global $wpdb;
	global $table_prefix;
	$query = new jfbremenTemplate(FSMASQL.$sql);
	$queryParams["__prefix__"] = $table_prefix;
	$queryParams["__id__"]    = $id;
	$queryParams["__col__"]    = $col;

	$queryText = $query->GetFilteredContent($queryParams);
	return $wpdb->get_var($queryText);

}

function FsmaGetBereich4ID($id = -1, $col = "Bezeichnung")
{
	global $wpdb;
	global $table_prefix;
	$query = new jfbremenTemplate(FSMASQL."GetBereich4ID.sql");
	$queryParams["__prefix__"] = $table_prefix;
	$queryParams["__BID__"]    = $id;
	$queryParams["__col__"]    = $col;

	$queryText = $query->GetFilteredContent($queryParams);
	return $wpdb->get_var($queryText);

}

function FsmaGetBereichByLeiter($id = -1, $col = "BID_Bereich", $isGetAll = false)
{
	global $wpdb;
	global $table_prefix;
	$query = new jfbremenTemplate(FSMASQL."GetBereichByLeiter.sql");
	$queryParams["__prefix__"] = $table_prefix;
	$queryParams["__UID__"]    = $id;
	$queryParams["__col__"]    = $col;

	$queryText = $query->GetFilteredContent($queryParams);
 if ($isGetAll == true){
    return $wpdb->get_col($queryText);
 }else{
	    return $wpdb->get_var($queryText);
 }
}

function FsmaEncode($string)
{
    $new = strtolower(trim($string));

    // TODO This strips out also multibyte-chars - should it changed?
    $new = preg_replace('#[^a-z0-9]#', '_', $new);
	return trim($new);
}

function FsmaInsertNewStatus($value)
{
	global $wpdb;
	global $table_prefix;

	if (!empty($value))
		$wpdb->query("insert into ".$table_prefix."status (`value`) VALUES ('".$wpdb->escape($value)."')");
}



?>
