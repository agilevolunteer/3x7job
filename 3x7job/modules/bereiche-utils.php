<?php 
function x7ShowToggleState()
{
	$params = $_SESSION['x7BackendBereicheDetailItemOUT'];
	$in		= $_SESSION['x7BackendBereicheDetailItemIN'];
	$params["__dings__"]  =  $in["BID"];	
	
	$_SESSION['x7BackendBereicheDetailItemOUT'] = $params;	
}

function x7GetLeiterByBereichsId4Checkbox($BereichsId, $name, $template="checkboxBL.tpl")
{
	global $table_prefix;
	
	$option = new x7Template(X7TPL.$template);
	$BLs  = new x7Template(X7SQL."GetBereichsleiterByBereich.sql");
	$html   = "";
	
	$BLs = $BLs->DoMultipleQuery(true, 
			array(
				"__PREFIX__" => $table_prefix, 
				"__BID__" => $BereichsId				
			), ARRAY_A);
	for ($i=0;$i<count($BLs[0]);$i++)
	{
		$user = $BLs[0][$i];
		
		$user_object = new WP_User($user["ID"]);
		$roles = $user_object->roles;
		$rolesString = "";
		for ($j = 0; $j < count($roles); $j++)
		{
			$rolesString .= $roles[$j];			
		}
				
		$params = array();
		$params["__VALUE__"] 	= $user["ID"];
		$params["__TEXT__"]  	= $user["display_name"];
		$params["__SENDER__"] 	= $user["sender"];
		//$params["__ACTIONURL__"]	= FsmaAddUrlParam("b", $BereichsId);
		$params["__ACTIONURL__"]	= FsmaAddUrlParam("bl", $user["ID"]);//, $params["__ACTIONURL__"]);
		$params["__X7URL__"]	= X7URL;
		$params["__NAME__"] 	= $name;
		$params["__ROLES__"]	= $rolesString;
		$params["__USERPAGE__"]	= get_bloginfo("wpurl")."/wp-admin/user-edit.php?user_id=".$user["ID"];
		
		
		$html .= $option->GetFilteredContent($params, true);
	}
	
	return $html;
}

function x7GetBereichsleiter4DropDown($selectedID=-1, $where = "0=0")
{
	global $table_prefix;

	$option = new x7Template(X7TPL."option.tpl");
	$users  = new x7Template(X7SQL."Bereichsleiter4DropDown.sql");
	$html   = "";
	
	$users = $users->DoMultipleQuery(true, array(
		"__PREFIX__" => $table_prefix, 
		"__WHERE__" => $where,
		"__BID__" => $selectedID,	
	), ARRAY_A);
	for ($i=0;$i<count($users[0]);$i++)
	{
		$user = $users[0][$i];
		
		$params = array();
		$params["__VALUE__"] =  $user["ID"];
		$params["__TEXT__"]  =  $user["display_name"];
		if ($user["ID"] == $selectedID)
			$params["__SELECTED__"]  =  "SELECTED";
		
		$html .= $option->GetFilteredContent($params, true);
	}
	
	return $html;
}

function x7SaveBereich()
{
	$in		= $_SESSION['x7BackendBereicheSaveBereichIN'];
	if ($in["action"] != "saveAddInfo") return;
	
	
	global $table_prefix;
	$params = $_SESSION['x7BackendBereicheSaveBereichOUT'];
	
	
	if ($in["BID"] == -1)
	{
		$saveQuery = new x7Template(X7SQL."InsertNewBereich.sql");
	}
	else
	{
		$saveQuery = new x7Template(X7SQL."UpdateBereich.sql");
	}
			
	$SqlParams                      = array();
	$SqlParams["__prefix__"]        = $table_prefix;
	$SqlParams["__BID__"]           = $in["BID"];
	$SqlParams["__bezeichnung__"] 	= $in["bezeichnung"];
    $SqlParams["__kurztext__"] 		= $in["kurztext"];	
    $SqlParams["__langtext__"] 		= $in["langtext"];		
    $SqlParams["__FSK__"] 			= $in["FSK"];
    $SqlParams["__MailNormal__"] 	= $in["MailNormal"];				
    $SqlParams["__MailAufbau__"] 	= $in["MailAufbau"];		
    $SqlParams["__CustomFields__"] 	= $in["CustomFields"];
    $SqlParams["__CustomHint__"]    = $in["CustomHint"];
    $SqlParams["__locked__"] 		= (isset($in["locked"])) ? "b'001'" : "b'000'";
	$SqlParams["__VisInList__"] 		= (isset($in["VisInList"])) ? "b'001'" : "b'000'";
	
	$result = $saveQuery->DoMultipleQuery(true, $SqlParams, ARRAY_A, true);
		
	
	if ($saveQuery->Succeeded() == true)
		FsmaMessage("Der Bereich wurde erfolgreich gespeichert.");
	else
	{
		FsmaError("Fehler beim Speichern des Bereichs:<br>".$saveQuery->GetErrorText());		
	}
}

function x7PromoteBereichsleiter()
{
	$in		= $_SESSION['x7BackendBereicheSaveBereichIN'];
	if ($in["action"] != "promoteBL") return;
	
	global $table_prefix;	
	$promoteQuery = new x7Template(X7SQL."PromoteBL.sql");
	$SqlParams                  = array();
	$SqlParams["__prefix__"]    = $table_prefix;
	$SqlParams[" __bereich__"]  = $in["BID"];
	$SqlParams["__user__"] 		= $in["bereichsleiter"];
	
	$result = $promoteQuery->DoMultipleQuery(true, $SqlParams, ARRAY_A, true);
	
	if ($promoteQuery->Succeeded() == true)
		FsmaMessage("Der Bereichleiter  wurde zugewiesen.");
	else
	{
		FsmaError("Fehler beim Zuweisen der Bereichsleiter:<br>".$promoteQuery->GetErrorText());		
	}
	
}

function x7DismissBereichsleiter()
{
	$in		= $_SESSION['x7BackendBereicheSaveBereichIN'];
	if ($in["action"] != "dismissBL") return;	
	if (count($in["BLs"]) < 1)
	{
		FsmaError("Es wurde kein Bereichsleiter zum L&ouml;schen ausgew&auml;hlt!");
		return;
	}
	
	global $table_prefix;
		
	$dismissQuery = new x7Template(X7SQL."DismissBL.sql");
	$SqlParams                  = array();
	$SqlParams["__prefix__"]    = $table_prefix;
	$SqlParams[" __bereich__"]  = $in["BID"];
		
	for ($i = 0; $i < count($in["BLs"]); $i++)
	{		
		$SqlParams["__user__"] 		= $in["BLs"][$i];
		$dismissQuery->DoMultipleQuery(true, $SqlParams, ARRAY_A, true);		
	}
	
	if ($dismissQuery->Succeeded() == true)
	{
		FsmaMessage("Bereichsleiter wurden erfolgreich entlassen.");
	}
	else
	{
		FsmaError("Fehler beim Löschen eines Bereichsleiters:<br>".$dismissQuery->GetErrorText());
		$errorsOccured = true; 					
	}	
}

function x7PromoteAsOnlySender($bereich, $leiter)
{
	global $table_prefix;
	global $wpdb;
	$isBereichsleiter = $wpdb->get_var(
			"SELECT count(*) FROM ".$table_prefix."bereichsleiter f where BID_Bereiche = $bereich AND Uid_users = $leiter"
			);
			
	if ($isBereichsleiter < 1) return;
	
	$senderQuery = new x7Template(X7SQL."SetBereichMailSender.sql");
	$SqlParams                  = array();
	$SqlParams["__PREFIX__"]    = $table_prefix;
	$SqlParams[" __BID__"]  	= $bereich;
	$SqlParams[" __UID__"]  	= $leiter;
	
	$senderQuery->DoMultipleQuery(true, $SqlParams, ARRAY_A, true);
	
	if ($senderQuery->Succeeded() == true)
	{
		FsmaMessage("Absender erfolgreich ge&auml;ndert.");
	}
	else
	{
		FsmaError("Fehler beim &Auml;ndern des Absenders:<br>".$dismissQuery->GetErrorText());
		$errorsOccured = true; 					
	}
}
?>