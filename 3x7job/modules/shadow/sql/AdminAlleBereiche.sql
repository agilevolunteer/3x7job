sELECT b.BID, b.Bezeichnung, b.kurztext as Preview, b.arbeitszeit as Arbeitszeit, IF(b.locked= b'001',TRUE,FALSE) as Geschlossen, IF(b.VisInList= b'001',true,false) as Listenanzeige,
CONCAT(u.display_name, '<br>(', u.user_login, ')') as Leiter, IF(bl.sender= b'001',1,'') as sender
FROM __prefix__bereiche b
LEFT OUTER JOIN  __prefix__bereichsleiter bl
on b.BID = bl.BID_bereiche
LEFT OUTER JOIN __prefix__users u
ON bl.Uid_users = u.ID
WHERE __WHERE__ AND isDeleted = 0 ORDER BY Bezeichnung
__LIMIT__