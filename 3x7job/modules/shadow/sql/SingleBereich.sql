SELECT b.BID, b.Bezeichnung, b.langtext as Description, b.Arbeitszeit, u.display_name as Bereichsleiter
FROM __prefix__bereiche b 
LEFT OUTER JOIN __prefix__bereichsleiter bl
ON b.BID = bl.BID_Bereiche
RIGHT OUTER JOIN __prefix__users u 
ON bl.Uid_users = u.ID 
WHERE VisInList = b'1' AND b.BID = __BID__