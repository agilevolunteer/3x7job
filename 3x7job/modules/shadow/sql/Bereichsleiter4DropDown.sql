select ID, CONCAT(display_name, ' (', user_login, ')') as display_name 
FROM __PREFIX__users u
WHERE id NOT IN (SELECT Uid_users FROM __PREFIX__bereichsleiter WHERE BID_Bereiche = __BID__)
ORDER BY display_name
