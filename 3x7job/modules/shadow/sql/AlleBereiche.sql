SELECT DISTINCT b.BID, b.Bezeichnung, b.kurztext as Preview, b.arbeitszeit as Arbeitszeit
FROM __prefix__bereiche b 
WHERE VisInList = b'1' AND isDeleted = 0 __WHERE__
ORDER BY b.BID, Bezeichnung
__LIMIT__