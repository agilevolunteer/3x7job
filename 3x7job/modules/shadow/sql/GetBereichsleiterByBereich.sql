SELECT u.ID as ID, u.display_name as display_name, IF(bl.sender= b'001',1,0) as sender
FROM __PREFIX__bereichsleiter bl
INNER JOIN __PREFIX__users u
ON u.id = bl.Uid_users
WHERE bl.BID_Bereiche = __BID__
ORDER BY display_name