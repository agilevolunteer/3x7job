<div class="wrap">
<h2>Bereiche</h2>
<span [sif:__newlink__:neq:]><h3><a href="__newlink__">Neuen Bereich anlegen...</a></h3></span>
<div style="float:right; vertical-align: center;" valign="center">
	<img src="__X7URL__/style/icons/envelope.png" 
			alt="Absender f&uuml;r Bereichsmails" 
			alt="Absender f&uuml;r Bereichsmails"			 
			/> -> Absender f&uuml;r Mails des Bereichs
</div>
<table class="widefat" cellpadding=0 cellspacing=0>
	<thead>
	<tr>
		<th>
			Name
		</th>
		<th>
			Kurzbeschreibung
		</th>
		<th>
			Bereichsleiter
		</th>
		<th>
			&nbsp;
		</th>
	</tr>
	</thead>
	<tbody>