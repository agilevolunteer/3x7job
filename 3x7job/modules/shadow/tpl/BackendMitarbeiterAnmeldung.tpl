<div class="wrap submit">
<form action="__ACTION__" method="POST">
<input type="hidden" name="ThisPage" value="Anmeldung">
<input type="hidden" name="AnmeldeID" value="__AnmeldeID__">
<input type="hidden" name="Jahr" value="__Jahr__">
<input type="hidden" name="PrPool" value="__PrPool__">
<input type="hidden" name="AltPool" value="__AltPool__">
<input type="hidden" name="DismissedPool" value="__DismissedPool__">
<h2>Anmeldung __Jahr__ <input type="submit" value="anlegen" name="save" class="FsmaButton button button-primary" [sif:__AnmeldeID__:eq:]></h2>
<span [sif:__ERROR__:neq:] class="FsmaError">__ERROR__</span>
	<table class="BackendFormTable" [sif:__AnmeldeID__:neq:]>

		<tr class="submit">
			<td class="label">

			</td>
			<td class="InputCell">
				<input type="submit" value="Speichern" name="save" class="FsmaButton button button-primary">
			</td>
		</tr>

		<tr class="__EnablePrPool__ submit">
			<td class="label">
				Bereich*:
			</td>
			<td class="InputCell">
				<select name="Bereich" class="DropDown" __EnablePrPool__>
					__BereichOptions__
				</select>
				<select name="BereichStatus" class="DropDown" __EnablePrPool__>
					__BereichStatusOptions__
				</select>
				<input type="submit" value="Abweisen" name="abwBereich" class="FsmaButton button button-primary" [sif:__EnablePrPool__:eq:]>
			</td>
		</tr>

		<tr class="__EnableAltPool__ submit">
			<td class="label">
				Alternativbereich:
			</td>
			<td class="InputCell">
				<span [sif:__AltPool__:neq:]>
					<select name="AltBereich" class="DropDown" __EnableAltPool__>
						__AltBereichOptions__
					</select>
					<select name="AltBereichStatus" class="DropDown" __EnableAltPool__>
						__AltBereichStatusOptions__
					</select>
					<input type="submit" value="Abweisen" name="abwAltBereich" class="FsmaButton button button-primary" [sif:__EnableAltPool__:eq:]>
				</span>
				<span [sif:__AltPool__:eq:]>
					Der Mitarbeiter hat keinen Alternativbereich gew&auml;hlt.
				</span>
			</td>
		</tr>

		<tr class="__EnableDismissedPool__">
			<td class="label">
				"Schwimm"-bereich:
			</td>
			<td class="InputCell">
				<select name="DismissedBereich" class="DropDown" __EnableDismissedPool__>
					__DismissedBereichOptions__
				</select>
			</td>
		</tr>

		<tr class="submit">
			<td class="label">
				Aufbauteam:
			</td>
			<td class="InputCell">
					<select name="AufbauBereich" class="DropDown">
						__AufbauBereichOptions__
    				</select>
			</td>
		</tr>

		<tr>
			<td></td>
			<td>
				<input type="checkbox" name="AufbauAbbau" value="true" __AufbauAbbau__>
				<span class="checkBoxText">Teilnahme am Aufbau</span>
			</td>
		</tr>

        <tr>
			<td></td>
			<td>
				<input type="checkbox" name="Abbau" value="true" __Abbau__>
				<span class="checkBoxText">Teilnahme am Abbau (nur in Kombination mit dem Aufbau!)</span>
			</td>
		</tr>

		
		<tr>
			<td></td>
			<td>
				<input type="checkbox" name="Mitgearbeitet" value="true" __Mitgearbeitet__>
				<span class="checkBoxText">Ja, ich habe bereits mitgearbeitet.</span>
			</td>
		</tr>

		<tr>
			<td></td>
			<td>
				<input type="checkbox" name="Bezahlt" value="true" __Bezahlt__>
				<span class="checkBoxText">Bezahlt</span>
			</td>
		</tr>

		<tr>
			<td></td>
			<td>
				<input type="checkbox" name="Eingecheckt" value="true" __Eingecheckt__>
				<span class="checkBoxText">Eingecheckt</span>
			</td>
		</tr>

		<tr>
			<td class="label" >
				Freakstock-Erfahrungen:
			</td>
		</tr>

		<tr>
			<td></td>
			<td class="colspan2TextArea" colspan="2" >
				<textarea name="FsErfahrungen" class="TextAreaShort">__FsErfahrungen__</textarea>
			</td>
		</tr>

		<tr>
			<td class="label">
				Bemerkungen:
			</td>
		</tr>

		<tr>
			<td></td>
			<td class="colspan2TextArea" colspan="2" >
				<textarea name="Bemerkungen" class="TextAreaShort">__Bemerkungen__</textarea>
			</td>
		</tr>

		<tr>
			<td class="label">

			</td>
			<td class="InputCell submit">
				<input type="submit" value="Speichern" name="save" class="FsmaButton button button-primary">
			</td>
		</tr>

	</table>
</form>
</div>
