<div class="wrap submit">
<h2>__Bezeichnung__</h2>
<!--<a href="__ACTION__">__ACTION__</a>
--><input type="hidden" name="Arbeitszeit" value="0" class="TextBoxShort">
<!-- Leitung -->
<table class="BackendFormTable">
	<tr>
		<td>
		<div [sif:__BID__:neq:-1]>
		<a href="__BACKURL__">Zur&uuml;ck zur Liste...</a>
		<br />
		<br />
		
		<table cellpadding="0" cellspacing="0" class="separateBackendTable">
			<tr>
				<td>
					Neue Bereichsleiter zuweisen 
					<br />
					
					<form action="__ACTION__" method="POST">
						<input type="hidden" name="BID" value="__BID__" />
						<input type="hidden" name="action" value="promoteBL" /> 
						<select 
							name="bereichsleiter" 
							class="TextBox inputField" 
							
							__BereichsleiterEnabled__ 
						>
						__BereichsleiterOptions__
						</select> 
						<div class="ButtonsRight">
							<br />
							<input 
								type="submit" 
								value="&Uuml;bernehmen" 
								class="x7Button button button-primary">
							<a href="__BACKURL__">Zur&uuml;ck zur Liste</a>
						</div>
					</form>
				</td>
			</tr>
		</table>
		<br />
		
		<br />	
		<br />
			
		<table cellpadding="0" cellspacing="0" class="separateBackendTable">
			<tr>
				<td>
					<br />
					Bestehende Bereichsleiter entlassen 
					<br />
					
					<form action="__ACTION__" method="POST">
						<input type="hidden" name="action" value="dismissBL" />
						<input type="hidden" name="BID" value="__BID__" />
						<div class="inputField">
						<table>						
							__BereichsleiterCheckbox__
						</table>
						</div>
						
						<div class="ButtonsRight">
							<input 
								type="submit" 
								value="&Uuml;bernehmen" 
								class="x7Button button button-primary">
							<a href="__BACKURL__">Zur&uuml;ck zur Liste</a>
						</div>
					</form>
				</td>
			</tr>
		</table>
		<br />
		<!-- Ende Leitung -->

		
		<br />
		<br />
</div>
		<form action="__ACTION__" method="POST">
			<input type="hidden" name="BID" value="__BID__" />
			<input type="hidden" name="action" value="saveAddInfo" />
			<table cellpadding="0" cellspacing="0" class="separateBackendTable">
			<tr>
				<td>
					<div class="ButtonsRight">
						<input 
							type="submit" 
							value="&Uuml;bernehmen" 
							class="x7Button button button-primary">
						<a href="__BACKURL__">Zur&uuml;ck zur Liste</a>		
					</div>
					
					<br />
					Bezeichnung:<br />
					<INPUT 
						TYPE="text" 
						name="bezeichnung"
						value="__Bezeichnung__" 
						class="TextBox inputField">	
					<br />
					<br />
					
	
					Kurzbeschreibung:<br />
					<textarea 
						name="kurztext" 
						class="TextAreaShort inputField">__Preview__
					</textarea>
					
					<br />
					
					
					Beschreibung:<br />
					<textarea 
						name="langtext" 
						class="TextArea inputField">__Description__
					</textarea>
					<br />
					
					
					<input 
						type="checkbox" 
						name="VisInList" 
						value="false"
						class="inputField"
						__VisInList__ /> 
					soll in der Bereichsübersicht im Frontend angezeigt werden
					<br />
					
	
					<input 
						type="checkbox" 
						name="locked" 
						value="false"
						class="inputField"
						__LOCKED__ /> 
					Anmeldung am Bereich sind nicht möglich, es sind ausreichend Mitarbeiter vorhanden
					<br />
					<br />
						
					Mindesalter:<br />
					<input 
						type="text" 
						name="FSK" 
						value="__FSK__"
						class="TextBoxShort inputField"> Jahre					
					<br />
					<br />
	
					Bereichsspezifischer Text f&uuml;r Angenommenmail:<br />
					<textarea name="MailNormal"	class="TextArea inputField">__MailNormal__</textarea>					
					<br /><br />
					So sieht die Mail aus, die an den angenommenen Mitarbeiter geschickt wird:
					
					<div style="max-width: 400px; padding: 15px; border: 1px solid #d6d6d6; margin: 15px;">
						__EmailNormalText__
					</div>
										
					<br />					
					Bereichsspezifischer Text f&uuml;r Angenommenmail (mit Aufbau):<br />
					<textarea 
						name="MailAufbau" 
						class="TextArea inputField">__MailAufbau__</textarea>
					<br /><br />
					So sieht die Mail aus, die an den angenommenen Mitarbeiter geschickt wird:
					
					<div style="max-width: 400px; padding: 15px; border: 1px solid #d6d6d6; margin: 15px;">
						__EmailAufbauText__
					</div>
	
					<span [sif:__BereichsleiterEnabled__:eq:]>
						Benutzerdefinierte Felder f&uuml;r Mitarbeiter:<br />
						<textarea name="CustomFields" class="TextAreaShort inputField">__CustomFields__</textarea>
						<br />
					</span>
					
					<span [sif:__BereichsleiterEnabled__:eq:]>
						Bereichsspezifischer Hinweis:<br />
						<textarea name="CustomHint" class="TextAreaShort inputField">__CustomHint__</textarea>
						<br />
					</span>
	
					<div class="ButtonsRight">
						<input 
							type="submit" 
							value="&Uuml;bernehmen" 
							class="x7Button button button-primary">
						<a href="__BACKURL__">Zur&uuml;ck zur Liste</a>
					</div>
	
					
				</td>
			</tr>
			</table>
		</form>
		</td>
	</tr>
</table>
</div>
