<?php
function x7GetWorkshopInfos ( $args ) {
    //return "HAllo";
    
    global $wp_xmlrpc_server;
     $wp_xmlrpc_server->escape( $args );
     $username = $args[0];
     $password = $args[1];
     $user = $wp_xmlrpc_server->login($username, $password);
     $response = array();
	 
     //$response["user"] = $user;    
     if (!$user)
        return $wp_xmlrpc_server->error;
        //$response["error"] = "Loginfehler";
     if(current_user_can("use_fsma")){
       // $response["cap"] = "Can export";
        $bereich = FsmaGetBereichByLeiter($user->ID, "BID_Bereiche");		
        //$response["ma"] = 
        $response["ma"] = x7RpcLoadMa($bereich);
	    $response["workshops"] = x7RpcLoadWorkshops();
       
        
     }else{
       // $response["cap"] = "nope";
     }
     //$response["ma"] = x7LoadMa4Workshops();
     
     $response["status"] = 1;
     return $response;
}
    
 function x7job_xmlrpc_extention ( $methods ) {
    $methods['x7job.getWorkshopInfos'] = 'x7GetWorkshopInfos';
     return $methods;
 }
 add_filter( 'xmlrpc_methods', 'x7job_xmlrpc_extention');

function x7RpcLoadMa($bid){
    global $table_prefix;
	$maData = array();
    $maQuery  = new x7Template(X7SQL."GetMa4Workshops.sql" );
    
    if(!$maQuery->Succeeded())
        return $maQuery->GetErrorText();
	
	/*
	return $maQuery->GetFilteredContent(array(
				            "__PREFIX__" => $table_prefix,
                "__BID__" => $bid
			        ));
	 */  
    
    $maData = $maQuery->DoMultipleQuery(true, 
			        array(
				            "__PREFIX__" => $table_prefix,
                "__BID__" => $bid
			        ),ARRAY_A);
    //return $maQuery;  
     if(!$maQuery->Succeeded())
        return $maQuery->GetErrorText();   
     
    return $maData;
}

function x7RpcLoadWorkshops(){
    global $wpdb;
    
    return $wpdb->get_results(
        "select * from workshop"
    );
}




?>