jQuery(function () {
    // Class to represent subscriptions
    function Bereich(bid, bezeichnung, preview, geschlossen, listensichtbar, geloescht) {
        var self = this;
        var className = "inactive";
        if (geschlossen == 0) {
            className ="active";
        }

        if (geloescht == 1) {
            className = className+" agv-bereich--deleted";
        }

        self.bid = bid;
        self.bezeichnung = bezeichnung;
        self.preview = preview;
        self.geschlossen = ko.observable(geschlossen == 1);
        self.listensichtbar = ko.observable(listensichtbar == 1);
        self.geloescht = ko.observable(geloescht == 1);
        self.className = ko.observable(className);
        self.label = bezeichnung + " (" + bid + ")";
        self.value = bid;
    }

    function BereichslisteModel() {
        self = this;
        self.bereichsliste = ko.observableArray();
        self.filteredList = ko.observableArray();

        self.loadBereiche = function () {
            // here is where the request will happen
            jQuery.ajax({
                url: wpajax,
                cache: false,
                type: 'POST',
                data: {
                    'action': '3x7_loadBereiche'
                },
                dataType: 'JSON',
                success: function (data) {

                    var mappedBereiche = jQuery.map(data, function (item) {
                        return new Bereich(item.BID, item.Bezeichnung, item.Preview, item.Geschlossen, item.Listenanzeige, item.geloescht);
                    });

                    /*
                     var bereicheAC = jQuery.map(data, function(item) {
                     return item.Bezeichnung;
                     });
                     */
                    self.bereichsliste(mappedBereiche);
                    //self.filteredList(bereicheAC);
                    //debugger;

                },
                error: function (errorThrown) {
                    alert("Bereichsstatus konnte nicht ge�ndet werden");
                    console.log(errorThrown);
                }
            });
        };
        self.changeDesc = function (data, e) {

            item = data;
            var isVisible = 0;
            if (item.listensichtbar() == true) {
                isVisible = 1;
            }

            jQuery.ajax({
                url: wpajax,
                data: {
                    'action': '3x7_toggleBereichDescVisible',
                    'bid': item.bid,
                    'status': isVisible,
                    'll': 'oo'
                },
                dataType: 'JSON',
                type: 'POST',
                success: function (returnedData) {
                    // alert(returnedData);

                    /*
                     for(prop in returnedData ){
                     jQuery(".wrap").append("<p>"+prop+": "+ returnedData[prop]+"</p>");
                     }
                     */
                    item.listensichtbar(!item.listensichtbar());
                },
                error: function (errorThrown) {

                    //			alert(errorThrown.toString());

                    /*
                     for(prop in errorThrown){
                     jQuery(".wrap").append("<p>"+prop+": "+errorThrown[prop]+"</p>");
                     }
                     */

                    alert(item.bezeichnung + " konnte nicht angepasst werden.");
                }
            });
        };
        self.hiding = null;
        self.changeDelete = function (data, e) {
            var hiding = null;

            item = data;
            var isVisible = 0;
            if (item.geloescht() == true) {
                isVisible = 1;
            }

            jQuery.ajax({
                url: wpajax,
                data: {
                    'action': '3x7_toggleBereichDeleted',
                    'bid': item.bid,
                    'status': isVisible,
                    'll': 'oo'
                },
                dataType: 'JSON',
                type: 'POST',
                success: function (returnedData) {
                    // alert(returnedData);

                    /*
                     for(prop in returnedData ){
                     jQuery(".wrap").append("<p>"+prop+": "+ returnedData[prop]+"</p>");
                     }
                     */
                    item.geloescht(returnedData.geloescht == 1);

                    var hide = function(){
                        item.className("agv-bereich--deleted");
                    };
                    if (returnedData.geloescht == 1){
                        self.hiding = setTimeout(hide, 1750);
                    } else {
                        console.log("clear");
                        clearTimeout(self.hiding);

                        item.className("");
                    }
                },
                error: function (errorThrown) {

                    //			alert(errorThrown.toString());

                    /*
                     for(prop in errorThrown){
                     jQuery(".wrap").append("<p>"+prop+": "+errorThrown[prop]+"</p>");
                     }
                     */
                    console.log(errorThrown);
                    alert(item.bezeichnung + " konnte nicht angepasst werden.");
                }
            });
        };
        self.printObject = function (obj) {

        };
        self.toggleDeleted = function(){
            self.bereichsliste().forEach(function(entry){
                entry.className("");
            });
        }
        self.changeLocked = function (data, e) {
            item = data;
            var isLocked = 0;
            if (item.geschlossen() == false) {
                isLocked = 1;
            }
            jQuery.ajax({
                url: wpajax,
                data: {
                    'action': '3x7_toggleBereichLocked',
                    'bid': item.bid,
                    'status': isLocked
                },
                dataType: 'JSON',
                type: 'POST',
                success: function (returnedData) {

                    console.log(returnedData);
                    if (returnedData.Geschlossen == "0") {
                        item.className("active");
                        item.geschlossen(false);
                    }
                    else {
                        item.className("inactive");
                        item.geschlossen(true);
                    }
                },
                error: function (errorThrown) {
                    alert(item.bezeichnung + " konnte nicht angepasst werden.");
                    console.log(errorThrown);
                }
            });

            return true;
        }

    }

    var bereichslisteViewModel = new BereichslisteModel();
    bereichslisteViewModel.loadBereiche();

    // Activates ViewModel
    ko.applyBindings(bereichslisteViewModel);
});