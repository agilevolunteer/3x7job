<?php

require_once FSMAPATH.'modules/bereiche-utils.php';
$CurrentUser = wp_get_current_user();
?>
	
<div class="wrap">
	<h2>Bereiche</h2>
	<span [sif:__newlink__:neq:]><h3><a href="__newlink__">Neuen Bereich anlegen...</a></h3></span>

	<table class="widefat" cellpadding=0 cellspacing=0>
	<tr>
		<th>
			Name
		</th>
		<th>
			Kurzbeschreibung
		</th>
		<th>
			Anmeldungen m&ouml;glich
		</th>
	</tr>
		<tbody data-bind="foreach: bereichsliste">
			<tr data-bind="attr{ class: geschlossen() : "inactive" ? "active" }">
				<td><b><span data-bind="html: bezeichnung"></b></td>
				<td><span data-bind="html: preview"</td>
				<td>
					<label>
						<input type="checkbox" data-bind="checked: geschlossen, click: $root.changeLocked"  />
						<span data-bind="visible: geschlossen() == true">Nein</span>
						<span data-bind="visible: geschlossen() == false">Ja</span>
					</label>
				</td>			
			</tr>
		</tbody>
	</table>
</div>
<script type="text/javascript">
var wpajax = "<?php echo get_bloginfo("wpurl"); ?>/wp-admin/admin-ajax.php";
</script>
<script type="text/javascript" src="<?php echo X7URL; ?>/scripts/knockout.js"></script>
<script type="text/javascript" src="<?php echo X7URL; ?>/scripts/bereichsliste.js"></script>