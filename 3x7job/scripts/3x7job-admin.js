/**
 * Created by marcel on 21.04.15.
 */
jQuery(document).ready(function() {
    var getGermanCityForZip = function(){
        var zip = jQuery("[name=plz]").val();
        if (zip){
            // Make HTTP Request
            jQuery.ajax({
                url: "http://api.zippopotam.us/de/" + zip,
                cache: false,
                dataType: "json",
                type: "GET",
                success: function(result, success) {
                    // German Post Code Records Officially Map to only 1 Primary Location
                    places = result['places'][0];
                    jQuery("[name=ort]").val(places['place name']);
                    zip_box.addClass('success').removeClass('error');
                },
                error: function(result, success) {
                    jQuery("[name=ort]").val("PLZ in BRD nicht gefunden");

                }
            });
        }
    }

    jQuery('.widefat').on('mouseenter', '.agv-bemerkung__preview', function (event) {
        jQuery(this).qtip({
            content: {
                text: jQuery(this).find(".agv-bemerkung__content").html(),
                title: "Bemerkungen"
            },
            show: {
                event: event.type, // Use the same event type as above
                ready: true // Show immediately - important!
            },
            position: {
                my: "top center",
                at: "bottom center"
            }
        });
    });

    getGermanCityForZip();
    jQuery("[name=plz]").keyup(getGermanCityForZip);
});

