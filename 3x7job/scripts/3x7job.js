jQuery(document).ready(function(){
    var toggleAbbau = function(isAbbauActivate){
        if (isAbbauActivate){
            jQuery(".agv-form__row--abbau").slideDown();
        } else {
            jQuery(".agv-form__row--abbau").slideUp();
            jQuery("input[name=Abbau]").attr("checked", false);
        }
    };

    jQuery(".agv-label--checkbox").click(function(e){
        var labelFor = jQuery(this).attr("for");
        var label = jQuery("input[name="+ labelFor +"]")

        label.attr("checked", !label.is(":checked"));
        label.trigger("change");
    });

    jQuery("input[name=Aufbau]").change(function(){
        var state = jQuery(this).is(":checked")
        toggleAbbau(state)
    });

    if (jQuery("input[name=Aufbau]").is(":checked") == true)
    {
        jQuery(".agv-form__row--abbau").show();
    }
    else
    {
        jQuery(".agv-form__row--abbau").hide();
    }
});


