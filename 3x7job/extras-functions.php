<?php
function FsmaGetUsers($username = "", $selectedID=-1)
{
	global $table_prefix;

	$option = new x7Template(X7TPL."option.tpl");
	$users  = new jfbremenTemplate(FSMASQL."SucheAlleUser.sql");
	$html   = "";
		
	$users = $users->DoMultipleQuery(true, array("__PREFIX__" => $table_prefix, "__USERNAME__" => $username), ARRAY_A);
	
	for ($i=0; $i<count($users[0]); $i++)
	{
		$user = $users[0][$i];
	
		$params = array();
		$params["__VALUE__"] =  $user["ID"];
		$params["__TEXT__"]  =  $user["display_name"];
		if ($user["ID"] == $selectedID)
			$params["__SELECTED__"]  =  "SELECTED";
		
		$html .= $option->GetFilteredContent($params, true);
	}
	
	return $html;	
}

function FsmaGetUserName4ID($id)
{
	global $wpdb;
	global $table_prefix;
	return $wpdb->get_var("SELECT user_login FROM ".$table_prefix."users WHERE ID = ".$wpdb->escape($id)."");		

}

function FsmaRenameUser($id, $newName)
{
	global $wpdb;
	global $table_prefix;

	if (!empty($id) && !empty($newName))
		$wpdb->query("UPDATE ".$table_prefix."users SET user_login = '".$wpdb->escape($newName)."' WHERE ID = ".$wpdb->escape($id)."");
}
?>