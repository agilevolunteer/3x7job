<?php
/* 
Plugin Name: Template Dev Plugin
Plugin URI: http://www.jesusfreaks-bremen.de/
Description: Provides a simple form to subscribe a Newsletter
Version: 0.1
Author: Jesusfreaks Bremen Webteam
Author URI: http://www.jesusfreaks-bremen.de/

Usage:
Insert the string <!--subscribeNL--> 
in the post or page, where you want to publish the registration.
*/

define('devURL', get_bloginfo("wpurl") . '/wp-content/plugins/subscribeNL/');	
define('devPATH',dirname(__FILE__).'/');
define('devSQL',devPATH."/sql/");
define('devTPL',devPATH."/tpl/");
define("devDEBUG",true);



function devAdminMenu()
{
	add_submenu_page('plugins.php', 'T1', 'T1', 10, dirname(__FILE__)."/admin/t1.php");
	add_submenu_page('plugins.php', 'T2', 'T2', 10, dirname(__FILE__)."/admin/t2.php");		
}


// Hook for adding admin menus
add_action('admin_menu', 'devAdminMenu');

?>
