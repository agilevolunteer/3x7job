<?php
/*
Plugin Name: jfbremenTemplate
Plugin URI: http://www.jesusfreaks-bremen.de/
Description: Stellt Funktionen f&uuml;r das Trennen von Funktionalit&auml;t und Oberfl&auml;che zur Verf&uuml;gung
Version: 0.2
Author: Jesusfreaks Bremen Webteam
Author URI: http://www.jesusfreaks-bremen.de/
*/

class jfbremenTemplate
{
    var $unfilteredContent;
	var $showErrors;
	var $errors;

    function jfbremenTemplate($path)
    {
        $this->errors = array();
        if(is_readable($path))
		      {
			$text = "";
			$formTpl = file($path);
			//Template einlesen
			foreach ($formTpl as $line)
			{
				$text .= $line;
			}

			$this->unfilteredContent = $text;
		}
		else
		{
			$this->unfilteredContent = "Die Datei <i>$path</i> konnte nicht geladen werden!";
    array_push($this->$errors, "Template nicht lesbar: "+$path);
		}
		$this->showErrors = true;

    }

	function ReplaceUnusedParams($text)
	{
		//$replacement = "__(\w+)__/";
		$text = preg_replace('/__(\\w+)__/','',$text);
		return $text;
	}

    function GetFilteredContent($params = array(), $replaceUnusedParams = false, $text = "")
    {

		$text = (empty($text)) ? $this->unfilteredContent : $text;
		$text = $this->ValidateUrlVisibility($text);
		$text = $this->ValidateVisibility($text, $params);


		$keys = array_keys($params);

		//Platzhalter ersetzen
		foreach ($keys as $key)
		{
		   $text = str_replace($key,$params[$key],$text);
		}

		if ($replaceUnusedParams == true)
		{
			$text = $this->ReplaceUnusedParams($text);
		}
		return $text;
    }

    function DoMultipleQuery( $exec = false, $params = array(), $mode = OBJECT, $replaceUnusedParams = false)
    {
    	global $wpdb;

		//$AllQueries = $this->unfilteredContent;
		$QueryArray = explode(';',$this->unfilteredContent);
		$results = array();

		//$this->printDebugArray($QueryArray);

		if ($this->showErrors == true)
		{
			$wpdb->show_errors();
		}
		else
		{
			$wpdb->hide_errors();
		}

		//foreach ($QueryArray as $SingleQuery)
		for ($i=0;$i<count($QueryArray);$i++)
		{
//			$SingleQuery = $this->GetFilteredContent($params, true, $SingleQuery);
			$SingleQuery = $QueryArray[$i];
		    if ((strtoupper(substr($SingleQuery,1,2)) == "GO") || empty($SingleQuery))
		    {
		    	break;
		    }
			else
			{
				$SingleQuery = $this->GetFilteredContent($params, true, $QueryArray[$i]);
			}

		    if ($exec == true)
		    {
		    	//Wirklich ausf?hren
		    	$preResult = $wpdb->query($SingleQuery);

		    	if ($preResult === FALSE)
		    	{
		    		array_push($this->errors, $wpdb->last_error);
		    	}
		    	elseif ((strtoupper(substr(trim($SingleQuery),0,6)) == "SELECT") || (strtoupper(substr(trim($SingleQuery),0,4)) == "SHOW"))
				{
			    	$SingleResult = $wpdb->get_results($SingleQuery, $mode);
				}
				else
				{
					$SingleResult = $preResult;
				}

		    	array_push($results, $SingleResult);
		    }
		    else
		    {
				array_push($results, $SingleQuery);
		    }
		}

		return $results;
    }

    function Succeeded()
    {
    	return (count($this->errors) == 0);

    }

    function GetErrorText()
    {
    	$text = "";

    	for ($i = 0; $i < count($this->errors); $i++)
    	{
    		$text .= $this->errors[$i];
    		$text .= "<br>";
    	}

    	return $text;

    }

	function DecideIfVisible($body, $params)
	{
		$SifContent = explode(":",$body);
		$MakeVisible = true;

		if (array_key_exists ( $SifContent[0], $params ))
		{
			switch ( $SifContent[1] )
			{
				case "neq": $MakeVisible = ($params[$SifContent[0]] != $SifContent[2]);
					break;
				default: $MakeVisible = ($params[$SifContent[0]] == $SifContent[2]);
					break;
			}
		}
		else
		{
			//return " style=\"/*Key ".$SifContent[0] ." doesn't exist*/\" ";
			$MakeVisible = false;
			switch ( $SifContent[1] )
			{
				case "neq": $MakeVisible = true;
					break;
				default: $MakeVisible = false;
					break;
			}
		}

		if ($MakeVisible == false)
			return " style=\"display: none;\" ";
		else
			return "";

	}

	function ValidateVisibility($content, $params)
	{
		$VisCommand = "[sif:";
		$SplitContent = explode($VisCommand,  $content);

		for ($i = 0; $i < count($SplitContent); $i++)
		{

			if (strpos($SplitContent[$i],"]") > 0)
			{
				$SifBody = substr($SplitContent[$i],0,strpos($SplitContent[$i],"]"));
				$content = str_replace($VisCommand . $SifBody."]", $this->DecideIfVisible($SifBody,$params),$content);
			}
		}

		return $content;
	}

	function ValidateUrlVisibility($content)
	{

		$SplittedQueryString = explode("&",$_SERVER["QUERY_STRING"]);

		$params = array();
		for ($i=0;$i<count($SplittedQueryString);$i++)
		{
			$ParamPair = explode("=",$SplittedQueryString[$i]);
			$params[$ParamPair[0]] = $ParamPair[1];
		}
		//jfPrintDebugArray($params);

		$VisCommand = "[sifui:";
		$SplitContent = explode($VisCommand,  $content);

		for ($i = 0; $i < count($SplitContent); $i++)
		{

			if (strpos($SplitContent[$i],"]") > 0)
			{
				$SifBody = substr($SplitContent[$i],0,strpos($SplitContent[$i],"]"));
				$content = str_replace($VisCommand . $SifBody."]", $this->DecideIfVisible($SifBody,$params),$content);
			}
		}

		return $content;
	}
}

class jfbremenValidate
{
  function enc8($text) {

    $text = preg_replace ('/([x80-xff])/se', "pack (\"C*\", (ord ($1) >> 6) | 0xc0, (ord ($1) & 0x3f) | 0x80)", $text);
    $text = addslashes($text);

    return $text;
  }

	function isMail($email)
	{
		if(empty($email))
		{
			return false;
		}
		else
		{
			if(!preg_match("/^[0-9a-zA-Z]([-_.]?[0-9a-zA-Z])*@[0-9a-zA-Z]([-.]?[0-9a-zA-Z])*\.[a-zA-Z]{2,}$/i", $email))
			{
				return false;
			}
		}
		return true;
	}

/***********************************************************************
* function isDate
* boolean isDate(string)
* Summary: checks if a date is formatted correctly: yyyy-mm-dd (european)
* Author: Laurence Veale
* Date: 30/07/2001
***********************************************************************/
	function isDate($i_sDate)
	{
	  $blnValid = TRUE;
	   // check the format first (may not be necessary as we use checkdate() below)
	   if(!ereg ("^[0-9]{4}-[0-9]{2}-[0-9]{2}$", $i_sDate))
	   {
	    $blnValid = FALSE;
	   }
	   else //format is okay, check that days, months, years are okay
	   {
	      $arrDate   = explode("-", $i_sDate); // break up date by slash
	      $intDay    = $arrDate[2];
	      $intMonth  = $arrDate[1];
	      $intYear   = $arrDate[0];
	      $intIsDate = checkdate($intMonth, $intDay, $intYear);
	     if(!$intIsDate)
	     {
	        $blnValid = FALSE;
	     }
	   }//end else
	   return ($blnValid);
	} //end function isDate

}

function jfPrintDebugArray($array)
{
	echo "<pre>";
	print_r($array);
	echo "</pre>";
}

/***********************************************************************
* function jfEnc8
* string jfEnc8(string)
* Summary: encodes a string as UTF8
* Author: igg (from coding-net.de)
* Date: 07/11/2005
***********************************************************************/
function jfEnc8($text) {

    $text = preg_replace ('/([x80-xff])/se', "pack (\"C*\", (ord ($1) >> 6) | 0xc0, (ord ($1) & 0x3f) | 0x80)", $text);
    $text = addslashes($text);

    return $text;
}


function jfGet($var, $type = 'INT')
{
	$type = strtoupper($type);
	$variable = $_GET[$var];
	switch ($type)
	{
		case 'INT':
			$value = intval($variable);
			return $value;
		default:
			return $_GET[$var];

	}

	return $_GET($var);
}


function jfPost($var, $type = 'INT')
{
	$type = strtoupper($type);
	$variable = $_POST[$var];
	switch ($type)
	{
		case 'INT':
			$value = intval($variable);
			return $value;
		default:
			return $_POST[$var];

	}

	return $_POST($var);
}
?>
