<?php
$CurrentUser = wp_get_current_user();

if (isset($_POST["BID"]))
{
	$BID = $_POST["BID"];
	$Message       = new jfbremenTemplate(FSMATPL."BackendMessage.tpl");
	$MessageParams = array();
	$MessageParams["__TEXT__"] = "Bereich <i>".$_POST["bezeichnung"]."</i> wurde gespeichert";
		
	$SqlParams                       = array();
	$SqlParams["__prefix__"]         = $table_prefix;
	$SqlParams["__BID__"]            = $_POST["BID"];
	$SqlParams["__bezeichnung__"]    = $_POST["bezeichnung"];
	$SqlParams["__kurztext__"]       = $_POST["kurztext"];
	$SqlParams["__langtext__"]       = $_POST["langtext"];
	$SqlParams["__FSK__"]            = $_POST["FSK"];
	$SqlParams["__Arbeitszeit__"]    = $_POST["Arbeitszeit"];
	$SqlParams["__kommentar__"]      = $_POST["kommentar"];
	$SqlParams["__bereichsleiter__"] = (isset($_POST["bereichsleiter"])) ? $_POST["bereichsleiter"] : $_POST["oldBereichsleiter"] ;
	$SqlParams["__CustomFields__"]   = $_POST["CustomFields"];
	//b'001'
	$SqlParams["__locked__"]         = (isset($_POST["locked"])) ? "b'001'" : "b'000'";
	$SqlParams["__VisInList__"]      = (isset($_POST["VisInList"])) ? "b'001'" : "b'000'";
		
	if ($BID == -1)
	{
		$SaveBereich = new jfbremenTemplate(FSMASQL."InsertNewBereich.sql");
	}
	else 
	{
		$SaveBereich = new jfbremenTemplate(FSMASQL."UpdateBereich.sql");
	}
		
	$result = $SaveBereich->DoMultipleQuery(true, $SqlParams, ARRAY_A, true);
	//echo $SaveBereich->GetFilteredContent($SqlParams, true);
	echo $Message->GetFilteredContent($MessageParams, true);
}


//spezifische Anzeige
if (!isset($_GET["BID"]))
{
	//ListView
	$Header       = new jfbremenTemplate(FSMATPL."BackendBereicheHeader.tpl");
	$ListItem     = new jfbremenTemplate(FSMATPL."BackendBereicheListItem.tpl");
	$ListItemAlt  = new jfbremenTemplate(FSMATPL."BackendBereicheListItemAlt.tpl");
	$Footer       = new jfbremenTemplate(FSMATPL."BackendBereicheFooter.tpl");
	$AlleBereiche = new jfbremenTemplate(FSMASQL."AdminAlleBereiche.sql");

	$SurParams                = array();
		
	$SqlParams = array();
	$SqlParams["__prefix__"]  = $table_prefix;
	$SqlParams["__LIMIT__"]   = "";

	//Nur die Bereiche  anzeigen, die gesehen werden d�fen, NeuLink, nur wenn man  alle Bereiche  administrieren darf
	if (current_user_can('edit_all_bereiche'))
	{
		$SqlParams["__WHERE__"]   = "1=1";
		$SurParams["__newlink__"] = FsmaAddUrlParam("BID",-1);
	}
	else
	{
		$SqlParams["__WHERE__"]   = "b.bereichsleiter = ".$CurrentUser->ID;
		$SurParams["__newlink__"] = "";
	}
	
	$result = $AlleBereiche->DoMultipleQuery(true, $SqlParams, ARRAY_A);
	echo $Header->GetFilteredContent($SurParams, true);
	$alt = false;
	
	for ($i=0;$i<count($result[0]);$i++)
	{			
		$Bereich = $result[0][$i];
		$params = array();
		$params["__Bezeichnung__"]     =  $Bereich["Bezeichnung"];
		$params["__BID__"]             =  $Bereich["BID"];
		$params["__Preview__"]         =  $Bereich["Preview"];
		$params["__morelink__"]        =  FsmaAddUrlParam("BID",$Bereich["BID"]) ;
		$params["__Arbeitszeit__"]     =  $Bereich["Arbeitszeit"];
		$params["__Bereichsleiter__"]  =  $Bereich["Leiter"];	
		
		if (!$alt)
		{
			echo $ListItem->GetFilteredContent($params);
		}
		else
		{
			echo $ListItemAlt->GetFilteredContent($params);
		}
		
		$alt = !$alt;
	}	

	echo $Footer->GetFilteredContent($SurParams, true);
}
else
{
	//DetailView
	$Bereich    = new jfbremenTemplate(FSMASQL."AdminSingleBereich.sql");
	$BID        = $_GET["BID"];
	$result = $Bereich->DoMultipleQuery(true, array("__prefix__" => $table_prefix, "__BID__" => $BID), ARRAY_A);

	$Bereich = $result[0][0];
	
	if(((current_user_can('edit_own_bereich')) && ($Bereich["bereichsleiter"] == $CurrentUser->ID)) || (current_user_can('edit_all_bereiche')))
	{	
		$DetailView = new jfbremenTemplate(FSMATPL."BackendBereicheDetailView.tpl");
		$params = array();
		$params["__BID__"]                    =  $BID;
		$params["__ACTION__"]		          =  FsmaEraseUrlParam("BID",FsmaAddUrlParam());//FsmaEraseUrlParam
		$params["__BACKURL__"]                =  FsmaEraseUrlParam("BID",FsmaAddUrlParam());
		
		if ($BID == -1)
		{
			$params["__BereichsleiterOptions__"]  =  FsmaGetUsers4DropDown();
			$params["__Bezeichnung__"]            = "&lt;Neuer Bereich&gt;"; 
		}		
		$params["__Bezeichnung__"]            =  $Bereich["Bezeichnung"];
		$params["__BereichsleiterOptions__"]  =  (current_user_can('set_bereichsleiter')) ? FsmaGetUsers4DropDown($Bereich["bereichsleiter"]) : FsmaOnlyMe4DropDown($CurrentUser);
		$params["__Bereichsleiter__"]         =  $Bereich["bereichsleiter"];
		$params["__Description__"]            =  $Bereich["Description"];
		$params["__Preview__"]                =  $Bereich["Preview"];
		$params["__Arbeitszeit__"]            =  $Bereich["Arbeitszeit"];
		$params["__FSK__"]                    =  $Bereich["FSK"];
		$params["__kommentar__"]              =  $Bereich["kommentar"];
		$params["__LOCKED__"]                 =  $Bereich["locked"];
		$params["__VisInList__"]              =  $Bereich["VisInList"];
		$params["__CustomFields__"]           =  $Bereich["CustomFields"];
		$params["__BereichsleiterEnabled__"]  =  (current_user_can('set_bereichsleiter')) ? "" : "disabled";

		echo $DetailView->GetFilteredContent($params, true);
	}
	else
	{
		$Fehler       = new jfbremenTemplate(FSMATPL."BackendError.tpl");
		$FehlerParams = array();
		$FehlerParams["__TEXT__"] = "Du hast nicht die Berechtigung diesen Bereich zu sehen!";
		echo $Fehler->GetFilteredContent($FehlerParams);
	}
}
?>
