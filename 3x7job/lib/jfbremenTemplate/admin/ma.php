<?php
function FsmaFormatGroupExp($val)
{
	$val = explode("-", $val);
	return $val[0];
}

function FsmaSwitchOrderExp($key, $val)
{
	switch ($key)
	{
		case "Bereich":
		case "AltBereich": return (current_user_can('read_all_ma')) ? $val : "";
			break;
		case "Name": return strtoupper(substr($val, 0, 1));
			break;
		case "Eingecheckt":
		case "Bezahlt": return ($val == 1) ? "Ja" : "Nein";
	}
}

$CurrentUser  = wp_get_current_user();
$BID          = FsmaGetBereichByLeiter($CurrentUser->ID, "BID");
$UID          = $_GET["UID"];
$orderExp     = (isset($_GET["sort"])) ? FsmaFormatGroupExp($_GET["sort"]) : "Bereich";
$Jahr         = $_GET["jahr"];
$cleanSortUrl = FsmaDeleteUrlParams(array("sort", "dir"));
global $wpdb;
global $table_prefix;

//Sachen bearbeiten
if ((isset($_POST["ThisPage"])) && ($_POST["ThisPage"] == "MA"))
{
	update_usermeta($UID,'gebdat',          $_POST['gebdat']);
	update_usermeta($UID,'Handy',           $_POST['Handy']);
	update_usermeta($UID,'VorwahlHandy',    $_POST['VorwahlHandy']); 
	update_usermeta($UID,'Tele',            $_POST['Tele']);
	update_usermeta($UID,'VorwahlTele',     $_POST['VorwahlTele']);
	update_usermeta($UID,'ort',             $_POST['ort']);
	update_usermeta($UID,'plz',             $_POST['plz']);
	update_usermeta($UID,'strassehausnr',   $_POST['strassehausnr']);
	update_usermeta($UID,'last_name',       $_POST['nachname']);
	update_usermeta($UID,'first_name',      $_POST['vorname']);
	$displayname = $wpdb->escape($_POST['nachname'].", ".$_POST['vorname']);
	$UpdateDisplayName = "UPDATE ".$wpdb->users." SET display_name='$displayname' WHERE ID=$UID";
	$wpdb->query($UpdateDisplayName);

	FsmaMessage("Der Mitarbeiter <i><b>$displayname</b></i> wurde gespeichert.");	
}
elseif ((isset($_POST["ThisPage"])) && ($_POST["ThisPage"] == "Anmeldung"))
{
	if (isset($_POST["abwBereich"]))
	{
		
		FsmaChangePoolStatus($_POST["PrPool"], get_option("FsmaDismissedStatus"));
		FsmaMessage("Der Mitarbeiter wurde in seinem Prim&auml;rbereich abgewiesen.");
	}
	elseif (isset($_POST["abwAltBereich"]))
	{
		FsmaChangePoolStatus($_POST["AltPool"], get_option("FsmaDismissedStatus"));
		FsmaMessage("Der Mitarbeiter wurde in seinem Alternativbereich abgewiesen.");
	}
	elseif (isset($_POST["save"]))
	{
	
		//Anmeldedaten
		//jfPrintDebugArray($_POST);
		//$Anmeldedaten = $_POST;
		$Anmeldedaten["__prefix__"] = $table_prefix;
		$Anmeldedaten["__UserID__"] = $UID;
		//$Anmeldedaten["__STATUS__"] = get_option("FsmaDefStatus");
		$Anmeldedaten["__Jahr__"]   = (empty($_POST["Jahr"])) ? get_option('FsmaJahr') : $_POST["Jahr"];
		$Anmeldedaten["__AufbauAbbau__"]   = (isset($_POST["AufbauAbbau"])) ? "b'001'" : "b'000'";
		$Anmeldedaten["__Mitgearbeitet__"] = (isset($_POST["Mitgearbeitet"])) ? "b'001'" : "b'000'";
		$Anmeldedaten["__Eingecheckt__"]   = (isset($_POST["Eingecheckt"])) ? "b'001'" : "b'000'";
		$Anmeldedaten["__Bezahlt__"]       = (isset($_POST["Bezahlt"])) ? "b'001'" : "b'000'";
		$Anmeldedaten["__FsErfahrungen__"] = $_POST["FsErfahrungen"];
		$Anmeldedaten["__Bemerkungen__"]   = $_POST["Bemerkungen"];
		$Anmeldedaten["__AnmeldeID__"]     = $_POST["AnmeldeID"];
		//jfPrintDebugArray($Anmeldedaten);
		
		if (!empty($_POST["AnmeldeID"]))
		{
			//echo "update";
			$AnmeldeQuery = new jfbremenTemplate(FSMASQL."BackendUpdateAnmeldung.sql");		
			FsmaChangePool($_POST["PrPool"], $_POST["Bereich"], $_POST["BereichStatus"]);
			FsmaChangePool($_POST["AltPool"], $_POST["AltBereich"], $_POST["AltBereichStatus"]);
		
			if (!empty($_POST["DismissedPool"]))
				FsmaChangePool($_POST["DismissedPool"], $_POST["DismissedBereich"], get_option("FsmaDefStatus"));
		}
		else
		{
			$AnmeldeQuery = new jfbremenTemplate(FSMASQL."InsertAnmeldung.sql");
			
		}
		
		//~ //Emails versenden, wenn Sie in einem Bereich angenommen wurden
		//~ FsmaSendNotificationMail($_POST["PrPool"], $_POST["Bereich"], $_POST["BereichStatus"]);
		//~ FsmaSendNotificationMail($_POST["AltPool"], $_POST["AltBereich"], $_POST["AltBereichStatus"]);
		//~ FsmaSendNotificationMail($_POST["DismissedPool"], $_POST["DismissedBereich"], get_option("FsmaDefStatus"));
		
		//echo $AnmeldeQuery->GetFilteredContent($Anmeldedaten, false);
		$AnmeldeQuery->DoMultipleQuery(true, $Anmeldedaten);
	
		FsmaMessage("Anmeldung wurde gespeichert!");
	}
	
	if ((FsmaIsAllDismissed($UID, $_POST["Jahr"])==true) && (empty($_POST["DismissedPool"])))
	{		
		FsmaSendToDismissedPool($UID, $_POST["Jahr"]);
		FsmaMessage("Mitarbeiter wurde dem Schwimmbereich zugewiesen");
	}
	

}
elseif ((isset($_POST["ThisPage"])) && ($_POST["ThisPage"] == "CustomFields"))
{
	$keys = array_keys($_POST);
	
	//Platzhalter ersetzen
	foreach ($keys as $key)
	{
		if ( $key != "ThisPage")
			update_usermeta($UID, $key, $_POST[$key]);
	}
	
	FsmaMessage("Zusatzinformationen wurden gespeichert");
}
elseif ((isset($_POST["ThisPage"])) && ($_POST["ThisPage"] == "Filter"))
{
	$FilterBID    = $_POST["bereich"];
	$FilterAltBID = $_POST["altbereich"];
	$Jahr         = (empty($_POST["jahr"])) ? get_option('FsmaJahr') : $_POST["jahr"];
	
	$where = ($FilterBID == -1) ? "" : " (bpr.BID = $FilterBID) AND ";
	$where .= ($FilterAltBID == -1) ? "" : " (balt.BID = $FilterAltBID) AND ";
	$where .= (empty($_POST["display_name"])) ? "" : ("ma.display_name like '%".$_POST["display_name"]."%' AND ");
	
	$_SESSION["where"]  = $where;
}

$where = $_SESSION["where"];

if ((isset($_GET["chg"])) && ($_GET["chg"] == "check"))
{
	$value = $_GET["val"];
	$aid   = $_GET["aid"];
	
	$query = "UPDATE ".$table_prefix."anmeldung set Eingecheckt = b'".$wpdb->escape($value)."' WHERE id = ".$wpdb->escape($aid);
	$wpdb->query($query);
}
if ((isset($_GET["chg"])) && ($_GET["chg"] == "zahl"))
{
	$value = $_GET["val"];
	$aid   = $_GET["aid"];
	
	$query = "UPDATE ".$table_prefix."anmeldung set Bezahlt = b'".$wpdb->escape($value)."' WHERE id = ".$wpdb->escape($aid);
	$wpdb->query($query);
}


//Suche
$SuchTemplate = new jfbremenTemplate(FSMATPL."BackendMaFilter.tpl");
$SuchParams   = array();
$SuchParams["__BereichsOptions__"]    = FsmaGetBereiche4DropDown($FilterBID);
$SuchParams["__AltBereichsOptions__"] = FsmaGetBereiche4DropDown($FilterAltBID);
$SuchParams["__JahrOptions__"]        = FsmaGetJahr4DropDown($Jahr);
$SuchParams["__ACTION__"]             = FsmaAddUrlParam();
$SuchParams["__DISPLAYNAME__"]        = $_POST["display_name"];
$SuchParams["__inDetailView__"]       = isset($_GET["UID"]);

echo $SuchTemplate->GetFilteredContent($SuchParams);

//spezifische Anzeige
if (!isset($_GET["UID"]))
{
	//ListView
	
	?>
	
	<div class="wrap">
	<h2>Mitarbeiter</h2>
	<?php
	//ListView
	$Header       = new jfbremenTemplate(FSMATPL."BackendMAHeader.tpl");
	$ListItem     = new jfbremenTemplate(FSMATPL."BackendMAListItem.tpl");
	$ListItemAlt  = new jfbremenTemplate(FSMATPL."BackendMAListItemAlt.tpl");
	$Footer       = new jfbremenTemplate(FSMATPL."BackendMAFooter.tpl");
	$AlleMA       = new jfbremenTemplate(FSMASQL."AdminAlleMA.sql");

	$SurParams                     = array();
	$SurParams["__CleanSortUrl__"] = $cleanSortUrl;
	$SurParams["__FSMAURL__"]      =  FSMAURL;
		
	$SqlParams = array();
	$SqlParams["__prefix__"]  = $table_prefix;
	$SqlParams["__LIMIT__"]   = "";
	$SqlParams["__ORDER__"]   = (isset($_GET["sort"])) ? str_replace("-", " ",$_GET["sort"]) :"Bereich, AltBereich, Name";
	$SqlParams["__JAHR__"]    = (empty($Jahr)) ? get_option('FsmaJahr') : $Jahr;
	$DismissedStatus          = get_option("FsmaDismissedStatus");
	$NewStatus                = get_option("FsmaDefStatus");
	$AcceptStatus             = get_option("FsmaAngenommenStatus");

	//Nur die MA  anzeigen, die gesehen werden d�fen, NeuLink, nur wenn man  alle MA  administrieren darf
	if (current_user_can('read_all_ma'))
	{
		$SqlParams["__WHERE__"]   = "$where 1=1";
		$SurParams["__newlink__"] = "";//FsmaAddUrlParam("UID",-1);
	}
	else
	{
		$SqlParams["__WHERE__"]   = "$where ((bpr.BID = $BID AND mpr.status_id <> $DismissedStatus) or (balt.BID = $BID AND malt.status_id <> $DismissedStatus))";
		$SurParams["__newlink__"] = "";
	}
	
	
	$result = $AlleMA->DoMultipleQuery(true, $SqlParams, ARRAY_A);
	
	echo $Header->GetFilteredContent($SurParams, true);
	$alt = false;
	$oldOrder = "";
	
	for ($i=0;$i<count($result[0]);$i++)
	{			
		$SingleMA = $result[0][$i];
		$params = array();
		$params["__FSMAURL__"]        =  FSMAURL;
		$params["__morelink__"]       =  FsmaAddUrlParam("UID",$SingleMA["id"]) ;
		$params["__morelink__"]       =  (empty($Jahr)) ? $params["__morelink__"] : FsmaAddUrlParam("jahr",$Jahr, $params["__morelink__"]) ;
		$params["__Name__"]           =  $SingleMA["Name"];
		$params["__Bereich__"]        =  $SingleMA["Bereich"];
		$params["__AltBereich__"]     =  $SingleMA["AltBereich"];
		$params["__Eingecheckt__"]    =  $SingleMA["Eingecheckt"];
		$params["__Bezahlt__"]        =  $SingleMA["Bezahlt"];
		
		//F�r die StatusIcons
		$params["__Dismissed__"]      =  ($SingleMA["Status"]    == $DismissedStatus);
		$params["__AltDismissed__"]   =  ($SingleMA["AltStatus"] == $DismissedStatus);
		
		$params["__New__"]            =  ($SingleMA["Status"]    == $NewStatus);
		$params["__AltNew__"]         =  ($SingleMA["AltStatus"] == $NewStatus);	
		
		$params["__Accept__"]         =  ($SingleMA["Status"]    == $AcceptStatus);
		$params["__AltAccept__"]      =  ($SingleMA["AltStatus"] == $AcceptStatus);
		
		
		$params["__Changed__"]        = ($SingleMA["aid"] == $_GET["aid"]) ? "change" : "";
		
		//Link zum Bearbeiten des Eingechecktstatus
		$params["__EditEingechecktUrl__"] = FsmaAddUrlParam("chg", "check");
		$params["__EditEingechecktUrl__"] = FsmaAddUrlParam("aid", $SingleMA["aid"], $params["__EditEingechecktUrl__"]);
		$params["__EditEingechecktUrl__"] = ($SingleMA["Eingecheckt"]) ? FsmaAddUrlParam("val", 0, $params["__EditEingechecktUrl__"]) : FsmaAddUrlParam("val", 1, $params["__EditEingechecktUrl__"]);
		
		//Link zum Bearbeiten des Bezahlstatus
		$params["__EditBezahltUrl__"] = FsmaAddUrlParam("chg", "zahl");
		$params["__EditBezahltUrl__"] = FsmaAddUrlParam("aid", $SingleMA["aid"], $params["__EditBezahltUrl__"]);
		$params["__EditBezahltUrl__"] = ($SingleMA["Bezahlt"]) ? FsmaAddUrlParam("val", 0, $params["__EditBezahltUrl__"]) : FsmaAddUrlParam("val", 1, $params["__EditBezahltUrl__"]);
		
		$ThisOrderExp = FsmaSwitchOrderExp($orderExp, $SingleMA[$orderExp]);
		
		if ($ThisOrderExp  == $oldOrder)
		{
			$params["__ORDEREXP__"]       =  "";
		}
		else
		{
			$params["__ORDEREXP__"]       =  $ThisOrderExp ;
			$alt = false;
		}
		
		$oldOrder                     =  $ThisOrderExp ;
		
		if (!$alt)
		{
			echo $ListItem->GetFilteredContent($params);
		}
		else
		{
			echo $ListItemAlt->GetFilteredContent($params);
		}
		
		$alt = !$alt;
	}	

	echo $Footer->GetFilteredContent($SurParams, true);
	?>
	</div>
	<?php
}
else
{
	//Anmeldedaten
	$Anmeldung = new jfbremenTemplate(FSMASQL."MitarbeiterAnmeldung.sql");
	$AnmeldungParams = array();
	$AnmeldungParams["__Mitarbeiter__"] = $UID;
	$AnmeldungParams["__Jahr__"]        = (empty($Jahr)) ? get_option('FsmaJahr') : $Jahr;
	$AnmeldungParams["__prefix__"]      = $table_prefix;
	$AnmeldeData = $Anmeldung->DoMultipleQuery(true, $AnmeldungParams);
	$Data = $AnmeldeData[0][0];
	$DismissedPoolArray = FsmaGetDismissedBereich4DropDown($UID, $AnmeldungParams["__Jahr__"], $Data->Bereich, true);
	$DismissedPool = $DismissedPoolArray[0];

	if (((current_user_can('read_own_ma')) && (($Data->Bereich == $BID) || ($Data->AltBereich == $BID)|| ($DismissedPool->BID_Bereiche == $BID))) || (current_user_can('read_all_ma')))
	{	
		//Persönliche Daten
		$PersDataTemplate  = new jfbremenTemplate(FSMATPL."BackendMitarbeiterPersData.tpl");
		$AnmeldungTemplate = new jfbremenTemplate(FSMATPL."BackendMitarbeiterAnmeldung.tpl");
		$CustomFieldsTemplate    = new jfbremenTemplate(FSMATPL."BackendMitarbeiterCustomFields.tpl");
		$PersDataParams    = array();
		$AnmeldeParams   = array();
		$CustomFieldParams = array();
		$MA = get_userdata($UID);
		//echo $MA->user_email;
		//$Anmeldung->printDebugArray($MA);
		$PersDataParams["__email__"]       = $MA->user_email;
		
		if ($fehler==true)
		{
			$PersDataParams["__gebdat__"]        = $_POST["gebdat"];
			$PersDataParams["__Handy__"]         = $_POST["Handy"];
			$PersDataParams["__VorwahlHandy__"]  = $_POST["VorwahlHandy"];
			$PersDataParams["__Tele__"]          = $_POST["Tele"];
			$PersDataParams["__VorwahlTele__"]   = $_POST["VorwahlTele"];
			$PersDataParams["__ort__"]           = $_POST["ort"];
			$PersDataParams["__plz__"]           = $_POST["plz"];
			$PersDataParams["__strassehausnr__"] = $_POST["strassehausnr"];
			$PersDataParams["__nachname__"]      = $_POST["nachname"];
			$PersDataParams["__vorname__"]       = $_POST["vorname"];			
		}	
		else
		{
			$PersDataParams["__gebdat__"]        = $MA->gebdat;
			$PersDataParams["__Handy__"]         = $MA->Handy;
			$PersDataParams["__VorwahlHandy__"]  = $MA->VorwahlHandy;
			$PersDataParams["__Tele__"]          = $MA->Tele;
			$PersDataParams["__VorwahlTele__"]   = $MA->VorwahlTele;
			$PersDataParams["__ort__"]           = $MA->ort;
			$PersDataParams["__plz__"]           = $MA->plz;
			$PersDataParams["__strassehausnr__"] = $MA->strassehausnr;
			$PersDataParams["__nachname__"]      = $MA->last_name;
			$PersDataParams["__vorname__"]       = $MA->first_name;
		}
		


		//$Anmeldung->printDebugArray($Data);
		//Angaben evtl. aus DB lesen

		//jfPrintDebugArray($DismissedPoolArray);
		
		$AnmeldeParams["__AnmeldeID__"]    = $Data->id;
		$AnmeldeParams["__BereichOptions__"]    = FsmaGetBereiche4DropDown($Data->Bereich);
		$AnmeldeParams["__AltBereichOptions__"] = FsmaGetBereiche4DropDown($Data->AltBereich);
		$AnmeldeParams["__FsErfahrungen__"]     = $Data->FSErfahrung;
		$AnmeldeParams["__Mitgearbeitet__"]     = ($Data->BereitsMitgearbeitet==true) ? "checked" : "";
		$AnmeldeParams["__AufbauAbbau__"]       = ($Data->AufAbbau==true) ? "checked" : "";
		$AnmeldeParams["__Eingecheckt__"]       = ($Data->Eingecheckt==true) ? "checked" : "";
		$AnmeldeParams["__Bezahlt__"]           = ($Data->Bezahlt==true) ? "checked" : "";
		$AnmeldeParams["__Bemerkungen__"]       = $Data->bemerkungen;
		
		
		$AnmeldeParams["__Jahr__"] 		      = $AnmeldungParams["__Jahr__"];
		$AnmeldeParams["__BereichStatusOptions__"]    = FsmaGetStatus4DropDown($Data->PoolStatus);
		$AnmeldeParams["__AltBereichStatusOptions__"] = FsmaGetStatus4DropDown($Data->AltPoolStatus);
		$AnmeldeParams["__DismissedBereichOptions__"] = $DismissedPoolArray[1];
		if (current_user_can("edit_all_ma"))
		{
			$AnmeldeParams["__PrPool__"]            = $Data->PrPool;
			$AnmeldeParams["__AltPool__"]           = $Data->AltPool;
			$AnmeldeParams["__DismissedPool__"]     = $DismissedPool->PoolID;
		}
		else
		{
			//$AnmeldeParams["__PrPool__"]            = $Data->PrPool;
			//$AnmeldeParams["__AltPool__"]           = $Data->AltPool;
			//$AnmeldeParams["__DismissedPool__"]     = $DismissedPool->PoolID;
			$AnmeldeParams["__PrPool__"]            = (current_user_can('edit_own_ma') && $Data->Bereich == $BID)               ? $Data->PrPool : "" ;
			$AnmeldeParams["__AltPool__"]           = (current_user_can('edit_own_ma') && $Data->AltBereich == $BID)            ? $Data->AltPool : "" ;
			$AnmeldeParams["__DismissedPool__"]     = (current_user_can('edit_own_ma') && $DismissedPool->BID_Bereiche == $BID) ? $Data->DismissedPool : "" ;
			
			$AnmeldeParams["__EnablePrPool__"]            = (current_user_can('edit_own_ma') && $Data->Bereich == $BID)               ? "" : "disabled";
			$AnmeldeParams["__EnableAltPool__"]           = (current_user_can('edit_own_ma') && $Data->AltBereich == $BID)            ? "" : "disabled";
			$AnmeldeParams["__EnableDismissedPool__"]     = (current_user_can('edit_own_ma') && $DismissedPool->BID_Bereiche == $BID) ? "" : "disabled";
		}
		
		
		//CustomFields
		$CustomItems = FsmaParseCustomFieldsBackend($BID, -1, $UID);
		$FieldsOutput = "";
						
		if (!empty($CustomItems))
		{
			$CustomFieldParams = array("__ACTION__" => FsmaAddUrlParam());
			$CustomFieldParams["__CustomFieldItems__"] = $CustomItems;
			$FieldsOutput = $CustomFieldsTemplate->GetFilteredContent($CustomFieldParams, true);
			//break;
		}
		
		//Ausgabe  
		echo $PersDataTemplate->GetFilteredContent($PersDataParams, true);
		echo $FieldsOutput;
		echo $AnmeldungTemplate->GetFilteredContent($AnmeldeParams, true);
	}


	else
	{
		$Fehler       = new jfbremenTemplate(FSMATPL."BackendError.tpl");
		$FehlerParams = array();
		$FehlerParams["__TEXT__"] = "Du hast nicht die Berechtigung diesen Mitarbeiter zu sehen!";
		echo $Fehler->GetFilteredContent($FehlerParams);
	}	


/*	{

		$Anmeldungw = new jfbremenTemplate(FSMATPL."BackendAnmeldungDetailView.tpl");
		$params = array();
		$params["__BID__"]                    =  $BID;
		$params["__ACTION__"]		          =  FsmaEraseUrlParam("BID",FsmaAddUrlParam());//FsmaEraseUrlParam
		$params["__BACKURL__"]                =  FsmaEraseUrlParam("BID",FsmaAddUrlParam());
		
		if ($BID == -1)
		{
			$params["__SingleMAsleiterOptions__"]  =  FsmaGetUsers4DropDown();
			$params["__Bezeichnung__"]            = "&lt;Neuer SingleMA&gt;"; 
		}		
		$params["__Bezeichnung__"]            =  $SingleMA["Bezeichnung"];
		$params["__SingleMAsleiterOptions__"]  =  FsmaGetUsers4DropDown($SingleMA["bereichsleiter"]);
		$params["__SingleMAsleiter__"]         =  $SingleMA["bereichsleiter"];
		$params["__Description__"]            =  $SingleMA["Description"];
		$params["__Preview__"]                =  $SingleMA["Preview"];
		$params["__Arbeitszeit__"]            =  $SingleMA["Arbeitszeit"];
		$params["__FSK__"]                    =  $SingleMA["FSK"];
		$params["__kommentar__"]              =  $SingleMA["kommentar"];
		$params["__LOCKED__"]                 =  $SingleMA["locked"];
		$params["__CustomFields__"]           =  $SingleMA["CustomFields"];
		$params["__SingleMAsleiterEnabled__"]  =  (current_user_can('set_bereichsleiter')) ? "" : "disabled";

		echo $DetailView->GetFilteredContent($params, true);
	}*/
}

?>
</div>
