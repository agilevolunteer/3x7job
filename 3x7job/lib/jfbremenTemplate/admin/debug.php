<div class="wrap">
<?php

	//ListView
	$Header       = new jfbremenTemplate(FSMATPL."mvif.tpl");

	$SurParams                = array();
	//Nur die Bereiche  anzeigen, die gesehen werden d�fen, NeuLink, nur wenn man  alle Bereiche  administrieren darf
	if (current_user_can('edit_all_bereiche'))
	{
		$SqlParams["__WHERE__"]   = "1=1";
		$SurParams["__newlink__"] = FsmaAddUrlParam("BID",-1);
	}
	else
	{
		$SurParams["__newlink__"] = "";
	}

	echo $Header->GetFilteredContent($SurParams);
?>
</div>