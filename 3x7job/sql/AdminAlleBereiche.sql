SELECT b.BID, b.Bezeichnung, b.kurztext as Preview, b.arbeitszeit as Arbeitszeit,
CONCAT(u.display_name, '<br>(', u.user_login, ')') as Leiter
FROM __prefix__bereiche b
LEFT OUTER JOIN  __prefix__bereichsleiter bl
on b.BID = bl.BID_bereiche
LEFT OUTER JOIN __prefix__users u
ON bl.Uid_users = u.ID
WHERE b.isDeleted = 0 ORDER BY Bezeichnung
__LIMIT__