SELECT ma.user_email as MailTo, IF(anmeld.AufAbbauConfirm = b'001',ber.MailAufbau,ber.MailNormal) as Body, bleiter.user_email as Sender
FROM __prefix__users ma
	INNER JOIN __prefix__mitarbeiterpools map
	ON ma.ID = map.MitarbeiterID
	INNER JOIN __prefix__anmeldung anmeld
	ON ma.ID = anmeld.MitarbeiterID and anmeld.jahr = map.jahr
	INNER JOIN __prefix__bereiche ber
	ON map.BID_Bereiche = ber.BID
	LEFT OUTER JOIN __prefix__bereichsleiter bltg 
	ON ber.BID = bltg.BID_Bereiche
	LEFT OUTER JOIN __prefix__users bleiter 
	ON bltg.Uid_users = bleiter.id
WHERE map.PoolID = __POOL__
