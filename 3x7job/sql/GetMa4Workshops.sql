SELECT distinct *
FROM `__PREFIX__userdata` ud
inner JOIN `__PREFIX__bereich4ma` bma
ON ud.`id`  = bma.`Ma` 
INNER JOIN `__PREFIX__status` s
ON bma.`status_id` = s.`id` 
WHERE bma.`Jahr` = 2014 and bma.`BID` = __BID__
