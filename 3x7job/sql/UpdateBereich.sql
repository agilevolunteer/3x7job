UPDATE `__prefix__bereiche` 
SET 
`Bezeichnung` = '__bezeichnung__',
`kurztext` = '__kurztext__',
`langtext` = '__langtext__',
`locked` = __locked__,
`VisInList` = __VisInList__,
`MailNormal` = '__MailNormal__',
`MailAufbau` = '__MailAufbau__',
`FSK` = '__FSK__',
`CustomFields` = '__CustomFields__',
`CustomHint` = '__CustomHint__'  
WHERE `__prefix__bereiche`.`BID` =__BID__ LIMIT 1;
DELETE FROM `__prefix__mitarbeiterpools`
WHERE BID_Bereiche = -1