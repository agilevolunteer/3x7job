SELECT b.BID, b.Bezeichnung, b.kurztext as Preview, b.arbeitszeit as Arbeitszeit, u.display_name as Leiter
FROM __prefix__bereiche b
LEFT OUTER JOIN __prefix__users u
	ON b.bereichsleiter = u.ID
WHERE VisInList = b'1'
ORDER BY Bezeichnung
__LIMIT__