SELECT mpr.BID_Bereiche AS Bereich, malt.BID_Bereiche AS AltBereich,  a.id,
 a.MitarbeiterID,
 if(a.BereitsMitgearbeitet=b'0',false,true) as BereitsMitgearbeitet,
 a.FSErfahrung,
 a.bemerkungen,
 if(a.Eingecheckt=b'0',false,true) as Eingecheckt,
 if(a.Bezahlt=b'0',false,true) as Bezahlt,
 a.jahr,
 a.AbbauBereich as AufbauBereich,
 if(a.AufAbbau=b'0',false,true) as AufAbbau,
 if(a.AufAbbauConfirm=b'0',false,true) as AufAbbauConfirm,
 if(a.Bett=b'0',false,true) as Bett,
 if(a.Abbau=b'0',false,true) as Abbau,
 mpr.status_id AS PoolStatus, malt.status_id AS AltPoolStatus,mpr.PoolID AS PrPool, malt.PoolID AS AltPool
FROM __prefix__anmeldung a
	INNER JOIN __prefix__mitarbeiterpools mpr
	ON a.MitarbeiterID = mpr.MitarbeiterID AND mpr.primaer = true AND mpr.jahr = __Jahr__
	LEFT OUTER JOIN __prefix__mitarbeiterpools malt
	ON a.MitarbeiterID = malt.MitarbeiterID AND malt.primaer = false AND malt.jahr = __Jahr__
WHERE a.MitarbeiterID = __Mitarbeiter__ AND mpr.BID_Bereiche != __WsBereich__
AND a.jahr = __Jahr__
order by mpr.TS, malt.TS
limit 1
