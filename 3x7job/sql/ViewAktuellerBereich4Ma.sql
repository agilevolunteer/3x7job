CREATE OR REPLACE ALGORITHM = TEMPTABLE VIEW __prefix__bereich4ma AS
SELECT DISTINCT u.id 'Ma', if(pp.status_id = __abgewiesen__, sp.BID_Bereiche, pp.BID_Bereiche) 'BID', if(pp.status_id = __abgewiesen__, sp.status_id, pp.status_id) 'status_id' 
, if(pp.status_id = __abgewiesen__, sp.jahr, pp.jahr) 'Jahr', if(pp.status_id = __abgewiesen__, sp.PoolID, pp.PoolID) 'PoolID', a.ID as 'AnmeldungID'
 FROM __prefix__users u 
 inner join __prefix__anmeldung a
 on  u.ID = a.MitarbeiterID
 INNER JOIN __prefix__mitarbeiterpools pp
 ON a.MitarbeiterID = pp.MitarbeiterID AND pp.primaer = TRUE and a.Jahr = pp.Jahr
 LEFT OUTER JOIN __prefix__mitarbeiterpools sp
 ON a.MitarbeiterID = sp.MitarbeiterID AND sp.primaer = FALSE and a.Jahr = sp.Jahr
WHERE (pp.status_id <> __abgewiesen__ AND sp.status_id <> __abgewiesen__) 
OR (pp.status_id <> __abgewiesen__ AND sp.status_id IS NULL) 
OR (pp.status_id = __abgewiesen__ AND sp.status_id <> __abgewiesen__) 
OR (pp.status_id = __angenommen__ AND sp.status_id = __abgewiesen__)
OR (pp.status_id <> __abgewiesen__ AND sp.status_id = __abgewiesen__ )
