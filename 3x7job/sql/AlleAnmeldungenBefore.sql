select distinct u.id "ID", u.display_name "Name", b.Bezeichnung "Bereich", p.PoolID "PID",
a.bemerkungen "Bemerkungen", p.status_id "Status", b.BID, IF(a.AufAbbau= b'001',true,false) as AufAbbau,
IF(a.Abbau = b'001',true,false) as Abbau, a.id as AID
from __prefix__users u
inner join __prefix__anmeldung a
on u.ID = a.mitarbeiterid
inner join __prefix__bereich4ma p
on p.ma = a.MitarbeiterID
inner join __prefix__bereiche b
on p.BID = b.BID
where p.Jahr = __JAHR__ and a.Jahr = __JAHR__ AND b.isDeleted = 0 AND __WHERE__
order by __ORDER__, Name
LIMIT __LIMIT__
