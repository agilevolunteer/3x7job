select u.*,
  b.Bezeichnung as Bereich,
  s.value as Status,
  IF(a.BereitsMitgearbeitet = b'001',"ja","nein") as Mitgearbeitet,
  a.FSErfahrung ,
  a.bemerkungen,
  IF(a.Eingecheckt = b'001',"ja","nein") as Eingecheckt,
  IF(a.Bezahlt = b'001',"ja","nein") as Bezahlt,
  a.jahr ,
  IF(a.AufAbbau = b'001',"ja","nein") as Aufbauwunsch,
  IF(a.AufAbbauConfirm = b'001',"ja","nein") as Teilnahme_am_Aufbau,
  IF(a.Bett = b'001',"ja","nein") as Bett
from __prefix__userdata u
inner join __prefix__anmeldung a
on u.id = a.MitarbeiterID
inner join __prefix__bereich4ma bma
on u.id = bma.Ma and a.Jahr = bma.Jahr
inner join __prefix__bereiche b
on bma.BID = b.BID
inner join __prefix__status s
on bma.status_id = s.id
where a.Jahr = __JAHR__