SELECT DISTINCT name.meta_value Nachname, vorname.meta_value Vorname, floor(((to_days(now()) - 
to_days(geb.meta_value)) / 365)) as "Alter", u.user_email as Email, vorwahlHandy.meta_value as "VorwahlHandy", 
Handy.meta_value as "Handy", vorwahlTele.meta_value as "VorwahlFestnetz", Tele.meta_value as "Festnetz", 
bpr.Bezeichnung as "Primärbereich", statuspr.value as "Status-Primaer",
balt.Bezeichnung as "Sekundaerbereich", statusalt.value as "Status-Sekundär", IF(anmeld.Bett= b'001',"ja","nein") as "Bett"
FROM __prefix__users u
	LEFT OUTER JOIN __prefix__usermeta name
	ON u.id = name.user_id and name.meta_key= "last_name"
	LEFT OUTER JOIN __prefix__usermeta vorname
	ON u.id = vorname.user_id and vorname.meta_key= "first_name"
	LEFT OUTER JOIN __prefix__usermeta geb
	ON u.id = geb.user_id and geb.meta_key= "gebdat"
	LEFT OUTER JOIN __prefix__usermeta vorwahlHandy
	ON u.id = vorwahlHandy.user_id and vorwahlHandy.meta_key= "VorwahlHandy"
	LEFT OUTER JOIN __prefix__usermeta Handy
	ON u.id = Handy.user_id and Handy.meta_key= "Handy"
	LEFT OUTER JOIN __prefix__usermeta vorwahlTele
	ON u.id = vorwahlTele.user_id and vorwahlTele.meta_key= "VorwahlTele"
	LEFT OUTER JOIN __prefix__usermeta Tele
	ON u.id = Tele.user_id and Tele.meta_key= "Tele"
	INNER JOIN __prefix__anmeldung anmeld
	ON u.id = anmeld.MitarbeiterID
	INNER JOIN __prefix__mitarbeiterpools mpr
	ON (anmeld.MitarbeiterID = mpr.MitarbeiterID AND mpr.primaer = b'001' AND mpr.jahr = __JAHR__ ) 
	LEFT OUTER JOIN __prefix__bereiche bpr
	ON mpr.BID_Bereiche = bpr.BID
	LEFT OUTER JOIN __prefix__mitarbeiterpools malt
	ON (anmeld.MitarbeiterID = malt.MitarbeiterID AND malt.primaer = b'000' AND malt.jahr = __JAHR__ ) 
	LEFT OUTER JOIN __prefix__bereiche balt
	ON malt.BID_Bereiche = balt.BID
	LEFT OUTER JOIN __prefix__status statuspr
	ON mpr.status_id = statuspr.id
	LEFT OUTER JOIN __prefix__status statusalt
	ON malt.status_id = statusalt.id
WHERE anmeld.jahr = __JAHR__ and __WHERE__
ORDER BY Nachname, Vorname