alter table __prefix__anmeldung add column AbbauBereich int;

CREATE TABLE `__prefix__food` (
  `ID` int  NOT NULL AUTO_INCREMENT,
  `value` TEXT  NOT NULL,
  PRIMARY KEY (`ID`)
);
CREATE TABLE `__prefix__abbau` (
  `ID` int  NOT NULL AUTO_INCREMENT,
  `value` TEXT  NOT NULL,
  PRIMARY KEY (`ID`)
);
insert into `__prefix__food` (value) VALUES
('fleischlich'),
('vegetarisch'),
('laktoseintollerant / vegan');
insert into `__prefix__abbau` (value) VALUES
('Küchenteam'),
('Putz- und Dekoteam'),
('Bauteam')
