CREATE TABLE if not exists `__prefix__bereiche` (
`BID` INTEGER auto_increment ,
`bereichsleiter` INTEGER NOT NULL default '0',
`Bezeichnung` char (50) NOT NULL default '',
`kurztext` MEDIUMTEXT NOT NULL default '',
`langtext` MEDIUMTEXT NOT NULL default '',
`locked` bit  NOT NULL default false,
`arbeitszeit` INTEGER NOT NULL default '0',
`kommentar` MEDIUMTEXT NOT NULL default '',
`FSK`  INTEGER NOT NULL default '0',
PRIMARY KEY (`BID`)
);

CREATE TABLE if not exists `__prefix__anmeldung` (
`id` INTEGER auto_increment ,
`MitarbeiterID` DOUBLE NOT NULL default '0',
`BereitsMitgearbeitet` bit  NOT NULL default false,
`FSErfahrung` DOUBLE NOT NULL default '0',
`bemerkungen` DOUBLE NOT NULL default '0',
`Eingecheckt` bit  NOT NULL default 0,
`Bezahlt` bit  NOT NULL default 0,
`jahr` DOUBLE NOT NULL default '2007',
PRIMARY KEY (`id`)
);

CREATE TABLE if not exists `__prefix__mitarbeiterpools` (
`PoolID` INTEGER auto_increment ,
`MitarbeiterID` INTEGER  ,
`bereiche_bereiche` INTEGER default '0',
`status_id` INTEGER default '0',
`funktion` char (32) NOT NULL default '',
`primaer` bit NOT NULL default true,
PRIMARY KEY (`PoolID`)
);

CREATE TABLE if not exists `__prefix__status` (
`id` INTEGER auto_increment ,
`value` DOUBLE NOT NULL default '0',
PRIMARY KEY (`id`)
);

