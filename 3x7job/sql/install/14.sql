CREATE TABLE `__prefix__bereichsleiter` (
  `BID_Bereiche` INTEGER UNSIGNED NOT NULL,
  `Uid_users` INTEGER UNSIGNED NOT NULL,
  PRIMARY KEY (`BID_Bereiche`, `Uid_users`)
);
insert into __prefix__bereichsleiter (BID_Bereiche, Uid_users)
select b.BID as BID_Bereiche, u.id as Uid_users from __prefix__bereiche b
inner join __prefix__users u
on b.bereichsleiter = u.id;
ALTER TABLE `__prefix__bereiche` CHANGE bereichsleiter _bereichsleiter INTEGER;