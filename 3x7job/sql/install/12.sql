ALTER TABLE __prefix__users ADD KEY(ID);
ALTER TABLE __prefix__users ADD KEY(display_name);
ALTER TABLE __prefix__mitarbeiterpools ADD KEY(PoolID);
ALTER TABLE __prefix__mitarbeiterpools ADD KEY(MitarbeiterID);
ALTER TABLE __prefix__mitarbeiterpools ADD KEY(BID_Bereiche);
ALTER TABLE __prefix__mitarbeiterpools ADD KEY(status_id);
ALTER TABLE __prefix__anmeldung ADD KEY(id);
ALTER TABLE __prefix__anmeldung ADD KEY(MitarbeiterID);
ALTER TABLE __prefix__anmeldung ADD KEY(id);
ALTER TABLE __prefix__bereiche ADD KEY(BID);
ALTER TABLE __prefix__bereiche ADD KEY(Bezeichnung);