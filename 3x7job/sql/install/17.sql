CREATE OR REPLACE ALGORITHM = TEMPTABLE VIEW __prefix__userdata AS
SELECT DISTINCT u.id as id
, name.meta_value Nachname
, vorname.meta_value Vorname
, floor(((to_days(now()) - to_days(geb.meta_value)) / 365)) as "Alter"
, ort.meta_value as "Ort"
, plz.meta_value as "Plz"
, strassehausnr.meta_value as "Strassehausnr"
, u.user_email as Email
, food.meta_value as food
, vorwahlHandy.meta_value as "VorwahlHandy"
, Handy.meta_value as "Handy"
, vorwahlTele.meta_value as "VorwahlFestnetz"
, Tele.meta_value as "Festnetz"
, Anmerkungen.meta_value as "Anmerkungen"
FROM __prefix__users u
	LEFT OUTER JOIN __prefix__usermeta name
	ON u.id = name.user_id and name.meta_key= "last_name"
	LEFT OUTER JOIN __prefix__usermeta vorname
	ON u.id = vorname.user_id and vorname.meta_key= "first_name"
	LEFT OUTER JOIN __prefix__usermeta geb
	ON u.id = geb.user_id and geb.meta_key= "gebdat"
	LEFT OUTER JOIN __prefix__usermeta vorwahlHandy
	ON u.id = vorwahlHandy.user_id and vorwahlHandy.meta_key= "VorwahlHandy"
	LEFT OUTER JOIN __prefix__usermeta Handy
	ON u.id = Handy.user_id and Handy.meta_key= "Handy"
	LEFT OUTER JOIN __prefix__usermeta vorwahlTele
	ON u.id = vorwahlTele.user_id and vorwahlTele.meta_key= "VorwahlTele"
	LEFT OUTER JOIN __prefix__usermeta Tele
	ON u.id = Tele.user_id and Tele.meta_key= "Tele"
	LEFT OUTER JOIN __prefix__usermeta Anmerkungen
	ON u.id = Anmerkungen.user_id and Anmerkungen.meta_key= "Anmerkungen"
  	LEFT OUTER JOIN __prefix__usermeta ort
	ON u.id = ort.user_id and ort.meta_key= "ort"
  	LEFT OUTER JOIN __prefix__usermeta plz
	ON u.id = plz.user_id and plz.meta_key= "plz"
  	LEFT OUTER JOIN __prefix__usermeta strassehausnr
	ON u.id = strassehausnr.user_id and strassehausnr.meta_key= "strassehausnr"
	LEFT OUTER JOIN __prefix__usermeta food
	ON u.id = food.user_id and food.meta_key= "food"
ORDER BY id;

