SELECT u.ID, u.display_name
FROM __PREFIX__users u
LEFT OUTER JOIN __PREFIX__anmeldung a
ON u.ID = a.MitarbeiterID
WHERE a.MitarbeiterID IS NULL
ORDER BY u.display_name