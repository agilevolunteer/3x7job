SELECT ma.id, ma.display_name Name, bpr.Bezeichnung as Bereich, balt.Bezeichnung as AltBereich, anmeld.AufAbbau, mpr.status_id as Status, malt.status_id as AltStatus, IF(anmeld.Eingecheckt= b'001',true,false) as Eingecheckt, IF(anmeld.Bezahlt= b'001',true,false) as Bezahlt, anmeld.id as aid, bpr.BID as PrBID, balt.BID as AltBID
FROM __prefix__users ma 
	INNER JOIN __prefix__anmeldung anmeld
	ON ma.id = anmeld.MitarbeiterID
	INNER JOIN __prefix__mitarbeiterpools mpr
	ON (anmeld.MitarbeiterID = mpr.MitarbeiterID AND mpr.primaer = b'001' AND mpr.jahr = __JAHR__ ) 
	LEFT OUTER JOIN __prefix__bereiche bpr
	ON mpr.BID_Bereiche = bpr.BID
	LEFT OUTER JOIN __prefix__mitarbeiterpools malt
	ON (anmeld.MitarbeiterID = malt.MitarbeiterID AND malt.primaer = b'000' AND malt.jahr = __JAHR__ ) 
	LEFT OUTER JOIN __prefix__bereiche balt
	ON malt.BID_Bereiche = balt.BID
WHERE anmeld.jahr = __JAHR__ and __WHERE__
ORDER BY __ORDER__
