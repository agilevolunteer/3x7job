<?php 

function FsmaSendNotificationMail($PoolID, $BID, $Status)
{
	global $wpdb;
	global $table_prefix;
			
			$query = new x7Template(X7SQL."AdminNotifyMail.sql");
			$params = array();
			$params["__prefix__"]           = $table_prefix;
			$params["__POOL__"]           = $PoolID;
			$result = $query->DoMultipleQuery(true, $params);
			$i = 0;
			
			while ($result[0][$i]->prio == 1 || $i < count($result[0][$i]))
			{
				$dbMail = $result[0][$i];
				$i++;
			} 
			
			$sender = get_option("FsmaMailAbsender");
   $replyTo = (empty($dbMail->Sender)) ? get_option("FsmaMailAbsender") : $dbMail->Sender;
			
   // echo $replyTo;

    //�bergeordnetes Template laden
			$mainMail = "";
			if ($dbMail->UseAufbau == true)
			{
				$mainMail = get_option('FsmaAngenommenMailMitAufbauText');
			}
			else
			{
				$mainMail = get_option('FsmaAngenommenMailText');
			}
			
			$mainMail = str_replace("__bereichsname__", $dbMail->Bereich, $mainMail);
			$bodyText = str_replace("__bereichstext__", $dbMail->Body, $mainMail);
				
			//ICh denke, dass der Aufruf so richtig ist.
			$success = x7SendHtmlMail($dbMail->MailTo, $sender, $bodyText, get_option("FsmaAngenommenMailBetreff"), "HtmlStandardMail.tpl",0, $replyTo);
					
    if($success){
	    FsmaMessage("Der Mitarbeiter wurde &uuml;ber die Annahme per Email informiert.");
    }else{
       FsmaError("Email konnte nicht f�r den Versand vorbereitet werden."); 
    }
}

function x7SendHtmlMail($addressee, $sender, $message, $subjekt, $template="HtmlStandardMail.tpl", $htmlcontent = 0, $reply = "")
{
	

	//sonderzeichen und zeilen�mbr�che HTML f�hig machen
	
	if ($htmlcontent == 0)
	{
		//$message = htmlentities($message);
		$message = nl2br($message);
	}

    if($reply ==""){
        $reply = $sender;
    }
    
	 //	echo "x7SendHtmlMail<br><br>";
	$headers = "FROM: $sender"."\nREPLY-TO: $reply"."\nContent-type: text/html; charset=UTF-8\n";
	

	//�bersch�ssige leerzeichen entfernen, mann weiss ja nie...
	$template = trim($template);
	$message = toGerman($message);
	
	$mail = new x7Template(X7TPL.$template);
	
	//Hier vielleicht noch den Betreff mit �bergeben?
	$message = $mail->GetFilteredContent(array("__MESSAGE__"=>$message));
	if (FSMADEBUG)
	{
    
  		echo nl2br($headers);  
		echo $subjekt;

		jfPrintDebugArray($message);
    	return true;
	}	
	else {
//		echo "sending mail...";
		return mail($addressee, $subjekt, $message, $headers);
	}
}

function toGerman($get)
{
    $get = str_replace(chr(228),"&auml;",$get);
    $get = str_replace(chr(246),"&ouml;",$get);
    $get = str_replace(chr(252),"&uuml;",$get);
    $get = str_replace(chr(196),"&Auml;",$get);
    $get = str_replace(chr(214),"&Ouml;",$get);
    $get = str_replace(chr(220),"&Uuml;",$get);
    $get = str_replace(chr(223),"&szlig;",$get);
    $get = str_replace("�","&auml;",$get);
    $get = str_replace("�","&ouml;",$get);
    $get = str_replace("�","&uuml;",$get);
    $get = str_replace("�","&Auml;",$get);
    $get = str_replace("�","&Ouml;",$get);
    $get = str_replace("�","&Uuml;",$get);
    $get = str_replace("�","&szlig;",$get);
    return $get;
}
//TODO: Irgendwo muss auch noch die Anmeldebest�tigung rumgeistern. Die sollte auch noch
//      nach hier ausgelagert werden.
?>
