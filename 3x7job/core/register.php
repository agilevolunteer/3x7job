<?php
//The Frontend
function FsmaWorkersReg($content)
{
	if (substr_count($content,FSMAWORKERSREG) > 0);
	{
		global $table_prefix;

		$validator               = new jfbremenValidate();
		$RegTemplate             = new x7Template(X7TPL."FrontendMitarbeiterRegistration.tpl");
		$PersDataTemplate        = new x7Template(X7TPL."FrontendMitarbeiterPersData.tpl");
		$AnmeldeTemplate         = new x7Template(X7TPL."FrontendMitarbeiterAnmeldung.tpl");
		$CustomFieldsTemplate    = new x7Template(X7TPL."FrontendMitarbeiterCustomFields.tpl");
		$LastCheckTemplate       = new x7Template(X7TPL."FrontendMitarbeiterLastCheck.tpl");
		$PersDataParams          = array("__ACTION__" => FsmaAddUrlParam());
		$RegParams               = array("__ACTION__" => FsmaAddUrlParam());
		$AnmeldeParams           = array("__ACTION__" => FsmaAddUrlParam());
		$LastCheckParams         = array("__ACTION__" => FsmaAddUrlParam());


		$ShowNextPage            = true;
		//$RegParams["__ACTION__"] = ;
		$CurrentUser = $_SESSION['FsmaUser'];
		$CurrentUserID = $_SESSION['FsmaUserID'];
		$RegParams['__LOGINERROR__']    = "";


		/*$validator->printDebugArray($CurrentUser);*/

		if (empty($_POST["ThisPage"]))
		{
			$Page = 0;
			$NextPage = 1;
		}
		else
		{
			$Page = $_POST["ThisPage"];
			$NextPage = $_POST["NextPage"];
		}

		//Codeausführung für absendende Seite,
		switch ($Page)
		{
			case 99:
				// $fehler = true;
				// $RegParams["__LOGINERROR__"] = "Passwort falsch";
				if (isset($_POST) && isset($_POST["email"]))
				{
					$user 		= $_POST["email"];
					$password 	= $_POST["password"];
					$errortext	= "";

					if ((empty($user)) || (empty($password)))
					{
						$errortext .= "Füll bitte alle Pflichtfelder aus.";
						$fehler = true;
					}
					if (!$validator->isMail($user))
					{
						$errortext .="<br>Bitte gib eine korrekte Emailadresse an.";
						$fehler = true;
					}


					//Benutzer einloggen
					if ($fehler == false)
					{
						$user_id = username_exists($user);
						$creds = array(
							'user_login'    => $user,
							'user_password' => $password,
							'remember'      => false
						);

						if ( !$user_id )
						{
							$fehler = true;
							$errortext .= "Es besteht kein Eintrag mit dieser Emailadresse.";
						}
						elseif (!wp_signon( $creds, true))
						{
							$fehler = true;
							$errortext .= "Das Passwort ist nicht korrekt.";
						}
						else
						{
							//wp_setcookie($user, $password, false, '', '', false);
							//do_action('wp_login', $user);
							//$CurrentUser = get_userdatabylogin($user);
							$CurrentUser = get_user_by('email',$user);
							$_SESSION['FsmaUser'] = $CurrentUser;
							$_SESSION['FsmaUserID'] = $CurrentUser->ID;
							 //$_SESSION["FsmaUser"];


						}
					}

					$RegParams['__email__']    = $user;
					$RegParams['__password__'] = $password;
					$RegParams['__LOGINERROR__']    = $errortext;

					if ($fehler == true)
						{
							$output = $RegTemplate->GetFilteredContent($RegParams, true);
							$ShowNextPage = false;
						}
				}

				break;
			case 1:
				if (isset($_POST) && isset($_POST["email"]))
				{
					$user 		= $_POST["email"];
					$password 	= $_POST["password"];
					$errortext	= "";

					if ((empty($user)) || (empty($password)))
					{
						$errortext .= "F&uuml;ll bitte alle Pflichtfelder aus.";
						$fehler = true;
					}
					if (!$validator->isMail($user))
					{
						$errortext .="<br>Bitte gib eine korrekte Emailadresse an.";
						$fehler = true;
					}


					//Neuen Benutzer anlegen
					if ($fehler == false)
					{
						$user_id = username_exists($user);
						if ( !$user_id )
						{
							//$random_password = md5(uniqid(microtime())), 0, 6);
							$user_id = wp_create_user($user, $password, $user);
							$creds = array(
								'user_login'    => $user,
								'user_password' => $password,
								'remember'      => false
							);
							if (!wp_signon( $creds, true))
							{
								$fehler = true;
								$errortext .= "Fehler beim Login, versuchte es mit dem Schritt 'Bestehendes Konto' noch einmal.";
							} else {
								$CurrentUser = get_user_by('email',$user);
								$_SESSION['FsmaUser'] = $CurrentUser;
								$_SESSION['FsmaUserID'] = $CurrentUser->ID;
							}

						}
						else
						{
							$errortext .= "Es besteht bereits ein Eintrag mit deiner Emailadresse";
							$fehler = true;
						}
					}

					$RegParams['__email__']    = $user;
					$RegParams['__password__'] = $password;
					$RegParams['__ERROR__']    = $errortext;

					if ($fehler == true)
					{
						$output = $RegTemplate->GetFilteredContent($RegParams, true);
						$ShowNextPage = false;
					}
				}
				break;
				case 2:
					$AnmeldeParams["__BereichOptions__"]    = FsmaGetBereiche4DropDown($_POST["Bereich"], "locked = 'false' and FSK < ((to_days('".get_option("FsmaTermin")."') - to_days('".$_SESSION["PersData"]["__gebdat__"]."'))/365)");
					$AnmeldeParams["__AltBereichOptions__"] = FsmaGetBereiche4DropDown($_POST["AltBereich"], "locked = 'false' and FSK < ((to_days('".get_option("FsmaTermin")."') - to_days('".$_SESSION["PersData"]["__gebdat__"]."'))/365)");
					$AnmeldeParams["__FsErfahrungen__"]     = $_POST["FsErfahrungen"];
					$AnmeldeParams["__Mitgearbeitet__"]     = (isset($_POST["mitgearbeitet"])) ? "checked" : "";
					$AnmeldeParams["__Aufbau__"]       = (isset($_POST["Aufbau"])) ? "checked" : "";
                    $AnmeldeParams["__Abbau__"]       = (isset($_POST["Abbau"])) ? "checked" : "";
					$AnmeldeParams["__Bett__"]       		= (isset($_POST["Bett"])) ? "checked" : "";
					$AnmeldeParams["__Bemerkungen__"]       = $_POST["Bemerkungen"];
					$AnmeldeParams["__BereichsID__"]        = (empty($_POST["Bereich"])) ? $_POST["oldBereich"] : $_POST["Bereich"];
					$AnmeldeParams["__AltBereichsID__"]     = (empty($_POST["AltBereich"])) ? $_POST["oldAltBereich"] : $_POST["AltBereich"];
					$AnmeldeParams["__AufbauBereichOptions__"] = x7GetAufbauBereiche4DropDown();
					$AnmeldeParams["__AufbauBereichID__"]   = $_POST["AufbauBereich"];
					$AnmeldeParams["__FsmaIsBedActive__"]   = get_option('FsmaIsBedActive');

					if ($_POST["Bereich"]==-1)
					{
						$fehler = true;
						$error = "Bitte w&auml;hle einen Hauptbereich aus!";
						$AnmeldeParams["__ERROR__"] = $error;
						$NextPage = $Page;
					}
					elseif(($_POST["Bereich"]==$_POST["AltBereich"]) && (!empty($_POST["Bereich"])))
					{
						$fehler = true;
						$error = "Alternativ- und Hauptbereich m&uuml;ssen unterschiedlich sein!";
						$AnmeldeParams["__ERROR__"] = $error;
						$NextPage = $Page;
					}
					else
					{
						$_SESSION["Anmeldung"] = $AnmeldeParams;
						//jfPrintDebugArray($AnmeldeParams);
					}
				break;
				case 3:
					if ((empty($_POST["vorname"])) || (empty($_POST["nachname"])) || (empty($_POST["VorwahlTele"])) || (empty($_POST["plz"])) || (empty($_POST["Tele"])) || (empty($_POST["gebdat"])))
					{
						$NextPage = 3;
						$fehler = true;
						$PersDataParams["__ERROR__"] = "Bitte f&uuml;lle alle Pflichtfelder aus!";
					}

					if (!$validator->isDate($_POST["gebdat"]))
					{
						$NextPage = 3;
						$fehler = true;
						$PersDataParams["__ERROR__"] .= "<br>Bitte gib dein Geburtsdatum in der Form YYYY-MM-DD (2015-07-30) statt ". $_POST["gebdat"] ." ein!";
					}

					$PersDataParams["__gebdat__"]        = $_POST["gebdat"];
					$PersDataParams["__Handy__"]         = $_POST["Handy"];
					$PersDataParams["__VorwahlHandy__"]  = $_POST["VorwahlHandy"];
					$PersDataParams["__Tele__"]          = $_POST["Tele"];
					$PersDataParams["__VorwahlTele__"]   = $_POST["VorwahlTele"];
					$PersDataParams["__ort__"]           = $_POST["ort"];
					$PersDataParams["__plz__"]           = $_POST["plz"];
					$PersDataParams["__strassehausnr__"] = $_POST["strassehausnr"];
					$PersDataParams["__nachname__"]      = $_POST["nachname"];
					$PersDataParams["__vorname__"]       = $_POST["vorname"];
                    $PersDataParams["__food__"]          = $_POST["food"];

					if ($fehler == false)
					{
						$_SESSION["PersData"] = $PersDataParams;
					}
					//$validator->printDebugArray($_POST);
					break;
				case 4:
					//$validator->printDebugArray($_POST);
					$_SESSION["CustomFields"] = $_POST;
				break;

		}

		//Seite anzeigen
		if ($ShowNextPage == true)
		{
			switch ($NextPage)
			{
				case 1:
					//Login oder Neuregistrierung
					$output = $RegTemplate->GetFilteredContent($RegParams, true);
					break;
				case 2:
					//Anmeldedetails, wie Bereichsauswahl


					if ($fehler == false)
					{


						$Anmeldung = new jfbremenTemplate(FSMASQL."MitarbeiterAnmeldung.sql");
						$AnmeldungParams = array();
						$AnmeldungParams["__Mitarbeiter__"] = $CurrentUserID;
						$AnmeldungParams["__Jahr__"]        = get_option("FsmaJahr");
						$AnmeldungParams["__prefix__"]      = $table_prefix;
						$AnmeldungParams["__WsBereich__"]   = get_option('AgvSection4Workshop');

						$AnmeldeData = $Anmeldung->DoMultipleQuery(true, $AnmeldungParams);
						//$Anmeldung->printDebugArray($AnmeldeData[0][0]);
						$Data = $AnmeldeData[0][0];

//							echo $Data->Bereich;

						//Angaben evtl. aus DB lesen __oldBereich__
						$AnmeldeParams["__oldBereich__"]        = $Data->Bereich;
						$AnmeldeParams["__oldAltBereich__"]     = $Data->AltBereich;
						$AnmeldeParams["__BereichOptions__"]    = FsmaGetBereiche4DropDown($Data->Bereich, "locked = 'false' and FSK < ((to_days('".get_option("FsmaTermin")."') - to_days('".$_SESSION["PersData"]["__gebdat__"]."'))/365)");
						$AnmeldeParams["__AltBereichOptions__"] = FsmaGetBereiche4DropDown($Data->AltBereich, "locked = 'false' and FSK < ((to_days('".get_option("FsmaTermin")."') - to_days('".$_SESSION["PersData"]["__gebdat__"]."'))/365)");
						$AnmeldeParams["__FsErfahrungen__"]     = $Data->FSErfahrung;
						$AnmeldeParams["__Mitgearbeitet__"]     = ($Data->BereitsMitgearbeitet!=0) ? "checked" : "";
						$AnmeldeParams["__Aufbau__"]       = ($Data->AufAbbau!=0) ? "checked" : "";
						$AnmeldeParams["__Abbau__"]       = ($Data->Abbau!=0) ? "checked" : "";
						$AnmeldeParams["__Bett__"]       		= ($Data->Bett!=0) ? "checked" : "";
						$AnmeldeParams["__Bemerkungen__"]       = $Data->bemerkungen;
						$AnmeldeParams["__BereichReadonly__"]   = (!empty($Data->Bereich)) ? "disabled" : "";
						$AnmeldeParams["__AltBereichReadonly__"]= (!empty($Data->Bereich)) ? "disabled" : "";
						$AnmeldeParams["__oldAltBereich__"]     = (empty($Data->AltBereich)) ? -1 : $Data->AltBereich;
                        $AnmeldeParams["__AufbauBereichOptions__"] = x7GetAufbauBereiche4DropDown();
						$AnmeldeParams["__FsmaIsBedActive__"]   = get_option('FsmaIsBedActive');
						$AnmeldeParams["__Mitarbeiter__"] = $CurrentUserID;

					}


					$output = $AnmeldeTemplate->GetFilteredContent($AnmeldeParams, true);
					break;
				case 3:
					//Persönliche Daten

					//Vorbelegen, um Missverst�ndnissen vorzubeugen
					$PersDataParams["__gebdat__"]        = "0000-00-00";

					if ($fehler==true)
					{
						$PersDataParams["__gebdat__"]        = $_POST["gebdat"];
						$PersDataParams["__Handy__"]         = $_POST["Handy"];
						$PersDataParams["__VorwahlHandy__"]  = $_POST["VorwahlHandy"];
						$PersDataParams["__Tele__"]          = $_POST["Tele"];
						$PersDataParams["__VorwahlTele__"]   = $_POST["VorwahlTele"];
						$PersDataParams["__ort__"]           = $_POST["ort"];
						$PersDataParams["__plz__"]           = $_POST["plz"];
						$PersDataParams["__strassehausnr__"] = $_POST["strassehausnr"];
						$PersDataParams["__nachname__"]      = $_POST["nachname"];
						$PersDataParams["__vorname__"]       = $_POST["vorname"];
						$PersDataParams["__food__"]          = x7GetFood4DropDown($_POST["food"]);
					}
					else
					{
						$PersDataParams["__gebdat__"]        = (empty($CurrentUser->gebdat)) ? "0000-00-00" : $CurrentUser->gebdat;
						$PersDataParams["__Handy__"]         = $CurrentUser->Handy;
						$PersDataParams["__VorwahlHandy__"]  = $CurrentUser->VorwahlHandy;
						$PersDataParams["__Tele__"]          = $CurrentUser->Tele;
						$PersDataParams["__VorwahlTele__"]   = $CurrentUser->VorwahlTele;
						$PersDataParams["__ort__"]           = $CurrentUser->ort;
						$PersDataParams["__plz__"]           = $CurrentUser->zip;
						$PersDataParams["__strassehausnr__"] = $CurrentUser->strassehausnr;
						$PersDataParams["__nachname__"]      = $CurrentUser->last_name;
						$PersDataParams["__vorname__"]       = $CurrentUser->first_name;
						$PersDataParams["__food__"]          = x7GetFood4DropDown($CurrentUser->food);
					}

					$output = $PersDataTemplate->GetFilteredContent($PersDataParams, true);
					break;
				case 4:
					//CustomFields
					$CustomItems = FsmaParseCustomFields($_SESSION["Anmeldung"]["__BereichsID__"], $_SESSION["Anmeldung"]["__AltBereichsID__"]);
					$CustomHints = FsmaLoadCustomHints($_SESSION["Anmeldung"]["__BereichsID__"], $_SESSION["Anmeldung"]["__AltBereichsID__"]);


					if (!empty($CustomItems) || !empty($CustomHints))
					{
						$CustomFieldParams = array("__ACTION__" => FsmaAddUrlParam());
						$CustomFieldParams["__CustomFieldItems__"] = $CustomItems;
						$CustomFieldParams["__MaID__"] = $CurrentUserID;

						for ($i=0;$i<count($CustomHints);$i++){
							$CustomFieldParams["__CustomHint".$i."__"]       = $CustomHints[$i];
						}

						$output = $CustomFieldsTemplate->GetFilteredContent($CustomFieldParams, true);
						$output = str_replace("_MaID_",$CurrentUserID,$output);

						break;
					}
					else
					{
						$_SESSION["CustomFields"] = 0;
					}
				case 5:
					//Alle Eingaben erneut anzeigen
					$CustomItems = FsmaParseCustomFields($_SESSION["Anmeldung"]["__BereichsID__"], $_SESSION["Anmeldung"]["__AltBereichsID__"], "FrontendMitarbeiterCustomItemsLastCheck.tpl", $_SESSION["CustomFields"]);
					$LastCheckParams = array_merge($LastCheckParams, $_SESSION["PersData"]);
					$LastCheckParams = array_merge($LastCheckParams, $_SESSION["Anmeldung"]);
					$LastCheckParams["__CustomFields__"]  = $CustomItems;
					$LastCheckParams["__Aufbau__"]   = ($LastCheckParams["__Aufbau__"] == "checked" ) ? "ja" : "nein";
                    $LastCheckParams["__Abbau__"]   = ($LastCheckParams["__Abbau__"] == "checked" ) ? "ja" : "nein";
					$LastCheckParams["__Bett__"]   = ($LastCheckParams["__Bett__"] == "checked" ) ? "ja" : "nein";
					$LastCheckParams["__Mitgearbeitet__"] = ($LastCheckParams["__Mitgearbeitet__"] == "checked" ) ? "ja" : "nein";
					$LastCheckParams["__BereichsID__"]    = FsmaGetBereich4ID($_SESSION["Anmeldung"]["__BereichsID__"]);
					$LastCheckParams["__AltBereichsID__"] = FsmaGetBereich4ID($_SESSION["Anmeldung"]["__AltBereichsID__"]);
					$LastCheckParams["__AufbauBereichID__"] =   FsmaGetContent4ID($_SESSION["Anmeldung"]["__AufbauBereichID__"]);

					$_SESSION["LastCheckParams"] = $LastCheckParams;
					$output = $LastCheckTemplate->GetFilteredContent($LastCheckParams, true);

					break;
				case 6:
					global $wpdb;
					$userid = $CurrentUserID;
					$userData = get_userdata($userid);
					//Userdaten speichern


					update_usermeta($userid,'gebdat',$_SESSION["PersData"]['__gebdat__']);
					update_usermeta($userid,'Handy',$_SESSION["PersData"]['__Handy__']);
					update_usermeta($userid,'VorwahlHandy',$_SESSION["PersData"]['__VorwahlHandy__']);
					update_usermeta($userid,'Tele',$_SESSION["PersData"]['__Tele__']);
					update_usermeta($userid,'VorwahlTele',$_SESSION["PersData"]['__VorwahlTele__']);
					update_usermeta($userid,'ort',$_SESSION["PersData"]['__ort__']);
					update_usermeta($userid,'zip',$_SESSION["PersData"]['__plz__']);
					update_usermeta($userid,'strassehausnr',$_SESSION["PersData"]['__strassehausnr__']);
					update_usermeta($userid,'last_name',$_SESSION["PersData"]['__nachname__']);
					update_usermeta($userid,'first_name',$_SESSION["PersData"]['__vorname__']);
					update_usermeta($userid,'food',$_SESSION["PersData"]['__food__']);
					$displayname = $wpdb->escape($_SESSION["PersData"]['__nachname__'].", ".$_SESSION["PersData"]['__vorname__']);
					$UpdateDisplayName = "UPDATE ".$wpdb->users." SET display_name='$displayname' WHERE ID=$userid";
					$wpdb->query($UpdateDisplayName);

					//Anmeldedaten
					$Anmeldedaten = $_SESSION["Anmeldung"];
					$Anmeldedaten["__prefix__"] = $table_prefix;
					$Anmeldedaten["__UserID__"] = $userid;
					$Anmeldedaten["__STATUS__"] = get_option("FsmaDefStatus");
					$Anmeldedaten["__Jahr__"]   = get_option("FsmaJahr");
					$Anmeldedaten["__Aufbau__"]   = ($Anmeldedaten["__Aufbau__"] == "checked" ) ? "b'001'" : "b'000'";
                    $Anmeldedaten["__Abbau__"]   = ($Anmeldedaten["__Abbau__"] == "checked" ) ? "b'001'" : "b'000'";
					$Anmeldedaten["__Bett__"]   = ($Anmeldedaten["__Bett__"] == "checked" ) ? "b'001'" : "b'000'";
					$Anmeldedaten["__Mitgearbeitet__"] = ($Anmeldedaten["__Mitgearbeitet__"] == "checked" ) ? "b'001'" : "b'000'";
					//jfPrintDebugArray($Anmeldedaten);

					if (FsmaUserHasAnmeldung($userid))
					{
						//Update
						$AnmeldeQuery = new jfbremenTemplate(FSMASQL."UpdateAnmeldung.sql");

						$AnmeldeQuery->DoMultipleQuery(true, $Anmeldedaten);

						$output .= get_option("FsmaSuccessUpdateText");

						if (FSMADEBUG)
						{
						    $output .= '<div class="debug">';
						    $mailtemplate = new x7Template(X7TPL."FrontendMitarbeiterMail.tpl");
						    $mailparams = $_SESSION["LastCheckParams"];
						    $mailparams["__ANMELDETEXT__"] = $mailtemplate->GetFilteredContent($mailparams,true,get_option("FsmaSuccessMailBody"));
						    //$output .= $guid;
						    //$mailbody  = ;
						    $output .= $mailtemplate->GetFilteredContent($mailparams, true);
						    $output .= '</div>';
						}
					}
					else
					{
						$AnmeldeQuery = new jfbremenTemplate(FSMASQL."InsertAnmeldung.sql");
						$AnmeldeQuery->DoMultipleQuery(true, $Anmeldedaten);

						//Anmeldeemail an den Mitarbeiter senden
						$mailparams = $_SESSION["LastCheckParams"];

						$mailtemplate = new x7Template(X7TPL."FrontendMitarbeiterMail.tpl");
						$mailparams = $_SESSION["LastCheckParams"];
						$mailparams["__ANMELDETEXT__"] = $mailtemplate->GetFilteredContent($mailparams,true,get_option("FsmaSuccessMailBody"));
						//$output .= $guid;
						//$mailbody  = ;
						$mailbody .= $mailtemplate->GetFilteredContent($mailparams, true);
						//$mailbody = trim(strip_tags($mailbody));
						//jfPrintDebugArray($mailparams);
						if (FSMADEBUG)
						{
							$output .= '<div class="debug">';
							$output .= "Mail to: ".$userData->user_email."<br>";
							$output .= "Btreff: ".get_option("FsmaSuccessMailBetreff")."<br>";
							$output .= "FROM: ".get_option("FsmaMailAbsender")."<br><br>";
							$output .= nl2br($mailbody);
							$output .= '</div>';
						}
						else
						{
							/*
							mail($userData->user_email,
								get_option("FsmaSuccessMailBetreff"),
								utf8_decode($mailbody),
								"FROM: ".get_option("FsmaMailAbsender")."\nContent-type: text/html; charset=iso-8859-1");
								*/

							x7SendHtmlMail($userData->user_email, get_option("FsmaMailAbsender"), $mailbody, get_option("FsmaSuccessMailBetreff"));
						}

						$output .= get_option("FsmaSuccessText");
					}

					if (is_array($_SESSION["CustomFields"]))
					{
						$keys = array_keys($_SESSION["CustomFields"]);

						foreach($keys as $key )
						{
							if ($key !="ThisPage" && $key !="NextPage")
								update_usermeta($userid,$key,$_SESSION["CustomFields"][$key]);
								//echo "$key: ".$_SESSION["CustomFields"][$key];
						}
					}

					break;

			}
		}

		$content = str_replace(FSMAWORKERSREG,$output,$content);
	}

	return $content;
}

function FsmaBereiche($content)
{
	global $table_prefix;

	//Nur ausführen, wenn Bereiche wirklich angezeigt werden sollen
	if (substr_count($content,FSMABEREICHE)>0)
	{
		if (isset($_GET["BID"]))
		{
			//DetailView
			$DetailView = new x7Template(X7TPL."FrontendBereicheDetailView.tpl");
			$Bereich    = new x7Template(X7SQL."SingleBereich.sql");
			$result = $Bereich->DoMultipleQuery(true, array("__prefix__" => $table_prefix, "__BID__" => $_GET["BID"]), ARRAY_A);

			//Grundinfos des Bereichs
			if (count($result[0]) == 0) return;

			$Bereich = $result[0][0];
			$params = array();
			$params["__Bezeichnung__"]  =  $Bereich["Bezeichnung"];
			$params["__BID__"]          =  $Bereich["BID"];
			$params["__Description__"]  =  $Bereich["Description"];
			$params["__Arbeitszeit__"]  =  $Bereich["Arbeitszeit"];
			$params["__Bereichsleiter__"]  =  "<br>".$Bereich["Bereichsleiter"];

			//foreach ($result[0] as $Bereich)
			for ($i = 1; $i < count($result[0]); $i++)
			{
				$BereichBL = $result[0][$i];
				$params["__Bereichsleiter__"]  .=  "<br>".$BereichBL["Bereichsleiter"];
			}

			$output .= $DetailView->GetFilteredContent($params);
		}
		else
		{
			//ListView
			$Header       = new x7Template(X7TPL."FrontendBereicheHeader.tpl");
			$ListItem     = new x7Template(X7TPL."FrontendBereicheListItem.tpl");
			$ListItemAlt  = new x7Template(X7TPL."FrontendBereicheListItemAlt.tpl");
			$Footer       = new x7Template(X7TPL."FrontendBereicheFooter.tpl");
			$AlleBereiche = new x7Template(X7SQL."AlleBereiche.sql");

			$result = $AlleBereiche->DoMultipleQuery(true, array("__prefix__" => $table_prefix, "__LIMIT__" => ""), ARRAY_A);

			/*
			$output .= "<!--AlleBereiche.sql \n";
			$output .= $AlleBereiche->GetFilteredContent(array("__prefix__" => $table_prefix, "__LIMIT__" => ""));
			$output .= "-->";
			*/

			$output .= $Header->GetFilteredContent();
			$alt = false;

			//foreach ($result[0] as $Bereich)
			for ($i = 0; $i < count($result[0]); $i++)
			{
				$Bereich = $result[0][$i];
				$params = array();
				$params["__Bezeichnung__"]  =  $Bereich["Bezeichnung"];
				$params["__BID__"]          =  $Bereich["BID"];
				$params["__Preview__"]      =  $Bereich["Preview"];
				$params["__morelink__"]     =  FsmaAddUrlParam("BID",$Bereich["BID"]) ;
				$params["__Arbeitszeit__"]  =  $Bereich["Arbeitszeit"];

				if (!$alt)
				{
					$output .= $ListItem->GetFilteredContent($params);
				}
				else
				{
					$output .= $ListItemAlt->GetFilteredContent($params);
				}

				$alt = !$alt;
			}

			if ($i == 0)
				$output .= "Es sind keine Bereiche vorhanden, die anzeigt werden sollen.";

			$output .= $Footer->GetFilteredContent();
		}

		$content = str_replace(FSMABEREICHE,$output,$content);
	}

	return $content;
}
?>
