<?php 
function FsmaIsAllDismissed($UID, $Jahr)
{
	global $wpdb;
	global $table_prefix;
	$query = new jfbremenTemplate(FSMASQL."BereicheCountNotDismissed.sql");
	$params = array();
	$params["__prefix__"]           = $table_prefix;
	$params["__UID__"]              = $UID;
	$params["__Jahr__"]             = $Jahr;
	$params["__DismissedStatus__"]  = get_option("FsmaDismissedStatus");
	$params["__DismissedBereich__"] = get_option("FsmaDismissedPool");
	$count = $wpdb->get_var($query->GetFilteredContent($params)) ;
		
	return ($count == 0);	
}

function FsmaChangePoolStatus($PoolID, $Status)
{
	global $wpdb;
	global $table_prefix;

	if (!empty($PoolID) && !empty($Status))
		$wpdb->query("UPDATE ".$table_prefix."mitarbeiterpools SET status_id = ".$wpdb->escape($Status)." WHERE PoolID = ".$wpdb->escape($PoolID)."");
}

function FsmaChangePool($PoolID, $BID, $Status, $sendmail = true)
{
	global $wpdb;
	global $table_prefix;

	if (!empty($PoolID) && !empty($Status))
	{
		$oldStatus = $wpdb->get_var("SELECT status_id FROM ".$table_prefix."mitarbeiterpools WHERE PoolID = ".$wpdb->escape($PoolID)."");
		$wpdb->query("UPDATE ".$table_prefix."mitarbeiterpools SET status_id = ".$wpdb->escape($Status).", BID_Bereiche = ".$wpdb->escape($BID)." WHERE PoolID = ".$wpdb->escape($PoolID)."");
		$wpdb->query("DELETE FROM ".$table_prefix."mitarbeiterpools WHERE BID_Bereiche = -1");
		//Mail schicken, wenn angenommen und nicht vorher schon angenommen gewesen
		if (($Status == get_option("FsmaAngenommenStatus")) && ($Status <> $oldStatus) && ($sendmail == true))
		{
			/*
			$query = new jfbremenTemplate(FSMASQL."AdminNotifyMail.sql");
			$params = array();
			$params["__prefix__"]           = $table_prefix;
			$params["__POOL__"]           = $PoolID;
			$result = $query->DoMultipleQuery(true, $params);
			$dbMail = $result[0][0];
			
			$sender = (empty($dbMail->Sender)) ? get_option("FsmaMailAbsender") : $dbMail->Sender;
						
			if (FSMADEBUG)
			{
				$output = "";
				$output .= '<div class="debug">';
				$output .= "Mail to: ".$dbMail->MailTo."<br>";
				$output .= "Btreff: ".get_option("FsmaSuccessMailBetreff")."<br>";
				$output .= "FROM: $sender <br><br>";
				$output .= nl2br($dbMail->Body);
				$output .= '</div>';
				
				echo $output;
			}
			else
			{
				mail($dbMail->MailTo,
					get_option("FsmaSuccessMailBetreff"),
					$dbMail->Body,
					"FROM: $sender");
			}
					
			FsmaMessage("Der Mitarbeiter wurde &uuml;ber die Annahme per Email informiert");
			*/
			
			FsmaSendNotificationMail($PoolID, $BID, $Status);
		}
	}
}

function FsmaSendToDismissedPool($UID, $Jahr)
{
	global $table_prefix;
	$query = new jfbremenTemplate(FSMASQL."InsertNewMaToPool.sql");
	$params = array();
	$params["__prefix__"]           = $table_prefix;
	$params["__UserID__"]           = $UID;
	$params["__Jahr__"]             = $Jahr;
	$params["__STATUS__"]           = get_option("FsmaDefStatus");
	$params["__BereichsID__"]       = get_option("FsmaDismissedPool");

	$query->DoMultipleQuery(true, $params);
}
?>