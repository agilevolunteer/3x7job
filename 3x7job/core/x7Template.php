<?php
class x7Template extends jfbremenTemplate
{
	var $fallBackPath;
	var $shadowPath;

	function x7Template($path, $shadow=X7SHADOW, $fallback=X7PATH)
	{
		$this->fallBackPath = $fallback;
		$this->shadowPath	= $shadow;
		
		if (is_readable($this->shadowPath.$path))
		{
			$this->jfBremenTemplate($this->shadowPath.$path);
		}
		else
		{
			$this->jfBremenTemplate($this->fallBackPath.$path);
		}
		
	}
}
?>