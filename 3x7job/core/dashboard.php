<?php
// Create the function to output the contents of our Dashboard Widget

function x7AdminDashboardWidget() 
{
	global $wpdb;
	global $table_prefix; 
	$jahr = get_option("FsmaJahr");
	$canceledPool = get_option('FsmaCanceledPool');
	$dismissedPool = get_option('FsmaDismissedPool');
	
	if (!current_user_can('edit_all_bereiche'))
		$where = "";
	
	$mitarbeiterCount = $wpdb->get_var(
			"SELECT count(*) FROM ".$table_prefix."bereich4ma f
				where Jahr = $jahr AND f.BID != $canceledPool" 
			);
	$aufbauCount = $wpdb->get_var(
			"SELECT count(*) FROM ".$table_prefix."bereich4ma f
inner join fsma_mitarbeiterpools p
on f.PoolId = p.PoolId
inner join fsma_anmeldung a
on a.MitarbeiterId = p.MitarbeiterID and a.Jahr = p.Jahr			
			WHERE a.Jahr = $jahr AND a.AufAbbau = b'001' AND f.BID != $canceledPool" 
			); 
	
	$checkinCount = $wpdb->get_var(
			"SELECT count(*) FROM ".$table_prefix."bereich4ma f
inner join fsma_mitarbeiterpools p
on f.PoolId = p.PoolId
inner join fsma_anmeldung a
on a.MitarbeiterId = p.MitarbeiterID and a.Jahr = p.Jahr		
			WHERE a.Jahr = $jahr AND a.Bezahlt = b'001' AND f.BID != $canceledPool" 
			);

	$bedCount = $wpdb->get_var(
			"SELECT count(*) FROM ".$table_prefix."bereich4ma f
inner join fsma_mitarbeiterpools p
on f.PoolId = p.PoolId
inner join fsma_anmeldung a
on a.MitarbeiterId = p.MitarbeiterID and a.Jahr = p.Jahr		
			WHERE a.Jahr = $jahr AND a.Bett = b'001' AND f.BID != $canceledPool" 
			);

	$wpdb->hide_errors();
	$roomRequestCount = $wpdb->get_var(
			"SELECT COUNT(*) 
			FROM ".$table_prefix."roomrequest
			WHERE year = $jahr"
			); 
	$wpdb->show_errors();

	$schwimmerCount = $wpdb->get_var(
			"SELECT count(*) 
			FROM ".$table_prefix."bereich4ma f
			WHERE BID = $dismissedPool AND Jahr = $jahr"
			);
			
	$cancelCount = $wpdb->get_var(
			"SELECT count(*) 
			FROM ".$table_prefix."bereich4ma f
			WHERE BID = $canceledPool AND Jahr = $jahr"
			);

	$lostCount = $wpdb->get_var(
			"SELECT count(*) 
			FROM ".$table_prefix."bereich4ma f
			WHERE Jahr  = ''"
			); 

	$params = array();
	$params["__MITARBEITERCOUNT__"] = $mitarbeiterCount;
	$params["__AUFBAUCOUNT__"] 	= $aufbauCount;
	$params["__CHECKINCOUNT__"] 	= $checkinCount;
	$params["__BEDCOUNT__"] 	= $bedCount;
	$params["__ROOMREQUESTCOUNT__"] = $roomRequestCount;
	$params["__LOSTCOUNT__"] 	= $lostCount;
	$params["__SCHWIMMERCOUNT__"] 	= $schwimmerCount;
	$params["__BEREICH__"] = "";
	
	
	
	$dashStats       = new x7Template(X7TPL."dashboardStats.tpl");
	echo $dashStats->GetFilteredContent($params, true);	
} 

function x7BlDashboardWidget() 
{
	global $wpdb;
	global $table_prefix; 
	$jahr = get_option("FsmaJahr");
		$CurrentUser = wp_get_current_user();
				
		$Bereich = $wpdb->get_var(
			"select b.Bezeichnung
			from ".$table_prefix."bereiche b
			inner join ".$table_prefix."bereichsleiter bl
			on b.BID = bl.BID_Bereiche
			where bl.Uid_Users = ".$CurrentUser->ID
		);
		
		$BereichsID = $wpdb->get_var(
			"select b.BID
			from ".$table_prefix."bereiche b
			inner join ".$table_prefix."bereichsleiter bl
			on b.BID = bl.BID_Bereiche
			where bl.Uid_Users = ".$CurrentUser->ID
		);
		
		$mitarbeiterCountBereich = $wpdb->get_var(
			"SELECT count(*) FROM ".$table_prefix."bereich4ma f
				where Jahr = $jahr AND f.BID = $BereichsID" 
			);
		$aufbauCountBereich = $wpdb->get_var(
			"SELECT count(*) FROM ".$table_prefix."bereich4ma f
			inner join fsma_mitarbeiterpools p
			on f.PoolId = p.PoolId
			inner join fsma_anmeldung a
			on a.MitarbeiterId = p.MitarbeiterID and a.Jahr = p.Jahr			
			WHERE a.Jahr = $jahr AND a.AufAbbau = b'001' AND f.BID = $BereichsID" 
			); 
	
		$checkinCountBereich = $wpdb->get_var(
			"SELECT count(*) FROM ".$table_prefix."bereich4ma f
			inner join fsma_mitarbeiterpools p
			on f.PoolId = p.PoolId
			inner join fsma_anmeldung a
			on a.MitarbeiterId = p.MitarbeiterID and a.Jahr = p.Jahr		
			WHERE a.Jahr = $jahr AND a.Bezahlt = b'001' AND f.BID = $BereichsID" 
			);
		$params = array();
		$params["__BEREICH__"] 	= $Bereich;
		$params["__MITARBEITERCOUNTBEREICH__"] = $mitarbeiterCountBereich;
		$params["__AUFBAUCOUNTBEREICH__"] 	= $aufbauCountBereich;
		$params["__CHECKINCOUNTBEREICH__"] 	= $checkinCountBereich;
		
		$dashStats       = new x7Template(X7TPL."dashboardBlStats.tpl");
		echo $dashStats->GetFilteredContent($params, true);	
	
}
// Create the function use in the action hook

function x7AddDashboardWidgets() {
	//wp_add_dashboard_widget('x7AdminDashboardWidget', 'Example Dashboard Widget', 'x7AdminDashboardWidget');
	if (current_user_can("dashboard_stats"))
	{	
		global $wp_meta_boxes;
		
		foreach (array_keys($wp_meta_boxes['dashboard']['normal']['core']) as $name){
				unset($wp_meta_boxes['dashboard']['normal']['core'][$name]);			
			}
		
		foreach (array_keys($wp_meta_boxes['dashboard']['side']['core']) as $name){
			unset($wp_meta_boxes['dashboard']['side']['core'][$name]);			
		}
		
		if (!current_user_can('edit_all_bereiche'))
		{
			wp_add_dashboard_widget('x7BlDashboardWidget', '<center><img src="'.FSMAURL.'style/icons/threexsevenjob-big.gif" /></center>', 'x7BlDashboardWidget');	
		}
		
		wp_add_dashboard_widget('x7AdminDashboardWidget', '<center><img src="'.FSMAURL.'style/icons/threexsevenjob-big.gif" /></center>', 'x7AdminDashboardWidget');
	}
} 
?>
