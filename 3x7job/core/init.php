<?php
//Erstmal alles reinholen
//require_once(FSMAPATH."core/init.php");
require_once(FSMAPATH."core/utils.php");
require_once(FSMAPATH."core/register.php");
require_once(FSMAPATH."core/mail.php");
require_once(FSMAPATH."core/utils-pools.php");
require_once(FSMAPATH."core/manualAdvice.php");
require_once(FSMAPATH."core/dashboard.php");
require_once(FSMAPATH."core/hooks.php");
require_once(FSMAPATH."core/ajax.php");

require_once(FSMAPATH."modules/xmlrpc.php");


function FsmaMakeAdminMenu()
{
    global $menu;
    global $submenu;

    add_menu_page('Mitarbeiter', '3x7job', "use_fsma", FSMAPATH.'/admin/ma.php', '','');

    add_submenu_page(FSMAPATH.'/admin/ma.php', 'Mitarbeiter', 'Mitarbeiter', "read_own_ma", FSMAPATH.'/admin/ma.php');
	add_submenu_page(FSMAPATH.'/admin/ma.php', 'Bereiche', 'Bereiche', "edit_own_bereich", FSMAPATH.'/admin/bereiche.php');
	$bereichslisteSuffix = add_submenu_page(FSMAPATH.'/admin/ma.php', 'Bereichsliste', 'Bereichsliste', "edit_all_bereiche", FSMAPATH.'/admin/bereiche2.php');

	//Extras and Export moved to modules in 0.9.4
	//add_submenu_page(FSMAPATH.'/admin/ma.php', 'Export', 'Export', "read_own_ma", FSMAPATH.'/admin/export.php');
	add_submenu_page(FSMAPATH.'/admin/ma.php', 'Extra', 'Extra', "read_own_ma", FSMAPATH.'/admin/extra.php');

    if (FSMADEBUG == true)
    {
        add_submenu_page(FSMAPATH.'/admin/ma.php', 'Debug', 'Debug', 'zorro_kaempft', FSMAPATH.'/admin/debug.php');
    }

	//Options Page new in FSMA Version 0.114
	//add_options_page('FSMA', 'FSMA', 'manage_fsma', __FILE__, 'FsmaManageOptions');
	//add_options_page('3x7job', '3x7job', 'manage_fsma', FSMAPATH.'/admin/options.php');
	add_submenu_page(FSMAPATH.'/admin/ma.php', 'Optionen', 'Optionen', "manage_fsma", FSMAPATH.'/admin/options.php');
	x7RegisterAction("x7AdminMenuGenerated");
}


function x7bereicheScripts(){
    wp_enqueue_script( 'jquery-ui-autocomplete' );
 }
function FsmaInitialize()
{

	if (class_exists("jfbremenTemplate"))
	{
		require_once 'x7Template.php';

		FsmaInstallDb();
		FsmaInstallOptions();
		add_action('admin_menu', 'FsmaMakeAdminMenu');
		add_action('admin_head', 'FsmaAdminHead');
		x7RegisterAction("x7Initialized");
	}
	else
	{
		add_action('admin_notices', 'FsmaFailInstall');
	}
}

function FsmaInstallDb()
{
	//TODO Vielleicht überprüfen, obs Installation wirklich erfolgreich war...
	global $table_prefix;

	$results = array();
	$currentDB = get_option('FsmaDbVersion');
	if (empty($currentDB))
	{
		$currentDB = 0;
	}

	if (($currentDB < FSMADBVERSION) || ($currentDB == 0))
	{
		for ($i=$currentDB+1;$i<=FSMADBVERSION;$i++)
		{
			$installDb = new jfbremenTemplate(FSMASQL."/install/$i.sql");
			$results = $installDb->DoMultipleQuery(true, array("__prefix__" => $table_prefix), ARRAY_N);

		}

		update_option('FsmaDbVersion', FSMADBVERSION);
	}

	//print_r($results);
}

/*
 * Capabilities und initial ben�tigte Optionen anlegen
 */
function FsmaInstallOptions()
{
	$currentFsma = get_option('FsmaVersion');
	if (($currentFsma < FSMAVERSION) || (empty($currentFsma)))
	{
	    //TODO Caps in einem Rutsch anlegen!
	    //FsmaNewCap("Zorro_Kaempft");
		FsmaNewCap("Use_FSMA");
		FsmaNewCap("Edit_All_Ma");
		FsmaNewCap("Edit_Own_Ma");
		FsmaNewCap("Read_All_Ma");
		FsmaNewCap("Read_Own_Ma");
		FsmaNewCap("Set_Bereichsleiter");
		FsmaNewCap("Edit_All_Bereiche");
		FsmaNewCap("Edit_Own_Bereich");
		FsmaNewCap("Dismiss_Ma");
		FsmaNewCap("Move_AltBereich");
		FsmaNewCap("Edit_Own_Profile");
		FsmaNewCap("Edit_All_Profils");
		//New In FSMAVERSION 0.114
		FsmaNewCap("Manage_FSMA");

		//New In FSMAVERSION 0.6
		FsmaNewCap("Rename_User");

		//New In FSMAVERSION 0.9.5
		FsmaNewCap("Dashboard_Stats");

		update_option('FsmaVersion', FSMAVERSION);

		//Status 4 Mitarbeiter
		FsmaInsertNewStatus("Angemeldet");
		FsmaInsertNewStatus("Abgewiesen");
		FsmaInsertNewStatus("Angenommen");

	}
}

function FsmaFailInstall()
{
	echo '<div style="background-color: rgb(207, 235, 247);" id="message" class="updated fade"><p><i>FSMA:</i> Plugin <u>jfbremenTemplate</u> ist nicht aktiviert oder installiert! 3x7job kann nicht verwendet werden.</p></div>';
}

function FsmaHead()
{
	echo '<link rel="stylesheet" href="'.FSMAURL.'/css/3x7job.css" type="text/css" media="screen" />';
	//echo '<script type="text/javascript" src="'.FSMAURL.'/scripts/jquery-1.5.min.js"></script>';
	echo '<script type="text/javascript" src="'.FSMAURL.'/scripts/3x7job.js"></script>';
}

function FsmaAdminHead()
{
	if (isset($_GET["print"]))
	{
		echo '<link rel="stylesheet" href="'.FSMAURL.'/style/fsmaAdminPrint.css" type="text/css" media="all" />';
	}
	else
	{
		echo '<link rel="stylesheet" href="'.FSMAURL.'/style/fsmaAdmin.css" type="text/css" media="screen" />';
		echo '<link rel="stylesheet" href="'.FSMAURL.'/style/fsmaAdminPrint.css" type="text/css" media="print" />';
		echo '<link rel="stylesheet" href="'.FSMAURL.'/style/jquery.qtip.min.css" type="text/css" media="screen" />';
		echo '<script type="text/javascript" src="'.FSMAURL.'/scripts/jquery.qtip.min.js"></script>';
		echo '<script type="text/javascript" src="'.FSMAURL.'/scripts/3x7job-admin.js"></script>';

	}
}

function x7RedirectAfterLogin($redirect_to, $request, $user) {
	if ($user->allcaps['use_fsma'] == 1) {
		return 'wp-admin/admin.php?page='.plugin_basename( FSMAPATH.'/admin/ma.php' );
	} else {
		return $redirect_to;
	}
}

?>
