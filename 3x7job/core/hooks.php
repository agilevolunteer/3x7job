<?php
//Install Plugin or check for things to update
add_action('plugins_loaded','FsmaInitialize');


//Filter for the output in frontend
if (get_option('FsmaActive') == 1)
{
	add_filter("the_content", "FsmaWorkersReg");
	add_filter("the_content", "FsmaBereiche");
}
else
{
	add_filter("the_content", "FsmaNoReg");
}

add_action('init', 'FsmaStartSession');
add_action('wp_footer', 'FsmaHead');

//RTFM! ;)
add_action('login_form', 'FsmaManualAdvice');
add_action('sidemenu', 'FsmaManualAdviceSide');
add_action('admin_notices', 'FsmaManualAdviceNotice');
//add_filter('admin_head', 'FsmaManualAdviceFilter');

//und hier nun unsere eigenen Hooks :D
add_action('x7BackendBereicheDetailItem', 'x7ShowToggleState');
add_action('x7BackendBereicheSaveBereich', 'x7SaveBereich');
add_action('x7BackendBereicheSaveBereich', 'x7PromoteBereichsleiter');
add_action('x7BackendBereicheSaveBereich', 'x7DismissBereichsleiter');

// Hook into the 'wp_dashboard_setup' action to register our other functions
add_action('wp_dashboard_setup', 'x7AddDashboardWidgets' );

add_action( 'admin_enqueue_scripts', 'x7bereicheScripts' );





add_filter('login_redirect', 'x7RedirectAfterLogin', 10, 3);

?>
