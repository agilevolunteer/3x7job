<?php
function FsmaManualAdvice()
{
	$advice = new x7Template(X7TPL."ManualAdvice.tpl");
	echo $advice->GetFilteredContent(array("__FSMAURL__"=>FSMAURL));
}

function FsmaManualAdviceNotice()
{	
	if (isset($_GET[page]) && (strpos($_GET[page], "FSMA") == 0))
	{
		//jfPrintDebugArray($_GET[page]);
		$advice = new x7Template(X7TPL."ManualAdviceNotice.tpl");
		echo $advice->GetFilteredContent(array("__FSMAURL__"=>FSMAURL));
	}	
	
	if (FSMADEBUG)
	{
		FsmaError("<br><CENTER><B>MITARBEITERANMELDUNG IST IM DEBUGMODUS! ES WERDEN KEINE MAILS VERSENDET!</B></CENTER><br>");		
	}
}

function FsmaManualAdviceSide()
{
	$advice = new x7Template(X7TPL."ManualAdviceSide.tpl");
	echo $advice->GetFilteredContent(array("__FSMAURL__"=>FSMAURL));
}
?>