<?php
/*
 * Adds a new Capability to the Database
 */
function FsmaNewCap($cap_name)
{
    $cap = strtolower($cap_name);
    
    // TODO This strips out also multibyte-chars - should it changed? 
    $cap = preg_replace('#[^a-z0-9]#', '_', $cap);
    $caps = get_option('IWG_RoleMan_CapList');
    if ( !in_array($cap, $caps) ) 
    {	
		$caps[] = $cap;
		
		if(!is_array($caps)) 
		{
		    $caps = array();
		}
		update_option('IWG_RoleMan_CapList', $caps);
    }
}

function FsmaMessage($text)
{
	$Message       = new x7Template(X7TPL."BackendMessage.tpl");
	$MessageParams = array();
	$MessageParams["__TEXT__"] = $text;
	
	echo $Message->GetFilteredContent($MessageParams, true);
}

function FsmaError($text)
{
	$Message       = new x7Template(X7TPL."BackendError.tpl");
	$MessageParams = array();
	$MessageParams["__TEXT__"] = $text;
	
	echo $Message->GetFilteredContent($MessageParams, true);
}

function FsmaStartSession()
{
	session_start();
	x7RegisterAction("x7SessionStarted");
}

function FsmaFormatGroupExp($val)
{
	$val = explode("-", $val);
	return $val[0];
}

function FsmaSwitchOrderExp($key, $val)
{
	switch ($key)
	{
		case "Bereich":
		case "AltBereich": return (current_user_can('read_own_ma')) ? $val : "";
			break;
		case "Name": return strtoupper(substr($val, 0, 1));
			break;
		case "Eingecheckt":
		case "Bezahlt": return ($val == 1) ? "Ja" : "Nein";
	}
}

function FsmaUserHasAnmeldung($id = -1, $Jahr="", $checkWorkshops = false)
{
	global $wpdb;
	global $table_prefix;
	$wsBereich = "BID != ". get_option('AgvSection4Workshop');
	
	if ($Jahr=="")
		$Jahr = get_option("FsmaJahr");
	
	if($checkWorkshops == true){
		$wsBereich = "BID = ". get_option('AgvSection4Workshop');
	}
	
	
		
	$query = new jfbremenTemplate(FSMASQL."CountAnmeldungen4User.sql");
	$queryParams["__prefix__"] = $table_prefix;
	$queryParams["__UserID__"]    = $id;
	$queryParams["__Jahr__"]    = $Jahr;
	$queryParams["__WsBereich__"]    = $wsBereich;
	
	$queryText = $query->GetFilteredContent($queryParams);
	return ($wpdb->get_var($queryText)>0);
	
}

/*
 * Cleanses the URL from multiple parameters
 */
function FsmaDeleteUrlParams($params, $url="")
{
	$url = ($url == "") ? $_SERVER["REQUEST_URI"] : $url;
	
	for($i=0;$i<count($params);$i++)
	{
		$url = FsmaEraseUrlParam($par, $url);
	}
		
	return $url;
}

/*
 * Deletes the specified parameter from URL
 */
function FsmaEraseUrlParam($name, $url)
{
	$urlParams = explode("&",$url);
	$neueUrl = $urlParams[0];
	
	for ($i=1;$i<=count(urlParams);$i++)
	{
		if (substr_count($urlParams[$i],$name."=")==0)
			$neueUrl .= $urlParams[$i];
	}
	
	return $neueUrl;
}

/*
 * Adds the specified parameter with value to the url
 */
function FsmaAddUrlParam($name="", $value="", $url="")
{
	if ($url=="")
	{
	
		/*$proto = explode("/", $_SERVER["SERVER_PROTOCOL"]);
		//jfPrintDebugArray($_SERVER);
		//echo $_SERVER["[SERVER_PROTOCOL]"];
		$url = strtolower($proto[0]);
		$url .= "://";
		$url .= $_SERVER["SERVER_NAME"];*/
		
		$url = $_SERVER["REQUEST_URI"];
		
		if ($name=="" && $value=="")
			return $url;
	}
	
	
	
	if (substr_count($url,"?")>0)
	{
		if (substr_count($url,$name)>0)
		{			
			$urlParams = explode("&",$url);
			$neueUrl = $urlParams[0];
			
			for ($i=1;$i<=count(urlParams);$i++)
			{
				if (substr_count($urlParams[$i],$name)>0)
				{
					$neueUrl .= "&$name=$value";
				}
				else
				{					
					$neueUrl .= "&".$urlParams[$i];
				}				
			}
			
			return $neueUrl;
		}
		
		$url .= "&";
	}
	else
	{
		$url .= "?";
	}

	return "$url$name=$value";
}

function x7GenerateX7SubMenuPage($pageName, $path, $permission='use_fsma')
{
	 global $menu;
    global $submenu;
	
	add_submenu_page(FSMAPATH.'/admin/ma.php', $pageName, $pageName, $permission, $path);
}

function x7RegisterAction($name, &$output = null, &$input = null)
{	
	//Die Parameter kommen in ne Session, damit sie beim Bearbeiten der action zur Verf�gung stehen
	$_SESSION[$name."IN"] = $input;
	$_SESSION[$name."OUT"] = $output;
	do_action($name);
	
	//Zur�ckschreiben der hoffentlich bearbeiteten Parameter
	$input = $_SESSION[$name."IN"];
	$output = $_SESSION[$name."OUT"];		
	
	//und dran denken den Speicher aufzur�umen
	$_SESSION[$name."IN"] = null;
	$_SESSION[$name."OUT"] = null;
}

?>