<?php

add_action('wp_ajax_3x7_toggleBereichDescVisible', 'x7_ajax_toggleBereichDescVisible');
function x7_ajax_toggleBereichDescVisible(){
   // echo "hallo";
    $response = array();
    $response["status"] = jfPost('status');
   
    $response["prevquery"] = $wpdb->last_query;
    $bid = jfPost("bid");
    global $wpdb;
    global $table_prefix;
    $tableName = $table_prefix."bereiche";
    $col = 'VisInList';
    $status = jfPost('status');
	   $newStatus  = ($status == 1) ? "0" : "1";
    
    $wpdb->update(
        $tableName,
        array($col => $newStatus), 
        array("bid" => $bid),
        array('%d'),
        array('%d') 
    );
   
    
    $response["query"] = $wpdb->last_query;
    
    x7SendOutPut($response);
}

add_action('wp_ajax_3x7_loadBereiche', 'x7_ajax_loadBereiche');
function x7_ajax_loadBereiche(){
	global $table_prefix;
     $AlleBereiche = new x7Template(X7SQL."AdminAlleBereicheLight.sql");
	 $SqlParams = array();
	 $SqlParams["__prefix__"]  = $table_prefix;
	 $SqlParams["__LIMIT__"]   = "";
	 
	 //Nur die Bereiche  anzeigen, die gesehen werden d?fen, NeuLink, nur wenn man  alle Bereiche  administrieren darf
	if (current_user_can('edit_all_bereiche'))
	{
		$SqlParams["__WHERE__"]   = "1=1";
	}
	else
	{
		$SqlParams["__WHERE__"]   = "bl.Uid_users = ".$CurrentUser->ID;		
	}
	 $result = $AlleBereiche->DoMultipleQuery(true, $SqlParams, ARRAY_A);
		
	 x7sendOutPut($result[0]);
}

function x7sendOutPut($data)
{
	 $output=json_encode($data);
	 
	// response output
	header( "Content-Type: application/json" );
	if(is_array($output)){
        print_r($output);   
	}
	else{
		echo $output;
	}
         
	die;
}

add_action('wp_ajax_3x7_toggleBereichLocked', 'x7_ajax_toggleBereichLocked');
function x7_ajax_toggleBereichLocked()
{
	global $table_prefix;
    $saveQuery = new x7Template(X7SQL."AdminToggleBereicheLocked.sql");
	$SqlParams = array();
	$SqlParams["__prefix__"]  = $table_prefix;
	$SqlParams["__bid__"]   = jfPost('bid');
	$status = jfPost('status');
	$SqlParams["__status__"]   = ($status == 1) ? "b'001'" : "b'000'";
	
	$result = $saveQuery->DoMultipleQuery(true, $SqlParams, ARRAY_A, true);
		
	
	if ($saveQuery->Succeeded() == true)
	{
		$loadToggledBereich = new x7Template(X7SQL."AdminSingleBereich4Liste.sql");
		$loadBereichResult = $loadToggledBereich->DoMultipleQuery(true, $SqlParams, ARRAY_A);
		x7sendOutPut($loadBereichResult[0][0]);
	}
	else
	{
		echo $saveQuery->GetErrorText();
		x7sendOutPut(false);	
	} 
	//sendOutPut($bid);
}

add_action('wp_ajax_3x7_toggleBereichDeleted', 'x7_ajax_toggleBereichDeleted');
function x7_ajax_toggleBereichDeleted()
{
	global $table_prefix;
	$saveQuery = new x7Template(X7SQL."AdminToggleBereicheDeleted.sql");
	$SqlParams = array();
	$SqlParams["__prefix__"]  = $table_prefix;
	$SqlParams["__bid__"]   = jfPost('bid');
	$status = jfPost('status');
	$SqlParams["__status__"]   = ($status == 0) ? "b'001'" : "b'000'";

	$result = $saveQuery->DoMultipleQuery(true, $SqlParams, ARRAY_A, true);


	if ($saveQuery->Succeeded() == true)
	{
		$loadToggledBereich = new x7Template(X7SQL."AdminSingleBereich4Liste.sql");
		$loadBereichResult = $loadToggledBereich->DoMultipleQuery(true, $SqlParams, ARRAY_A);
		x7sendOutPut($loadBereichResult[0][0]);
	}
	else
	{
		echo $saveQuery->GetErrorText();
		x7sendOutPut(false);
	}
	//sendOutPut($bid);
}
?>