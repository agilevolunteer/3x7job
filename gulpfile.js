// Untitled Project - created with Gulp Fiction
var gulp = require("gulp");
var stylus = require("gulp-stylus");
var concat = require("gulp-concat");
gulp.task("styles", [], function () {
    gulp.src(["./3x7job/style/frontend/**/*.styl"])
        .pipe(stylus({
            paths: ["./3x7job/style/lib"],
            import: ["variables", "mediaQueries"]
        }))
        .pipe(concat("3x7job.css"))
        .pipe(gulp.dest("./3x7job/css"));
});

gulp.task("build:setup", [], function () {
    gulp.src(["./3x7job*/**/*.{js,css,php,html}", "*.php"])
        .pipe(gulp.dest("build"))
})

gulp.task("watch", function(){
    return gulp.watch("./3x7job/style/**/*.styl", ["default"]);
});

gulp.task("default", ["styles"]);
gulp.task("develop", ["default", "watch"]);



